let reporter = require('cucumber-html-reporter');

let options = {
    theme: 'bootstrap',
    jsonFile: 'report.json',
    output: 'cucumber_report.html',
    reportSuiteAsScenarios: true,
    scenarioTimestamp: true,
    launchReport: true,
    metadata: {
        "App Version":"0.3.2",
        "Test Environment": "STAGING",
        "Browser": "Chrome  54.0.2840.98",
        "Platform": "Windows 10",
        "Parallel": "Scenarios",
        "Executed": "Remote"
    }
};

reporter.generate(options);



/*
npmjs.2019.cucumber-html-reporter.Available at:https://www.npmjs.com/package/cucumber-html-reporter
Accessed[17/09/2019]
 */