/* Enable babel polyfill to allow google crawler process ES6Code */

require('babel-polyfill');

require('whatwg-fetch');

if (process.env.NODE_ENV === 'test') {
    require('raf').polyfill(global);
}

/*
2017.MartijnHols.
Available at:https://github.com/WoWAnalyzer/WoWAnalyzer/blob/2c67a970f8bd9026fa816d31201c42eb860fe2a3/config/polyfills.js#L1
Accessed[07/08/2019]

*/
