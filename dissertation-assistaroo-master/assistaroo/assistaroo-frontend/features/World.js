// Dependencies
const { setWorldConstructor } = require('cucumber');
const puppeteer = require('puppeteer');
const scope = require('./support/scope');

const World = function() {
    scope.driver = puppeteer;
};

setWorldConstructor(World);

/*

Code adopted from
Medium.2018.End-to-end testing Single Page Apps and Node.js APIs with Cucumber.js and Puppeteer.
Available at:https://medium.com/@anephenix/end-to-end-testing-single-page-apps-and-node-js-apis-with-cucumber-js-and-puppeteer-ad5a519ace0
[Accessed: 13/09/2019]

 */