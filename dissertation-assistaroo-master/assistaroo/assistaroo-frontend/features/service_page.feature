Feature: Access The service list  page


  Scenario Outline: A Customer can navigate to the service list  page
    Given that a customer navigates to the service home page at "http://localhost:3000/"
    Then  the customer must see a page that contains personal main category and "<personal-sub-Categories>"
    Examples:
      | personal-sub-Categories |
      | hair                    |
      | beauty                  |
      | treatments              |


