const { Given,  Then } = require('cucumber');
const puppeteer = require('puppeteer');


    Given('that a customer navigates to the service home page at {string}', function (url) {
            try {
                url= "http://localhost:3000/";
                (async() => {
                    const browser =  await puppeteer.launch({
                        args: ['--no-sandbox', '--disable-setuid-sandbox'],
                    });
                const page = await browser.newPage();
               await page.goto(url);
                })();
            }catch (error) {
                console.log(error);
                console.log('failed to open the page', url);
            }
        return 1;
    });

    Then('the customer must see a page that contains personal main category and {string}', function (string) {

        try{
        (async() => {
            const browser =  await puppeteer.launch({
                args: ['--no-sandbox', '--disable-setuid-sandbox'],
            });
            const page = await browser.newPage();
            await page.goto("http://localhost:3000/");
            await page.content("personal");
        })();
        }catch (error) {
        console.log(error);
        console.log('failed to open the page');
    }

        return "Customer is on" + string + "Category";

    });










    /*
    REFERENCES
    Available at:https://github.com/GoogleChrome/puppeteer/issues/963
    [Accessed: 13/09/2019]
     */