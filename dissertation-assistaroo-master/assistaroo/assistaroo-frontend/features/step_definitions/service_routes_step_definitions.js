
const {When, Then  } = require('cucumber');
const puppeteer = require('puppeteer');



    When('the customer clicks on {string} from the main category', function (string) {

        let url = "http://localhost:3000/" +string;
       try {
           (async () => {
               const browser = await puppeteer.launch({
                   args: ['--no-sandbox', '--disable-setuid-sandbox'],
               });
               const page = await browser.newPage();
               await page.goto('http://localhost:3000');
               await page.waitFor(5000);
               await page.click("#" + string);
               await page.goto(url);
               await browser.close();
           })();
       }catch (error) {
           console.log(error);
           console.log('failed to open the page', url);
       }
        return 1;
    });


    Then('the customer must be redirected to {string} and should see {string} subcategory', function (string, string2) {

        try {
            (async () => {
                const browser = await puppeteer.launch({
                    args: ['--no-sandbox', '--disable-setuid-sandbox'],
                });

                const page = await browser.newPage();
                await page.url('http://localhost:3000' + string);
                await page.content(string2);
                await browser.close();
            })();
        }catch (error) {
            console.log(error);
            console.log('failed to open the page', url);
        }

        return 1;
    });



