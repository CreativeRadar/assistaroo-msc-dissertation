Feature: Access The routes in services page


  Scenario Outline: A Customer can navigate to each of the service categories page

    When  the customer clicks on "<service>" from the main category
    Then  the customer must be redirected to "http://localhost:3000/<service>" and should see "<sub-categories>" subcategory

    Examples:
    |service|sub-categories|
    |laundry|washing       |
    |cleaning|cleaning     |
    |personal|beauty       |
    |maintenance|window cleaning|
    |pets       |grooming       |
    |products   |vacum cleaner  |
    |home-care      |care       |