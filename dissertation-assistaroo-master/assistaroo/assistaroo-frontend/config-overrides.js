
const { injectBabelPlugin } = require('react-app-rewired');

const rootImportConfig  = [
    "root-import",
    {
        rootPathPrefix: "~",
        rootPathSuffix: "src"
    }
];

module.exports =  config => injectBabelPlugin(rootImportConfig, config);

/*

code adapted from
https://medium.com/@leonardobrunolima/react-tips-working-with-relative-path-using-create-react-app-fe55c5f97a21
Accessed[22/07/2019]

 */