import React from 'react';
import './css/customer.scss';
import  'bootstrap/dist/css/bootstrap-grid.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

// Code Adapted from https://material-ui.com/components/grid/#grid-with-breakpoints
//[Accessed: 09 July 2019]
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        sideBar:{
            float:'right',
            marginTop:'5vh',
            marginRight:'2.5vw',
            width:'30vw',
        },
        sideBarItems:{
            color: theme.palette.text.secondary,
            height: '41.45vh',
            marginBottom:'3vh',
            padding: theme.spacing(2),
            textAlign: 'center',
        },

    }),
);


export default function Test() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            {/* SIde Bar */}
            <Grid  xs={12} sm={3} className={classes.sideBar}>
                <header class="sub-header text-center">Test</header>
                <Paper className={classes.sideBarItems}>Test</Paper>
            </Grid>



        </div>
    );

}
