import React from 'react';
import './css/customer.scss';
import  'bootstrap/dist/css/bootstrap-grid.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import HeaderComponent from "./HeaderComponent";
import ServiceMainCategories from "./ServiceMainCategories";
import Media from "react-media";
import SideBar from "./SideBar";
import GlobalFooter from "./GlobalFooter";
import confirmPostcode from './VerifyPostcodePopup';


// Code Adapted from https://material-ui.com/components/grid/#grid-with-breakpoints
//Accessed[09th July 2019]
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            fontSize: '1em',
            height: '41.25vh',
            marginBottom:'3vh',
            marginLeft: '2.5vw',
            textAlign: 'left',
            width:'62.2vw',
        },
        '@media (max-width: 992px)': {
            paper: {
                fontSize: '1em',
                width: '93vw',
                marginLeft: '0',
                marginTop: '-0.45em',


            },
        },
        subCategories:{
            color: theme.palette.text.secondary,
            height: '41.25vh',
            marginBottom:'3vh',
            marginLeft: '2.5vw',
            marginTop:'-1.4vh',
            position:'absolute',
            padding: theme.spacing(2),
            textAlign: 'center',
            top:'47vh',
        },
        imageRounded:{
            borderBottomLeftRadius: '0.25em',
            borderBottomRightRadius: '0.25em',
        },
        Height:{
            height: '30vh',
        },
    }),
);



export default function HomeComponent(){
    const classes = useStyles();

/* Get Users Current Location on pageload
    useEffect(() => {
        getLocation();
    }); */


    return (
        <div className={classes.root}>
            <HeaderComponent />
            <ServiceMainCategories/>
            <Media query="(min-width: 992px)">
                <SideBar/>
            </Media>

            <Grid container spacing={4}>
                {/* Home Sub-Categories */}
                <Grid item xs={12} sm={8} className={classes.subCategories}>

                    <span id="geolocation-support"> </span>
                    <Grid>
                        <header class="sub-header categories-sub-header"                                                                                                                                                                    >Home</header>
                    </Grid>

                        <Paper className={classes.paper} onClick={confirmPostcode}>
                            Home
                            <span></span>

                        </Paper>



                </Grid>

            </Grid>
            <GlobalFooter/>
        </div>
    );

}
