/* HTML5 Geolocation Code adapted from
https://www.w3schools.com/html/html5_geolocation.asp
[Accessed: 02/08/2019]
and modified to suit project needs
*/
import React from 'react'

// Get and display  Users Current Location(latitude and Longitude)
let browser_support = document.getElementById("geolocation-support");
    export default function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            browser_support.innerHTML = "Geolocation is not supported by this browser.";
        }




        //Extract users postcode from above longitudinal and latitudinal values using googles reverse Geocoding API
        function showPosition(position,latitude,longitude) {
                 latitude = position.coords.latitude;
                 longitude = position.coords.longitude;

            let req = new XMLHttpRequest();
                req.open("GET", 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&key=AIzaSyC6MMG3QMJgmAPzyy7Bfi7vf-6R94hWs0Q', true)

                // Result callback
                req.onreadystatechange = function() {
                    if (req.readyState == 4) {
                        let result = JSON.parse(req.response).results
                        for (let i = 0, length = result.length; i < length; i++) {
                            for (let j = 0; j < result[i].address_components.length; j++) {
                                let component = result[i].address_components[j]
                                if (~component.types.indexOf("postal_code")) {
                                    window.postcode = component.long_name;
                                }
                            }
                        }
                    }
                }

                req.send()
            }


    }

/* Reverse Geocoding Code Adapted from
    https://stackoverflow.com/questions/16725239/how-do-i-convert-lat-and-long-to-uk-postcode-with-javascript
   [Accessed: 06/08/2019]
   and modified to suit project needs
   */