import React from 'react';
import './css/customer.scss';
import  'bootstrap/dist/css/bootstrap-grid.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { useState,useEffect} from 'react';


// Code Adapted from https://material-ui.com/components/grid/#grid-with-breakpoints
//[Accessed: 09 July 2019]
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
        subCategories:{

            color: theme.palette.text.secondary,
            height: '41.25vh',
            marginBottom:'3vh',
            marginLeft: '2.5vw',
            padding: theme.spacing(2),
            textAlign: 'center',
},
        sideBar:{
            color: theme.palette.text.secondary,
            height: '41.45vh',
            marginBottom:'3vh',
            padding: theme.spacing(2),
            textAlign: 'center',
        },
        Height:{
            height: '30vh',
        },
    }),
);


export default function ServiceSubCategories() {
    const classes = useStyles();
    const [laundry, setLaundry] = useState([]);


    useEffect( () => {
         fetch('/laundry')
         .then(response =>response.json())
         .then(data => setLaundry(data))
     .catch((e)=>console.log(e))
}, []);

    return (
        <div className={classes.root}>
                    <Grid container spacing={4}>


                        {/* Laundry sub-category*/}
                        <Grid item xs={12} sm={8} className={classes.subCategories}>
                            <Grid>
                                <header class="sub-header categories-sub-header">Category Name</header>
                            </Grid>
                            {laundry.map(laundry => (
                                <Paper className={classes.subCategories}>
                                        {laundry.sub_category}
                                </Paper>
                            ))}
                     </Grid>



                    </Grid>
                </div>
            );

    }
