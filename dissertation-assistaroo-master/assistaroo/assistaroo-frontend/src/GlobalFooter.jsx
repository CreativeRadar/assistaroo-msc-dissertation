import React from 'react';
import './css/App.css';
import './css/customer.scss';
import {createStyles, makeStyles, Theme} from "@material-ui/core";
import {faEnvelope, faPhone, faFax, faHomeLg} from "@fortawesome/pro-light-svg-icons";
import {faInstagram, faTwitter, faLinkedIn, faLinkedinIn} from "@fortawesome/free-brands-svg-icons"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
    }),
);


export default function GlobalFooter(){
    const classes = useStyles();
        return(
            <div>
                {/* Begin Footer */}
                <footer class="page-footer font-small " >

                    <div class="container text-md-left  mt-5">
                        <div class="row mt-3 footer-row">
                            <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

                                {/*Content */}
                                <h4 class="font-weight-bold footer-header">Assistaroo.com</h4>
                                <hr class="accent-2 mb-4 mt-0 d-inline-block mx-auto footer-hr" />
                                    <p>UK's first app that connects consumers with multiple home and personal
                                        care support service suppliers, through a centralised and safe market place.</p>

                            </div>

                            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4 footer-follow">


                                <h4 class="font-weight-bold footer-header">Follow</h4>
                                <hr class="accent-2 mb-4 mt-0 d-inline-block mx-auto footer-hr" />

                                <p><FontAwesomeIcon icon={faInstagram}  className={'footer-item'}/>Instagram</p>
                                <p><FontAwesomeIcon icon={faTwitter}  className={'footer-item'}/>Twitter</p>
                                <p><FontAwesomeIcon icon={faLinkedinIn}  className={'footer-item'}/>Linked In</p>
                            </div>
                            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4 footer-explore">


                                <h4 class=" font-weight-bold footer-header">Explore</h4>
                                <hr class=" accent-2 mb-4 mt-0 d-inline-block mx-auto footer-hr" />
                                <p onClick={()=>window.location.href = "https://assistaroo-back-end.azurewebsites.net/about-us.html"}>
                                       About Us
                                </p>
                                <p onClick={()=>window.location.href = "https://assistaroo-back-end.azurewebsites.net/faq.html"}>
                                   Help
                                </p>
                                <p onClick={()=>window.location.href = "https://assistaroo-back-end.azurewebsites.net/termsandconditions.html"}>
                                   Terms and Conditions
                                </p>
                                <p onClick={()=>window.location.href = "https://assistaroo-back-end.azurewebsites.net/customer-details.html"}>
                                    Manage Account
                                </p>

                            </div>

                            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4 footer-contact">


                                <h4 class=" font-weight-bold footer-header">Contact Us</h4>
                                <hr class="accent-2 mb-4 mt-0 d-inline-block mx-auto footer-hr" />
                                    <p>
                                        <FontAwesomeIcon icon={faHomeLg}  className={'footer-item'}/> South wales UK</p>
                                    <p>
                                        <FontAwesomeIcon icon={faEnvelope}  className={'footer-item'} />info@assistaroo.com</p>
                                    <p>
                                        <FontAwesomeIcon icon={faPhone}  className={'footer-item'}/> +447-45-56-56 </p>
                                    <p>
                                        <FontAwesomeIcon icon={faFax}  className={'footer-item'}/> +447-34-34-34</p>

                            </div>


                        </div>


                    </div>

                    {/* Copyright */}
                    <div class="footer-copyright text-center py-3">&copy; Assitaroo.com 2019. All rights reserved
                    </div>


                </footer>
                {/* End Footer */}



            </div>
        );
    }




/*
* Footer Code Adapted from https://mdbootstrap.com/docs/jquery/navigation/footer/
* [Accessed: 30/07/2019]
*/
