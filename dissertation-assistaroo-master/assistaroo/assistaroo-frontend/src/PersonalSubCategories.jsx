import React from 'react';
import './css/customer.scss';
import  'bootstrap/dist/css/bootstrap-grid.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import Image from 'react-bootstrap/Image'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { useState,useEffect } from 'react';
import HeaderComponent from "./HeaderComponent";
import ServiceMainCategories from "./ServiceMainCategories";
import Media from "react-media";
import SideBar from "./SideBar";
import GlobalFooter from "./GlobalFooter";
import Helmet from "react-helmet";
import confirmPostcode from './VerifyPostcode';
import getLocation from './CustomerLocation';

// Code Adapted from https://material-ui.com/components/grid/#grid-with-breakpoints
//[Accessed: 09 July 2019]
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            fontSize: '1em',
            height: '41.25vh',
            marginBottom:'3vh',
            marginLeft: '2.5vw',
            textAlign: 'left',
            width:'62.2vw',
        },
        '@media (max-width: 992px)': {
            paper: {
                fontSize: '1em',
                width: '93vw',
                marginLeft: '0',
                marginTop: '-0.45em',
            },
        },
        subCategories:{
            color: theme.palette.text.secondary,
            height: '41.25vh',
            marginBottom:'3vh',
            marginLeft: '2.5vw',
            marginTop:'-1.4vh',
            position:'absolute',
            padding: theme.spacing(2),
            textAlign: 'center',
            top:'47vh',
        },
        imageRounded:{
            borderBottomLeftRadius: '0.25em',
            borderBottomRightRadius: '0.25em',
        },
        Height:{
            height: '30vh',
        },
    }),
);



export default function PersonalSubCategories() {
    const classes = useStyles();
    const [personal, setPersonal] = useState([]);


    // Fetch Personal data from Api on load
    useEffect( () => {
        getLocation();
        fetch('https://assistaroo-back-end.azurewebsites.net/personal-sub-categories')
            .then(response => response.json())
            .then(data => setPersonal(data))
            .catch((e)=>console.log(e))
    }, []);


   //Method to Render nested subcategories when parent category is clicked or redirect to Postcode pop up.
    function displayNestedCategories(e) {
        let verify_postcode = '';
        if(e.target.id == 6) {
            window.location.href = '/treatments';
        }

        else {
            confirmPostcode();
            //get serviceID of clicked element
            let serviceID = e.target.getAttribute('data-description');
            let postalCode = window.user_postcode;

            // submit postcode and serviceID of triggering element to the server
            let data = {"serviceID": serviceID, "postcode": postalCode}
            fetch('/customer-location',{
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(response => response.json())
                .then(data => console.log(data));

            // redirect to suppliersList route
            (window.user_postcode !== null) ? window.location.href = '/suppliersList/' + serviceID : alert('Please confirm your postcode');

        }

    }


    return (
        <div className={classes.root}>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Assistaroo | Personal</title>
                <meta name="description" content="Assistaroo Personal services" />
            </Helmet>

            <HeaderComponent />
            <ServiceMainCategories/>
            <Media query="(min-width: 992px)">
                <SideBar/>
            </Media>

            <Grid container spacing={4} >


                {/* Personal Sub-Category*/}
                <Grid item xs={12} sm={8} className={classes.subCategories}>
                    <span id="geolocation-support" >

                    </span>

                    <Grid>

                        <header class="sub-header categories-sub-header">
                            Personal
                        </header>


                    </Grid>

                    {/* Implement Conditional Routing for nested sub categories */}
                    {personal.map(personal => (


                        <Paper className={classes.paper}  onClick={displayNestedCategories}  >

                            <span class="title"  id={personal.servicesOfferedID}> {personal.description} </span>
                            <span class="body" >
                              <Image src={personal.image_url}  className={classes.imageRounded}  id={personal.servicesOfferedID}
                                     data-description={personal.serviceID}  width={'100%'} height={'100%'}/>

                            </span>

                        </Paper>
                    ))}
                </Grid>

            </Grid>
            <GlobalFooter/>
        </div>


    );

}

