import React from 'react';
import './css/customer.scss';
import  'bootstrap/dist/css/bootstrap-grid.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import Image from 'react-bootstrap/Image'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { useState,useEffect } from 'react';
import HeaderComponent from "./HeaderComponent";
import ServiceMainCategories from "./ServiceMainCategories";
import Media from "react-media";
import SideBar from "./SideBar";
import GlobalFooter from "./GlobalFooter";
import Helmet from "react-helmet";
import confirmPostcode from './VerifyPostcode';


/* Code Adapted from https://material-ui.com/components/grid/#grid-with-breakpoints
[Accessed: 09th July 2019]
*/

/* Style classes */
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            fontSize: '1em',
            height: '41.25vh',
            marginBottom:'3vh',
            marginLeft: '2.5vw',
            textAlign: 'left',
            width:'62.2vw',
        },
        '@media (max-width: 992px)': {
            paper: {
                fontSize: '1em',
                width: '93vw',
                marginLeft: '0',
                marginTop: '-0.45em',


            },
        },
        subCategories:{
            color: theme.palette.text.secondary,
            height: '41.25vh',
            marginBottom:'3vh',
            marginLeft: '2.5vw',
            marginTop:'-1.4vh',
            position:'absolute',
            padding: theme.spacing(2),
            textAlign: 'center',
            top:'47vh',
        },
        imageRounded:{
            borderBottomLeftRadius: '0.25em',
            borderBottomRightRadius: '0.25em',
        },
        Height:{
            height: '30vh',
        },
    }),
);



export default function LaundrySubCategories(){
    const classes = useStyles();
    const [products, setProducts] = useState([]);


    /* Fetch laundry data from Api on load */
    useEffect( () => {
        fetch('https://assistaroo-back-end.azurewebsites.net/products-sub-categories')
            .then(response =>response.json())
            .then(data => setProducts(data))
            .catch((e)=>console.log(e))
    }, []);

    // Display Supplier List
    function supplierList(e) {
        confirmPostcode();

        //get serviceID of clicked element
        let serviceID = e.target.id;
        let postalCode = window.user_postcode;

        let data = {"serviceID": serviceID, "postcode": postalCode}

        //submit postcode and serviceID of triggering element to the server
        fetch('/customer-location',{
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            console.log(data);
        });

        // redirect to suppliersList route
        (window.user_postcode !== null)? window.location.href = '/suppliersList/'+ serviceID:alert('Please confirm your postcode');


    }

    {/* Render Fetched Data */}
    return (
        <div className={classes.root}>

            <Helmet>
                <meta charSet="utf-8" />
                <title>Assistaroo | Laundry</title>
                <meta name="description" content="Assistaroo Laundry services" />
            </Helmet>

            <HeaderComponent />
            <ServiceMainCategories/>
            <Media query="(min-width: 992px)">
                <SideBar/>
            </Media>

            <Grid container spacing={4}>


                {/* Products Sub-Categories */}
                <Grid item xs={12} sm={8} className={classes.subCategories}>
                    <Grid>

                        <header class="sub-header categories-sub-header">
                            Products
                        </header>

                    </Grid>
                    {products.map(products => (
                        <Paper className={classes.paper} onClick={supplierList}>

                            <span class="title" id={products.serviceID}> {products.description} </span>
                            <span class="body">
                                <Image src={products.image_url}  width={'100%'} height={'100%'} className={classes.imageRounded} id={products.serviceID}/>
                            </span>

                        </Paper>
                    ))}
                </Grid>

            </Grid>

            <GlobalFooter/>
        </div>
    );

}
