import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import React from "react";
import { Link } from 'react-router-dom';
import {createStyles, makeStyles, Theme} from "@material-ui/core";
import { faDryerAlt,faBroom,faSpa,faDog,faShoppingCart,
         faChevronLeft,faChevronRight,faHammer,
         faHandsHelping}
         from  "@fortawesome/pro-light-svg-icons";
import {Button}from 'react-bootstrap';


// Code Adapted from https://material-ui.com/components/grid/#grid-with-breakpoints
//[Accessed: 09 July 2019]

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
            width: '13.2vw',
            fontSize:'1em',
            border:0,
            '&:hover': {
                background: '#f5f3f2',
            },
            '&:active':{
                background: '#eee',
                color:'#e65c00',
            },


        },

        link:{
            textDecoration:'none',
        },
            '@media (max-width: 992px)': {
                paper: {
                    fontSize: '1em',
                    width: '23vw',
                    marginTop: '-1em',
                },

    },
       mainServices:{
            color: theme.palette.text.secondary,
            height: '30vh',
            marginTop: '14vh',
            padding: theme.spacing(2),
            textAlign: 'center',
            flexWrap: 'nowrap',
            transform: 'translateZ(0)',
            overflowY: 'hidden',
            overflowX:'hidden',
            zIndex:-1,
            fontSize:'1.2em',
            },

        Height:{
            height: '30vh',
        },

    }),
);


function LeftScroll() {
    document.getElementById('main-service-category').scrollTo(-700, 0);
}
function RightScroll() {
    document.getElementById('main-service-category').scrollLeft = 700;
}

function ServiceMainCategories() {
    const classes = useStyles();

    return (

        <div className={classes.root}>


            {/*  Scroll buttons */}
            <div id='scroll-buttons'>

                <Button onClick={LeftScroll} id='left-button' aria-label='scroll-left-button'>
                    <FontAwesomeIcon icon={faChevronLeft} />
                </Button>

                <Button onClick={RightScroll} id='right-button' aria-label='scroll-right-button'>
                    <FontAwesomeIcon icon={faChevronRight}/>
                </Button>
            </div>


            {/* Main service Category list */}
        <Grid  container spacing={1} item  className={classes.mainServices} id='main-service-category'>

             <Grid item xs={12} sm={6} md={4} >

               <Link to={'/laundry'} className={classes.link}>
                <Paper className={classes.paper} id='laundry'>
                    <FontAwesomeIcon size='2x' icon={faDryerAlt} color={'#e65c00'}/>
                  <div class="caption">Laundry</div>
                 </Paper>
               </Link>

             </Grid>

    <Grid item xs={12} sm={6} md={4}  >
        <Link to={'/cleaning'} className={classes.link}>
         <Paper className={classes.paper} id='cleaning'>

                <FontAwesomeIcon size='2x' icon={ faBroom } color={'#e65c00'}/>

             <div className="caption">Cleaning</div>

        </Paper>
        </Link>
    </Grid>

    <Grid item xs={12} sm={6} md={4}>
        <Link to={'/'} className={classes.link}>
        <Paper className={classes.paper} id='personal'>
                <FontAwesomeIcon size='2x' icon={faSpa} color={'#e65c00'}/>
            <div className="caption">Personal</div>

        </Paper>
        </Link>
    </Grid>

    <Grid item xs={12} sm={6} md={4} >
        <Link to={'/maintenance'} className={classes.link}>
        <Paper className={classes.paper} id='maintenance'>
                <FontAwesomeIcon size='2x' icon={faHammer} color={'#e65c00'}/>
            <div className="caption"> Maintenance</div>

        </Paper>
        </Link>
    </Grid>
    <Grid item xs={12} sm={6} md={4} >
        <Link to={'/pets'} className={classes.link}>
        <Paper className={classes.paper} id='pets'>
                <FontAwesomeIcon size='2x' icon={faDog} color={'#e65c00'}/>
            <div className="caption"> Pets</div>

        </Paper>
        </Link>
    </Grid>

    <Grid item xs={12} sm={6} md={4}>

        <Link to={'/products'} className={classes.link}>
        <Paper className={classes.paper} id='products'>
                <FontAwesomeIcon size='2x' icon={faShoppingCart} color={'#e65c00'}/>
            <div className="caption"> Products</div>
        </Paper>
        </Link>
    </Grid>

        <Grid item xs={12} sm={6} md={4} >
            <Link to={'/home-care'} className={classes.link}>
            <Paper className={classes.paper} id='home-care'>
                    <FontAwesomeIcon size='2x' icon={faHandsHelping} color={'#e65c00'}/>
                <div className="caption">Care</div>
            </Paper>
            </Link>
        </Grid>


        </Grid>

        </div>
);
}

export  default ServiceMainCategories;