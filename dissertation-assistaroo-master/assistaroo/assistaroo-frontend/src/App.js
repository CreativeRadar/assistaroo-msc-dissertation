import React, { Component } from 'react';
import './css/App.css';
import {Switch, Route} from 'react-router-dom';
import LaundrySubCategories from "./LaundrySubCategories";
import CleaningSubCategories from "./CleaningSubCategories";
import PersonalSubCategories from "./PersonalSubCategories";
import MaintenanceSubCategories from "./MaintenanceSubCategories";
import PetsSubCategories from "./PetsSubCategories";
import ProductsSubCategory from "./ProductsSubCategory";
import HomeCareSubCategories from "./HomeCareSubCategories";
import TreatmentsSubCategories from "./TreatmentsSubCategory";
import Test from "./Test";


 function App()  {

        return(

                <Switch>
                    <Route exact path='/' component={PersonalSubCategories}
                    />
                    <Route path='/laundry' component={LaundrySubCategories}  />
                    <Route path='/cleaning' component={CleaningSubCategories} />
                    <Route path='/maintenance' component={MaintenanceSubCategories} />
                    <Route path='/pets' component={PetsSubCategories} />
                    <Route path='/products' component={ProductsSubCategory} />
                    <Route path='/home-care' component={HomeCareSubCategories} />
                    <Route path='/treatments' component={TreatmentsSubCategories} />
                    <Route path='/supplierListtest' component={Test}/>
                </Switch>

        )



}


export default App;

{/* -- All images used(in the images folder) adopted from https://pixabay.com  --*/}