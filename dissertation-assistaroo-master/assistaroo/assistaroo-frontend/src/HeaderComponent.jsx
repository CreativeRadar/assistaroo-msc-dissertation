import React, { useEffect} from 'react';
import './css/App.css';
import {AppBar,Tabs,Tab} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import './css/customer.scss';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHomeLg, faPhoneSquareAlt} from "@fortawesome/pro-regular-svg-icons";


const useStyles = makeStyles(theme => ({

    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    button: {
        margin: theme.spacing(1),
    },
    appBar: {
        backgroundColor: '#e65c00',
        zIndex: 2,
        height: '5em',
        paddingLeft:'0.5em',


    },
    tabs:{

    }


    }));


export default function HeaderComponent(){
    const classes = useStyles();
     useEffect( () => {

        (function() {
            var cx = '005533193429344081122:3sb5gacn7me';
            var gcse = document.createElement('script');
            gcse.type = 'text/javascript';
            gcse.async = true;
            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gcse, s);
        })();

        window.onload = function(){
           let input = document.getElementById('gsc-i-id1');
            input.placeholder = 'Search...';
        };

    }, );


//Toggle Hamburger Menu Items
    function handleDisplay() {
      document.getElementById('mobile-menu-items').style.visibility ='visible';
    }
    function hideMenu(){
        document.getElementById('mobile-menu-items').style.visibility ='hidden';
    }


    return (
            <div>
<AppBar className={classes.appBar}>

    <Tabs textColor={'#fff'} className={'main-navbar'}>
        <h1 style={{fontSize:'1.7em', paddingRight:'0.5em'}}> <FontAwesomeIcon icon={faHomeLg} /> Assistaroo.com</h1>
        <Tab label="My Bookings"  className={'tabs'} style={{textTransform: 'none', fontSize:'1em',}} onClick={()=>window.location.href = "https://assistaroo-back-end.azurewebsites.net/customerBookingsCalendar"} />
        <Tab label="Account Details"  className={'tabs'} style={{textTransform: 'none',fontSize:'1em'}} onClick={()=>window.location.href = "https://assistaroo-back-end.azurewebsites.net/customer-details.html"}/>
        <Tab label="FAQ" className={'tabs'} style={{textTransform: 'none',fontSize:'1em'}} onClick={()=>window.location.href = "https://assistaroo-back-end.azurewebsites.net/faq.html"} />
    </Tabs>

    <span id={'mobile-menu'}>
         <span className={'nav'} onClick={handleDisplay}>
             <div className="bar1"></div>
             <div className="bar2"></div>
              <div className="bar3"></div>
        </span>

        <h1> <FontAwesomeIcon icon={faHomeLg} /> Assistaroo.com</h1>
        <div id={'mobile-menu-items'} onMouseLeave={hideMenu}>
            <ul>
                 <li onClick={()=>window.location.href = 'https://assistaroo-back-end.azurewebsites.net/customerBookingsCalendar'}>My Bookings</li>
                  <li onClick={()=>window.location.href = 'https://assistaroo-back-end.azurewebsites.net/customer-details.html'}>Account Details</li>
                   <li onClick={()=>window.location.href = 'https://assistaroo-back-end.azurewebsites.net/faq.html'}>FAQ</li>
            </ul>
        </div>

    <FontAwesomeIcon icon={faPhoneSquareAlt} size={'3x'} style={{marginLeft:'88%', top:'0.3em', position:'absolute'}} />
    </span>
</AppBar>

                {/* Google Custom Search Engine */}
            <span id={'search'}>
            <div className='gcse-search' linktarget="_self" ></div>
            </span>
            </div>

        );
    }









/*
REFERENCES

Navbar Code adapted from w3schools.com.

w3schools.com.2019.How To Create a menu icon.Available at: https://www.w3schools.com/howto/howto_css_menu_icon.asp
[Accessed: 28/08/2019]
 */