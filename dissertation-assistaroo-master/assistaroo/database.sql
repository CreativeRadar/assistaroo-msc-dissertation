SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS payments;
DROP TABLE IF EXISTS reviews;
DROP TABLE IF EXISTS bookings;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS addresses;
DROP TABLE IF EXISTS servicesOffered;
DROP TABLE IF EXISTS serviceHierarchy;
DROP TABLE IF EXISTS servicesOffered2;
DROP TABLE IF EXISTS services2;
DROP TABLE IF EXISTS services;
DROP TABLE IF EXISTS company_general_details;
DROP TABLE IF EXISTS financial_details;
DROP TABLE IF EXISTS company_account_details;
DROP TABLE IF EXISTS company_service_details;
DROP TABLE IF EXISTS suppliers;
DROP TABLE IF EXISTS names;
DROP TABLE IF EXISTS companies;

CREATE TABLE names
(
nameID int not null auto_increment,
firstName varchar (30),
lastName varchar(30),
title varchar(5),
CONSTRAINT names_PK PRIMARY KEY (nameID)
) ENGINE=INNODB;


CREATE TABLE addresses
(
addressID int not null auto_increment,
address varchar(50),
postcode varchar(20),
CONSTRAINT addresses_PK PRIMARY KEY (addressID)
) ENGINE=INNODB;


CREATE TABLE suppliers
(
supplierID int not null auto_increment,
DBSReferenceNumber varchar(50),
PublicLiabilityInsurance varchar(50),
areaOfOperation float,
hearFrom varchar(40),
nameID int,
CONSTRAINT suppliers_pk PRIMARY KEY (supplierID),
CONSTRAINT fk_names
      FOREIGN KEY (nameID)
      REFERENCES names(nameID)
  ) ENGINE=INNODB;


CREATE TABLE customers
(
customerID int not null auto_increment,
nameID int,
CONSTRAINT customers_pk PRIMARY KEY (customerID),
CONSTRAINT fk_names2
      FOREIGN KEY (nameID)
      REFERENCES names(nameID)
) ENGINE=INNODB;

DROP TABLE IF EXISTS companies;
CREATE TABLE companies
(
registrationNumber varchar(20) not null,
name varchar(40),
CONSTRAINT companies_pk PRIMARY KEY (registrationNumber)
) ENGINE=INNODB;
DROP TABLE IF EXISTS users;


CREATE TABLE users
(
userID int not null  auto_increment,
userName  varchar(30) unique,
password varchar(200),
salt mediumblob,
email varchar(30) unique,
isAdmin boolean,
phone varchar (20),
addressID int,
registrationNumber varchar(20),
customerID int,
supplierID int,
CONSTRAINT users_pk PRIMARY KEY (userID),
CONSTRAINT fk_addresses
       FOREIGN KEY (addressID)
       REFERENCES addresses(addressID),
CONSTRAINT fk_companies
       FOREIGN KEY (registrationNumber)
       REFERENCES companies(registrationNumber),
CONSTRAINT fk_customers
       FOREIGN KEY (customerID)
       REFERENCES customers(customerID),
CONSTRAINT fk_suppliers
      FOREIGN KEY (supplierID)
	  REFERENCES suppliers(supplierID)
) ENGINE=INNODB;


CREATE TABLE services
(
serviceID int(5) not null auto_increment,
name varchar(50),
servicePathString varchar(300),
imageUrl varchar(400),
CONSTRAINT services_pk PRIMARY KEY (serviceID)
) ENGINE=INNODB;

CREATE TABLE services2
(
serviceID int(5) not null auto_increment,
name varchar(50),
servicePathString varchar(300),
imageUrl varchar(400),
CONSTRAINT services_pk PRIMARY KEY (serviceID)
) ENGINE=INNODB;

CREATE table serviceHierarchy
(
serviceHierarchyID int not null auto_increment,
parentID int(5),
childID int(5),
CONSTRAINT serviceHierarchy_pk PRIMARY KEY (serviceHierarchyID),
CONSTRAINT fk_parentID
      FOREIGN KEY (parentID)
	  REFERENCES services(serviceID),
CONSTRAINT fk_childID
      FOREIGN KEY (childID)
	  REFERENCES services(serviceID)
) ENGINE=INNODB;

CREATE TABLE servicesOffered
(
serviceOfferedID int(5) not null auto_increment,
price float(10),
description varchar(400),
serviceID int,
supplierID int,
CONSTRAINT services_pk PRIMARY KEY (serviceOfferedID),
CONSTRAINT fk_services
      FOREIGN KEY (serviceID)
      REFERENCES services(serviceID),
 CONSTRAINT fk_suppliers2
      FOREIGN KEY (supplierID)
      REFERENCES suppliers(supplierID)
) ENGINE=INNODB;

CREATE TABLE servicesOffered2
(
serviceOfferedID int(5) not null auto_increment,
price float(10),
description varchar(400),
serviceID int,
supplierID int,
image_url varchar(200),
service_name varchar (200),
CONSTRAINT services_pk PRIMARY KEY (serviceOfferedID),
CONSTRAINT fk_services2
      FOREIGN KEY (serviceID)
      REFERENCES services(serviceID),
 CONSTRAINT fk_suppliers3
      FOREIGN KEY (supplierID)
      REFERENCES suppliers(supplierID)
) ENGINE=INNODB;


CREATE TABLE bookings
(
bookingID int not null,
startTime timestamp,
duration float,
endTime timestamp,
cancelledById int,
cancelledDateTime timestamp,
confirmedById int,
confirmedDateTime timestamp,
cancelledBookingMessage varchar(100),
serviceOfferedID int,
customerID int,
CONSTRAINT bookings_pk PRIMARY KEY (bookingID),
CONSTRAINT fk_servicesOffered
      FOREIGN KEY (serviceOfferedID)
      REFERENCES servicesOffered(serviceOfferedID),
CONSTRAINT fk_customers2
      FOREIGN KEY (customerID)
      REFERENCES customers(customerID),
CONSTRAINT fk_usersCancelledId
      FOREIGN KEY (cancelledById)
      REFERENCES users(userID),
CONSTRAINT fk_usersConfirmedId
      FOREIGN KEY (confirmedById)
      REFERENCES users(userID)
) ENGINE=INNODB;

CREATE TABLE reviews
(
reviewID int not null auto_increment,
reviewDateTime timestamp,
starRating float,
description varchar(400),
bookingID int,
CONSTRAINT reviews_pk PRIMARY KEY (reviewID),
CONSTRAINT fk_bookings
      FOREIGN KEY (bookingID)
      REFERENCES bookings(bookingID)
) ENGINE=INNODB;

CREATE TABLE payments
(
paymentID int not null auto_increment,
bookingID int,
status varchar(20),
chargeAmount float,
refundedDateTime timestamp,
CONSTRAINT payments_pk PRIMARY KEY (paymentID),
CONSTRAINT fk_bookings2
      FOREIGN KEY (bookingID)
      REFERENCES bookings(bookingID)
) ENGINE=INNODB;

ALTER TABLE payments
  MODIFY refundedDateTime TIMESTAMP NULL;
  
create table company_general_details(
    companyGeneralDetailsID int not null  auto_increment PRIMARY KEY,
    registrationNumber varchar(20),
    heading varchar(500) not null,
    content varchar(500)  not null,
    is_deleted tinyint not null
);
create table company_service_details(
    company_service_details_ID int not null  auto_increment PRIMARY KEY,
    registration_number varchar(20),
    main_services varchar(100) not null,
    sub_services varchar(100)  not null,
    areas_of_operation varchar(100)  not null,
    is_deleted tinyint not null, 
    FOREIGN KEY (registration_number) REFERENCES companies(registrationNumber)
);


create table financial_details(
    financial_details_ID int not null  auto_increment PRIMARY KEY,
    registration_number varchar(20),
    card_name varchar(100) not null,
    card_number varchar(100)  not null,
    card_expiry_date date  not null,
    is_deleted tinyint not null, 
    FOREIGN KEY (registration_number) REFERENCES companies(registrationNumber)
);
create table company_account_details(
    company_account_details_ID int not null  auto_increment PRIMARY KEY,
    registration_number varchar(20),
    username varchar(100) not null,
    email varchar(100)  not null,
    password varchar(100)  not null,
    is_deleted tinyint not null, 
    FOREIGN KEY (registration_number) REFERENCES companies(registrationNumber)
);  

INSERT INTO names VALUES(1,'Nikolaos','Skarpos','Mr');
INSERT INTO names VALUES(2,'Eme','Effiom','Miss');
INSERT INTO names VALUES(3,'Katie','Nass','Miss');
INSERT INTO names VALUES(4,'Jennifer','Peart','Miss');
INSERT INTO names VALUES(5,'Adam','Scotch','Mr');
INSERT INTO names VALUES(6,'John','Louis','Mr');
INSERT INTO names VALUES(7,'George','Jones','Mr');
INSERT INTO names VALUES(8,'Layton','Schneider','Mr');
INSERT INTO names VALUES(9,'Jedd','Todd','Mr');
INSERT INTO names VALUES(10,'Jerry','Holland','Mr');
INSERT INTO names VALUES(11,'Travis','Robson','Mr');
INSERT INTO names VALUES(12,'Arron','Chen','Mr');
INSERT INTO names VALUES(13,'Priscilla','Cotton','Miss');
INSERT INTO names VALUES(14,'Suzanne','May','Miss');
INSERT INTO names VALUES(15,'Anita','Nieves','Miss');
INSERT INTO names VALUES(16,'Camilla','Fischer','Miss');
INSERT INTO names VALUES(17,'Amara','Novak','Miss');
INSERT INTO names VALUES(18,'Elsa','Melia','Miss');
INSERT INTO names VALUES(19,'Stuart','Lu','Mr');
INSERT INTO names VALUES(20,'Elliot','Edmonds','Mr');


INSERT INTO addresses VALUES(1,'17 saint Peter Street','CF24 3BA');
INSERT INTO addresses VALUES(2,'23 Newport Road','CF16 3BA');
INSERT INTO addresses VALUES(3,'5 Cathays','CF52 5CF');
INSERT INTO addresses VALUES(4,'52 Mary Street','CF14 3CA');
INSERT INTO addresses VALUES(5,'32 Gordon Street','CN12 2FC');
INSERT INTO addresses VALUES(6,'3 Karytainis','AT24 2NA');
INSERT INTO addresses VALUES(7,'3 saint Peter Street','CF24 3BA');
INSERT INTO addresses VALUES(8,'59  Newgate Street','IV7 6ZF');
INSERT INTO addresses VALUES(9,'8  Wade Lane','KA21 2ED');
INSERT INTO addresses VALUES(10,'19  Front St','LL77 1DR');
INSERT INTO addresses VALUES(11,'128  Thames Street','SA37 9PW');
INSERT INTO addresses VALUES(12,'39  Argyll Road','LL65 1WH');
INSERT INTO addresses VALUES(13,'60  Cunnery Rd','RG42 6WS');
INSERT INTO addresses VALUES(14,'73  West Lane','IV17 8SX');
INSERT INTO addresses VALUES(15,'56  Holburn Lane','WV10 1ST');
INSERT INTO addresses VALUES(16,'68  Ponteland Rd','BD23 9GR');
INSERT INTO addresses VALUES(17,'120  Warren St','KT24 1JU');
INSERT INTO addresses VALUES(18,'63  Great Western Road','TF9 8TJ');
INSERT INTO addresses VALUES(19,'88  Cefn Road','AB55 9WA');
INSERT INTO addresses VALUES(20,'104  Fulford Road','SY10 7SE');
INSERT INTO addresses VALUES(21,'64  Helland Bridge','NG16 6UY');
INSERT INTO addresses VALUES(22,'49  Grey Street','DN21 1JT');
INSERT INTO addresses VALUES(23,'145  Rowland Rd','HU12 6TW');
INSERT INTO addresses VALUES(24,'131  Middlewich Road','YO6 6QN');
INSERT INTO addresses VALUES(25,'79  Foregate Street','TF8 1RX');


INSERT INTO suppliers VALUES(1,'524368215478','524156',35,'site1',1);
INSERT INTO suppliers VALUES(2,'441268215478','524216',35,'site1',2);
INSERT INTO suppliers VALUES(3,'214533369885','221123',35,'site1',3);
INSERT INTO suppliers VALUES(4,'222200147856','221478',35,'site1',4);
INSERT INTO suppliers VALUES(5,'222000336987','522356',35,'site1',5);
INSERT INTO suppliers VALUES(6,'555522369874','233304',35,'site1',6);
INSERT INTO suppliers VALUES(7,'114485203954','220147',35,'site1',7);
INSERT INTO suppliers VALUES(8,'445213698521','699852',35,'site1',8);
INSERT INTO suppliers VALUES(9,'220014789952','332654',35,'site1',9);
INSERT INTO suppliers VALUES(10,'036988547123','236985',35,'site1',10);


INSERT INTO customers(nameID) VALUES(11);
INSERT INTO customers(nameID) VALUES(12);
INSERT INTO customers(nameID) VALUES(13);
INSERT INTO customers(nameID) VALUES(14);
INSERT INTO customers(nameID) VALUES(15);
INSERT INTO customers(nameID) VALUES(16);
INSERT INTO customers(nameID) VALUES(17);
INSERT INTO customers(nameID) VALUES(18);
INSERT INTO customers(nameID) VALUES(19);
INSERT INTO customers(nameID) VALUES(20);
SELECT customers.nameID from customers where customers.customerID = last_insert_id();

INSERT INTO companies VALUES('01234567','Multi Tech Development');
INSERT INTO companies VALUES('12563248','Pro Garden Management');
INSERT INTO companies VALUES('01234214','Circuit City');
INSERT INTO companies VALUES('CE201356','Central Health');
INSERT INTO companies VALUES('SC853412','Gene Walter');


INSERT INTO users VALUES(1,'sgnick8','12345', "",'nikos@email.com',false,'2145236584',1,null ,1,null);
INSERT INTO users VALUES(2,'eme','12345',null,'eme@email.com',false,'5212369852',2,null ,2,null);
INSERT INTO users VALUES(3,'Katie','12345',null,'Katie@email.com',false,'0125632874',3,null ,3,null);
INSERT INTO users VALUES(4,'Jennifer','12345',null,'Jen@email.com',false,'2014478563',4,null ,4,null);
INSERT INTO users VALUES(5,'Adam','12345',null,'adam@email.com',false,'52314785201',5,null ,5,null);
INSERT INTO users VALUES(6,'John','12345',null,'johny@email.com',false,'3021456324',6,null ,6,null);
INSERT INTO users VALUES(7,'george','12345',null,'geo@email.com',false,'2145236985',7,null ,7,null);
INSERT INTO users VALUES(8,'layton','12345',null,'lay@email.com',false,'0014236985',8,null ,8,null);
INSERT INTO users VALUES(9,'jedd','12345',null,'jedd@email.com',false,'2140336584',9,null ,9,null);
INSERT INTO users VALUES(10,'jerry','12345',null,'jerry@email.com',false,'0136985421',10,null ,10,null);
INSERT INTO users VALUES(11,'travis','12345',null,'travis@email.com',false,'2103654785',11,null ,null,1);
INSERT INTO users VALUES(12,'arron','12345',null,'arron@email.com',false,'6521320147',12,null ,null,2);
INSERT INTO users VALUES(13,'pris','12345',null,'pris@email.com',false,'122014523682',13,null ,null,3);
INSERT INTO users VALUES(14,'suzzy','12345',null,'suz@email.com',false,'4963654785',14,null ,null,4);
INSERT INTO users VALUES(15,'anita','12345',null,'anita@email.com',false,'201364785',15,null ,null,5);
INSERT INTO users VALUES(16,'camila','12345',null,'camila@email.com',false,'8893654785',16,null ,null,6);
INSERT INTO users VALUES(17,'amara','12345',null,'amara@email.com',false,'2103653224',17,null ,null,7);
INSERT INTO users VALUES(18,'elsa','12345',null,'elsa@email.com',false,'9852146324',18,null ,null,8);
INSERT INTO users VALUES(19,'stu','12345',null,'stu@email.com',false,'3603654785',19,null ,null,9);
INSERT INTO users VALUES(20,'elliot','12345',null,'elli@email.com',false,'52144730269',20,null ,null,10);


INSERT INTO services VALUES(1,'Laundry','Laundry','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');
INSERT INTO services VALUES(2,'Cleaning','Cleaning','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQw-j56dzjAhUN1RoKHVGfDT0QjRx6BAgBEAU&url=http%3A%2F%2Fkitacleaning.co.uk%2Fhard-floor-cleaning%2F&psig=AOvVaw22MWiC_m5DERZwn6vwimX9&ust=1564582438139463');
INSERT INTO services VALUES(3,'Personal','Personal','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjcwrH_6tzjAhXV8uAKHVW0As0QjRx6BAgBEAU&url=https%3A%2F%2Fwww.sharecare.com%2Fhealth%2Fbeauty-personal-care&psig=AOvVaw0wZy1Cyl_Kez9ID9tQh2L1&ust=1564582723739806');
INSERT INTO services VALUES(4,'Maintenance','Maintenance','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjmoe6n7tzjAhUIRBoKHSVACkYQjRx6BAgBEAU&url=https%3A%2F%2Fwww.budgetdumpster.com%2Fblog%2Fget-your-home-ready-for-summer-with-these-15-summer-home-maintenance-tips%2F&psig=AOvVaw3JmC48pi5NrPQjMxPKs8p5&ust=1564583611092231');
INSERT INTO services VALUES(5,'Pet','Pet','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiTy_S879zjAhVQxoUKHRKZCaAQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fpet%2F&psig=AOvVaw05EaUM8bJ0i1IAEwlmwSx1&ust=1564583891960249');
INSERT INTO services VALUES(6,'Products','Products','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQvIja79zjAhU3AWMBHT_BBvsQjRx6BAgBEAU&url=https%3A%2F%2Fsumo.com%2Fstories%2Fproduct-photography-ideas&psig=AOvVaw2N2Lx4lmQmHQNCfAadv8nL&ust=1564583977010470');
INSERT INTO services VALUES(7,'Monitoring','Monitoring','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiai8j779zjAhVyAWMBHe16DG8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.apialarm.com%2Fresidential-security%2F24-hour-monitoring%2F&psig=AOvVaw2SMiVX_UHGxQAH2S5TTyWO&ust=1564584034096946');
INSERT INTO services VALUES(8,'Washing','Laundry -> Washing','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');
INSERT INTO services VALUES(9,'Ironing','Laundry -> Ironing','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQw-j56dzjAhUN1RoKHVGfDT0QjRx6BAgBEAU&url=http%3A%2F%2Fkitacleaning.co.uk%2Fhard-floor-cleaning%2F&psig=AOvVaw22MWiC_m5DERZwn6vwimX9&ust=1564582438139463');
INSERT INTO services VALUES(10,'Hair','Personal -> Hair','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjcwrH_6tzjAhXV8uAKHVW0As0QjRx6BAgBEAU&url=https%3A%2F%2Fwww.sharecare.com%2Fhealth%2Fbeauty-personal-care&psig=AOvVaw0wZy1Cyl_Kez9ID9tQh2L1&ust=1564582723739806');
INSERT INTO services VALUES(11,'Beauty','Personal -> Beauty','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjmoe6n7tzjAhUIRBoKHSVACkYQjRx6BAgBEAU&url=https%3A%2F%2Fwww.budgetdumpster.com%2Fblog%2Fget-your-home-ready-for-summer-with-these-15-summer-home-maintenance-tips%2F&psig=AOvVaw3JmC48pi5NrPQjMxPKs8p5&ust=1564583611092231');
INSERT INTO services VALUES(12,'Treatments','Personal -> Treatments','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiTy_S879zjAhVQxoUKHRKZCaAQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fpet%2F&psig=AOvVaw05EaUM8bJ0i1IAEwlmwSx1&ust=1564583891960249');
INSERT INTO services VALUES(13,'Home','Maintenance -> Home','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQvIja79zjAhU3AWMBHT_BBvsQjRx6BAgBEAU&url=https%3A%2F%2Fsumo.com%2Fstories%2Fproduct-photography-ideas&psig=AOvVaw2N2Lx4lmQmHQNCfAadv8nL&ust=1564583977010470');
INSERT INTO services VALUES(14,'Garden','Maintenance -> Garden','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiai8j779zjAhVyAWMBHe16DG8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.apialarm.com%2Fresidential-security%2F24-hour-monitoring%2F&psig=AOvVaw2SMiVX_UHGxQAH2S5TTyWO&ust=1564584034096946');
INSERT INTO services VALUES(15,'Window Cleaning','Maintenance -> Window Cleaning','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');
INSERT INTO services VALUES(16,'Grooming','Pet -> Grooming','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQw-j56dzjAhUN1RoKHVGfDT0QjRx6BAgBEAU&url=http%3A%2F%2Fkitacleaning.co.uk%2Fhard-floor-cleaning%2F&psig=AOvVaw22MWiC_m5DERZwn6vwimX9&ust=1564582438139463');
INSERT INTO services VALUES(17,'Walking','Pet -> Walking','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjcwrH_6tzjAhXV8uAKHVW0As0QjRx6BAgBEAU&url=https%3A%2F%2Fwww.sharecare.com%2Fhealth%2Fbeauty-personal-care&psig=AOvVaw0wZy1Cyl_Kez9ID9tQh2L1&ust=1564582723739806');
INSERT INTO services VALUES(18,'Therapy','Personal -> Treatments -> Therapy','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjmoe6n7tzjAhUIRBoKHSVACkYQjRx6BAgBEAU&url=https%3A%2F%2Fwww.budgetdumpster.com%2Fblog%2Fget-your-home-ready-for-summer-with-these-15-summer-home-maintenance-tips%2F&psig=AOvVaw3JmC48pi5NrPQjMxPKs8p5&ust=1564583611092231');
INSERT INTO services VALUES(19,'Chiropodist','Personal -> Treatments -> Chiropodist','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiTy_S879zjAhVQxoUKHRKZCaAQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fpet%2F&psig=AOvVaw05EaUM8bJ0i1IAEwlmwSx1&ust=1564583891960249');
INSERT INTO services VALUES(20,'Physiotherapy','Personal -> Treatments -> Physiotherapy','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQvIja79zjAhU3AWMBHT_BBvsQjRx6BAgBEAU&url=https%3A%2F%2Fsumo.com%2Fstories%2Fproduct-photography-ideas&psig=AOvVaw2N2Lx4lmQmHQNCfAadv8nL&ust=1564583977010470');

INSERT INTO services2 VALUES(1,'Laundry','Laundry','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');
INSERT INTO services2 VALUES(2,'Cleaning','Cleaning','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQw-j56dzjAhUN1RoKHVGfDT0QjRx6BAgBEAU&url=http%3A%2F%2Fkitacleaning.co.uk%2Fhard-floor-cleaning%2F&psig=AOvVaw22MWiC_m5DERZwn6vwimX9&ust=1564582438139463');
INSERT INTO services2 VALUES(3,'Personal','Personal','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjcwrH_6tzjAhXV8uAKHVW0As0QjRx6BAgBEAU&url=https%3A%2F%2Fwww.sharecare.com%2Fhealth%2Fbeauty-personal-care&psig=AOvVaw0wZy1Cyl_Kez9ID9tQh2L1&ust=1564582723739806');
INSERT INTO services2 VALUES(4,'Maintenance','Maintenance','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjmoe6n7tzjAhUIRBoKHSVACkYQjRx6BAgBEAU&url=https%3A%2F%2Fwww.budgetdumpster.com%2Fblog%2Fget-your-home-ready-for-summer-with-these-15-summer-home-maintenance-tips%2F&psig=AOvVaw3JmC48pi5NrPQjMxPKs8p5&ust=1564583611092231');
INSERT INTO services2 VALUES(5,'Pet','Pet','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiTy_S879zjAhVQxoUKHRKZCaAQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fpet%2F&psig=AOvVaw05EaUM8bJ0i1IAEwlmwSx1&ust=1564583891960249');
INSERT INTO services2 VALUES(6,'Products','Products','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQvIja79zjAhU3AWMBHT_BBvsQjRx6BAgBEAU&url=https%3A%2F%2Fsumo.com%2Fstories%2Fproduct-photography-ideas&psig=AOvVaw2N2Lx4lmQmHQNCfAadv8nL&ust=1564583977010470');
INSERT INTO services2 VALUES(7,'Monitoring','Monitoring','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiai8j779zjAhVyAWMBHe16DG8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.apialarm.com%2Fresidential-security%2F24-hour-monitoring%2F&psig=AOvVaw2SMiVX_UHGxQAH2S5TTyWO&ust=1564584034096946');
INSERT INTO services2 VALUES(8,'Care','Laundry -> Washing','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');


INSERT INTO serviceHierarchy VALUES(1,1,8);
INSERT INTO serviceHierarchy VALUES(2,1,9);
INSERT INTO serviceHierarchy VALUES(3,3,10);
INSERT INTO serviceHierarchy VALUES(4,3,11);
INSERT INTO serviceHierarchy VALUES(5,3,12);
INSERT INTO serviceHierarchy VALUES(6,4,13);
INSERT INTO serviceHierarchy VALUES(7,4,14);
INSERT INTO serviceHierarchy VALUES(8,4,15);
INSERT INTO serviceHierarchy VALUES(9,5,16);
INSERT INTO serviceHierarchy VALUES(10,5,17);
INSERT INTO serviceHierarchy VALUES(11,12,18);
INSERT INTO serviceHierarchy VALUES(12,12,19);
INSERT INTO serviceHierarchy VALUES(13,12,20);
INSERT INTO serviceHierarchy VALUES(14,null,1);
INSERT INTO serviceHierarchy VALUES(15,null,2);
INSERT INTO serviceHierarchy VALUES(16,null,3);
INSERT INTO serviceHierarchy VALUES(17,null,4);
INSERT INTO serviceHierarchy VALUES(18,null,5);
INSERT INTO serviceHierarchy VALUES(19,null,6);
INSERT INTO serviceHierarchy VALUES(20,null,7);

INSERT INTO servicesOffered VALUES(1,20.00,'wahsing',1,1);
INSERT INTO servicesOffered VALUES(2,20.00,'ironing',1,1);
INSERT INTO servicesOffered VALUES(3,20.00,'cleaning',2,2);
INSERT INTO servicesOffered VALUES(4,20.00,'hair',3,3);
INSERT INTO servicesOffered VALUES(5,20.00,'beauty',3,4);
INSERT INTO servicesOffered VALUES(6,20.00,'treatments',3,4);
INSERT INTO servicesOffered VALUES(7,20.00,'home',4,5);
INSERT INTO servicesOffered VALUES(8,20.00,'garden',4,5);
INSERT INTO servicesOffered VALUES(9,20.00,'windows',4,5);
INSERT INTO servicesOffered VALUES(10,20.00,'grooming',5,6);
INSERT INTO servicesOffered VALUES(11,20.00,'walking',1,7);
INSERT INTO servicesOffered VALUES(12,20.00,'products',1,8);
INSERT INTO servicesOffered VALUES(13,20.00,'monitoring',1,9);
INSERT INTO servicesOffered VALUES(14, 5.00, 'Quick', 8, 3);
INSERT INTO servicesOffered VALUES (15, 7.00, '40', 8, 3);
INSERT INTO servicesOffered VALUES (16, 7.00, 'Hot', 8, 3);
INSERT INTO servicesOffered VALUES (17, 6.00, 'Press', 9, 3);
INSERT INTO servicesOffered VALUES (18, 10.00, 'Steam', 9, 3);
INSERT INTO servicesOffered VALUES (19, NULL, NULL, 1, 3);
INSERT INTO servicesOffered VALUES(20, 4.00, 'Quick', 8, 11);
INSERT INTO servicesOffered VALUES (21, 8.00, '40', 8, 11);
INSERT INTO servicesOffered VALUES (22, 5.00, 'Hot', 8, 11);
INSERT INTO servicesOffered VALUES (23, 6.00, 'Press', 9, 11);
INSERT INTO servicesOffered VALUES (24, 9.00, 'Steam', 9, 11);
INSERT INTO servicesOffered VALUES (25, NULL, NULL, 1, 11);

INSERT INTO servicesOffered2 VALUES(1,20.00,'Washing',1,1,'/images/washing.jpg','Laundry');
INSERT INTO servicesOffered2 VALUES(2,20.00,'Ironing',1,1,'/images/ironing.jpg','Laundry');
INSERT INTO servicesOffered2 VALUES(3,20.00,'Cleaning',2,2,'/images/new-house-clean.jpg','Laundry');
INSERT INTO servicesOffered2 VALUES(4,20.00,'Hair',3,3,'/images/hair.jpg','Personal');
INSERT INTO servicesOffered2 VALUES(5,20.00,'Beauty',3,4,'/images/beauty.jpg','Personal');
INSERT INTO servicesOffered2 VALUES(6,20.00,'Treatments',3,4,'/images/treatments.jpg','Personal');
INSERT INTO servicesOffered2 VALUES(7,20.00,'Home',4,5,'/images/home.jpg','Personal');
INSERT INTO servicesOffered2 VALUES(8,20.00,'Garden',4,5,'/images/garden-maintenance.jpg','Maintenance');
INSERT INTO servicesOffered2 VALUES(9,20.00,'Window cleaning',4,5,'/images/window-cleaning.jpg','Maintenance');
INSERT INTO servicesOffered2 VALUES(10,20.00,'Grooming',5,6,'/images/dog-grooming.jpg','Pets');
INSERT INTO servicesOffered2 VALUES(11,20.00,'Walking',5,7,'/images/dog-walking.jpg','Pets');
INSERT INTO servicesOffered2 VALUES(12,20.00,'Vacum Cleaners',6,5,'/images/product1.jpg','Products');
INSERT INTO servicesOffered2 VALUES(13,20,'Mobility Chairs',6,5,'/images/product2.png','Products');
INSERT INTO servicesOffered2 VALUES(14,20.00,'Care',7,9,'/images/care1.jpg','Care');

INSERT INTO bookings VALUES(1,'2019-01-01 11:20:00',1.00,'2019-01-01 12:20:00',null,null,null,null,'',1,1);
INSERT INTO bookings VALUES(2,'2019-03-01 00:00:01',1.00,'2019-03-01 01:00:01',null,null,null,null,'',3,2);
INSERT INTO bookings VALUES(3,'2019-04-01 00:00:01',1.5,'2019-04-01 01:30:01',null,null,null,null,'',4,5);
INSERT INTO bookings VALUES(4,'2019-01-11 21:00:00',2.00,'2019-01-11 23:00:00',null,null,null,null,'',5,1);
INSERT INTO bookings VALUES(5,'2019-02-12 00:00:01',0.5,'2019-02-12 00:30:01',null,null,null,null,'',5,3);
INSERT INTO bookings VALUES(6,'2019-09-02 11:20:00',1.00,'2019-01-01 12:20:00',null,null,null,null,'',20,1);
INSERT INTO bookings VALUES(7,'2019-10-01 00:00:01',1.00,'2019-03-01 01:00:01',null,null,null,null,'',21,2);
INSERT INTO bookings VALUES(8,'2019-08-01 00:00:01',1.5,'2019-04-01 01:30:01',null,null,null,null,'',22,5);
INSERT INTO bookings VALUES(9,'2019-09-11 21:00:00',2.00,'2019-01-11 23:00:00',null,null,null,null,'',23,1);
INSERT INTO bookings VALUES(10,'2019-08-12 00:00:01',0.5,'2019-02-12 00:30:01',null,null,null,null,'',24,3);

INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-01-01 00:00:01',4.00,'Very professional services.',1);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-03-01 00:00:01',1.00,'I felt i was robbed of my time.',2);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-04-01 00:00:01',3.00,'I was pleased with the services but the supplier took way too long to finish.',3);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-06-01 00:00:01',5.00,'Thank god for this site and its suppliers.',4);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-11-01 00:00:01',1.00,'This was one of the worst trades in the history of trade deals.',5);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-01-01 00:00:01',4.00,'Very professional services.',6);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-03-01 00:00:01',1.00,'I felt i was robbed of my time.',7);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-04-01 00:00:01',3.00,'I was pleased with the services but the supplier took way too long to finish.',8);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-06-01 00:00:01',5.00,'Thank god for this site and its suppliers.',9);
INSERT INTO reviews(reviewDateTime,starRating,description,bookingID) VALUES('2019-11-01 00:00:01',1.00,'This was one of the worst trades in the history of trade deals.',10);

INSERT INTO payments VALUES(1,1,'paid',20.0,null);
INSERT INTO payments VALUES(2,2,'paid',20.0,null);
INSERT INTO payments VALUES(3,3,'paid',30.0,null);
INSERT INTO payments VALUES(4,4,'refunded',40.0,'2019-11-11 15:33:01');
INSERT INTO payments VALUES(5,5,'in process',10.0,null);

INSERT INTO company_general_details(registrationNumber,heading,content,is_deleted)
VALUES('01234214','Email','new@assistaroo.com',0);

INSERT INTO company_service_details(registration_number,main_services,sub_services,areas_of_operation,is_deleted) 
VALUES('01234214','Laundry','ironing','HQ344df',0);
INSERT INTO company_service_details(registration_number,main_services,sub_services,areas_of_operation,is_deleted) 
VALUES('01234214','Laundry','washing','Hg4567v',0);

INSERT INTO financial_details(registration_number,card_name,card_number,card_expiry_date,is_deleted) 
VALUES('01234214','Eve Nile','123456783456','2022-12-17',0);

INSERT INTO company_account_details(registration_number,username,email,password,is_deleted) 
VALUES('01234214','Eve','eve@lll.com','1234',0);
INSERT INTO company_account_details(registration_number,username,email,password,is_deleted) 
VALUES('01234214','nill','nill@lll.com','1234',0);

SET FOREIGN_KEY_CHECKS = 1;