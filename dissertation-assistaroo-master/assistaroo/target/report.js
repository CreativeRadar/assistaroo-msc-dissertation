$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/feature/About-usPage.feature");
formatter.feature({
  "name": "Access about-us page",
  "description": "  A user can navigate to the about us page",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Access about-us page on route /about-us.html",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "a user navigates to \"/about-us.html\"",
  "keyword": "When "
});
formatter.match({
  "location": "AboutUsPageStepDef.a_user_navigates_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the about us page must be opened",
  "keyword": "Then "
});
formatter.match({
  "location": "AboutUsPageStepDef.the_about_us_page_must_be_opened()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response should contain \"Assistaroo | About-us\"",
  "keyword": "And "
});
formatter.match({
  "location": "AboutUsPageStepDef.the_response_should_contain_elements(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/resources/feature/AccessCompanyDetails.feature");
formatter.feature({
  "name": "Access Supplier Details page",
  "description": "    A Supplier can navigate to the details page",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": ": Access Company Details by {registration number}",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "a company supplier goes to the details page based on his \"\u003cregistration_number\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "the supplier should be directed to the details page with his \"\u003cregistration_number\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "the details html page should be rendered with all suppliers \"\u003cdetails\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "registration_number",
        "details"
      ]
    },
    {
      "cells": [
        "01234214",
        "Laundromart"
      ]
    },
    {
      "cells": [
        "01234567",
        "Multi Tech Development"
      ]
    }
  ]
});
formatter.scenario({
  "name": ": Access Company Details by {registration number}",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "a company supplier goes to the details page based on his \"01234214\"",
  "keyword": "When "
});
formatter.match({
  "location": "AccessCompanyDetailStepDefs.a_company_supplier_goes_to_the_details_page_based_on_his(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the supplier should be directed to the details page with his \"01234214\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AccessCompanyDetailStepDefs.the_supplier_should_be_directed_to_the_details_page_with_his(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the details html page should be rendered with all suppliers \"Laundromart\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AccessCompanyDetailStepDefs.the_details_html_page_should_be_rendered_with_all_suppliers(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": ": Access Company Details by {registration number}",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "a company supplier goes to the details page based on his \"01234567\"",
  "keyword": "When "
});
formatter.match({
  "location": "AccessCompanyDetailStepDefs.a_company_supplier_goes_to_the_details_page_based_on_his(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the supplier should be directed to the details page with his \"01234567\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AccessCompanyDetailStepDefs.the_supplier_should_be_directed_to_the_details_page_with_his(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the details html page should be rendered with all suppliers \"Multi Tech Development\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AccessCompanyDetailStepDefs.the_details_html_page_should_be_rendered_with_all_suppliers(String)"
});
formatter.result({
  "status": "passed"
});
});