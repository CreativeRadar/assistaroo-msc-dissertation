/**** Details UI ****/
$(document).ready(function() {
    let personal_actions = $("#personal-details-table td:last-child").html();
    let billing_actions = $("#billing-details-table td:last-child").html();

// Append Personal Details table element on add-personal-detail button click
    $("#add-personal-detail").click(function () {

        $(this).attr("disabled", "disabled");

        let row = '<tr>' +
            '<td id="input-heading"><input type="text" class="form-control" name="detail-heading"  id="heading"></td>' +
            '<td><input type="text" class="form-control" name="detail-content" id="content"></td>' +
            '<td>' + personal_actions + '</td>' +
            '</tr>';


        $('#personal-details-table tr:last').after(row);
        $("#personal-details-table tbody tr:last").find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();

    });


// Add row on add button click
    $(document).on("click", ".add", function () {
        let empty = false;
        let input = $(this).parents("tr").find('input[type="text"]');
        input.each(function () {
            if (!$(this).val()) {
                $(this).addClass("error");
                empty = true;
            } else {
                $(this).removeClass("error");
            }
        });
        $(this).parents("tr").find(".error").first().focus();
        if (!empty) {
            input.each(function () {
                $(this).parent("td").html($(this).val());
            });
            $(this).parents("tr").find(".add, .edit").toggle();
            $(".add-new").removeAttr("disabled");
        }
    });
// Edit row on edit button click
    $(document).on("click", ".edit", function () {
        $(this).parents("tr").find("td:not(:last-child)").each(function () {
            $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
        });
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");
    });
// Delete row on delete button click
    $(document).on("click", ".delete", function () {
        $(this).parents("tr").remove();
        $(".add-new").removeAttr("disabled");
    });

// Append Financal details table on add-credit-card button click
    $("#add-credit-card").click(function () {
        $(this).attr("disabled", "disabled");

        let row = '<tr>' +
            '<td><input type="text" class="form-control"  name="card-name" id="card-name"></td>' +
            '<td><input type="text" class="form-control" name="card-number" id="card-number"></td>' +
            '<td><input type="date" class="form-control" name="expiry-date" id="expiry-date"></td>' +
            '<td>' + billing_actions + '</td>' +
            '</tr>';


        $("#billing-details-table tr:last").after(row);
        $("#billing-details-table tbody tr:last").find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });
});





/*--
REFERENCES:
Code adapted from
Tutorial Republic.2019.Bootstrap Table with Add and Delete Row Feature.
Available at: https://www.tutorialrepublic.com/snippets/preview.php?topic=bootstrap&file=table-with-add-and-delete-row-feature
Accessed[07/08/2019]
and modified to suit project needs
*/
