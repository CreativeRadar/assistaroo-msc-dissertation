$(document).ready(function() {
    $('.update').hide();

    $('[data-toggle="tooltip"]').tooltip();
    let general_actions = $("#general-details-table td:last-child").html();
    let service_actions = $("#company-service-details-table td:last-child").html();
    let billing_actions = $("#billing-details-table td:last-child").html();
    let supplier_actions = $("#supplier-details-table td:last-child").html();


    // Add Table UI row on add button click
    $(document).on("click", ".add", function () {
        let empty = false;
        let input = $(this).parents("tr").find('input[type="text"]');
        input.each(function () {
            if (!$(this).val()) {
                $(this).addClass("error");
                empty = true;
            } else {
                $(this).removeClass("error");
            }
        });
        $(this).parents("tr").find(".error").first().focus();
        if (!empty) {
            input.each(function () {
                $(this).parent("td").html($(this).val());
            });

            $(this).parents("tr").find(".add, .edit").toggle();
            $(".add-new").removeAttr("disabled");
        }

    });

    //Table Input UI row on Update button Click
    $(document).on("click", ".update", function () {
        let empty = false;
        let input = $(this).parents("tr").find('input[type="text"]');
        input.each(function () {
            if (!$(this).val()) {
                $(this).addClass("error");
                empty = true;
            } else {
                $(this).removeClass("error");
            }
        });
        $(this).parents("tr").find(".error").first().focus();
        if (!empty) {
            input.each(function () {
                $(this).parent("td").html($(this).val());
            });

            $(this).parents("tr").find(".update, .edit").toggle();
            $(".add-new").removeAttr("disabled");
        }

    });

    /**** General Information ****/

    // Append General Details table element on add-general-detail button click
    $("#add-new-general-detail").click(function () {

        $(this).attr("disabled", "disabled");

        let row = '<tr>' +
            '<td id="input-heading"><input type="text" class="form-control" name="heading"  id="heading"></td>' +
            '<td><input type="text" class="form-control" name="content" id="content"></td>' +
            '<td>' + general_actions + '</td>' +
            '</tr>';



        $('#general-details-table tr:last').after(row);
        $("#general-details-table tbody tr:last").find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();

    });


    //Delete Company General Detail
    $(document).on("click", "#delete-company-general-detail", function () {

        let companyGeneralDetailsID = $(this).closest('td').attr('data-element-id');
        let url= '/delete-general-detail/' + companyGeneralDetailsID;
        console.log(companyGeneralDetailsID)
        if (confirm("Are you sure you want to delete this Item")) {

            //Show preloader icon
            $('.preloader').css("visibility","visible");

            (async () => {
                const response = await fetch(url, {
                    method: 'POST',
                });
                const responseData = await response.json();
                if (responseData == 1) {

                    //Hide Preloader
                    $('.preloader').css("visibility","hidden");

                    //Display system feedback
                    $('#response-message').prepend('<div class="alert alert-heading' +
                        ' alert-success alert-dismissible fade show">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        "Record Successfully Deleted" + '</div>'
                    );

                } else {
                    $('#response-message').prepend('<div class="alert alert-heading ' +
                        'alert-danger alert-dismissible fade-show">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        "Record Delete Failed " + '</div>'
                    );

                }
                console.log(responseData);


            })();

        }
        $(this).closest('tr').remove();

    });


    //Edit General Details
    $(document).on("click", "#edit-company-general-detail", function () {

        $(this).parents("tr").find("td:not(:last-child)").each(function () {

            //add input fields to each table data
            $(this).html('<input type="text" class="form-control a"  id="" value="' + $(this).text() + '">')
            $( '.a' ).each(function(index) {
                console.log(index);
                $(this).attr('id',index)
            });
            window.gen_heading = $('#0').val();
            window.gen_content = $('#1').val();
        });
        $(this).parents("tr").find(".update, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");

    });



    //Ajax Request for Updating Company General Details Record
    $(document).on("click", "#update-company-general-detail", function () {

        //Retrieve the input field value
        let registrationNumber = $(this).attr('data-reg-num');
        let heading = gen_heading;
        let detail =  gen_content;
        let id =$(this).closest("td").attr('data-element-id');


        (async () => {
            let data = {'registrationNumber':registrationNumber, 'heading': heading, 'content': detail,'companyGeneralDetailsID':id}

            const response = await fetch('/update-general-information', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });

            const responseData = await response.json();
            console.log(responseData);

            if(responseData == 1){
                $('#response-message').prepend('<div class="alert alert-heading' +
                    ' alert-success alert-dismissible fade show">' +
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                    "Record Successfully Updated" + '</div>'
                );
            }
            else {
                $('#response-message').prepend('<div class="alert alert-heading ' +
                    'alert-danger alert-dismissible fade-show">' +
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                    "Record Update Failed " + '</div>'
                );
            }



        })();


    });





    /**** Company Service Details *****/


    // Append to Service details table on add service detail button click
    $("#add-company-service-detail").click(function () {
        $(this).attr("disabled", "disabled");
        let row = '<tr>' +
            '<td><input type="text" class="form-control"  name="main-services" id="main-services"></td>' +
            '<td><input type="text" class="form-control"  name="sub-services" id="sub-services"></td>' +
            '<td><input type="text" class="form-control" name="areas-of-operation" id="areas-of-operation"></td>' +
            '<td>' + service_actions + '</td>' +
            '</tr>';


        $("#company-service-details-table tr:last").after(row);
        $("#company-service-details-table tbody tr:last").find(".add, .edit").toggle();
        $('.update').hide();
        $('[data-toggle="tooltip"]').tooltip();
    });


    //Edit Company Service Details on button click
    $(document).on("click", "#edit-company-service-details", function () {

        $(this).parents("tr").find("td:not(:last-child)").each(function () {

            //add input fields to each table data
            $(this).html('<input type="text" class="form-control a"  id="" value="' + $(this).text() + '">')
            $( '.a' ).each(function(index) {
                console.log(index);
                $(this).attr('id',index)
            });
            window.gen_heading = $('#0').val();
            window.gen_content = $('#1').val();
        });
        $(this).parents("tr").find(".update, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");

    });


    //Delete Company Service Detail
    $(document).on("click", "#delete-company-service-detail", function () {

        //Show Preloader
        $('.preloader').css("visibility","visible");

        let company_service_detail_ID = $(this).closest('td').attr('id');
        let url= '/delete-service-detail/' + company_service_detail_ID ;
        console.log(company_service_detail_ID)
        if (confirm("Are you sure you want to delete this Item")) {
            (async () => {
                const response = await fetch(url, {
                    method: 'DELETE',
                });
                const responseData = await response.json();
                if (responseData == 1) {

                    //Hide Preloader
                    $('.preloader').css("visibility","hidden");

                    $('#response-message').prepend('<div class="alert alert-heading' +
                        ' alert-success alert-dismissible fade show">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        "Record Successfully Deleted" + '</div>'
                    );

                }
                else {
                    $('#response-message').prepend('<div class="alert alert-heading ' +
                        'alert-danger alert-dismissible fade-show">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                        "Record Delete Failed " + '</div>'
                    );

                }
                console.log(responseData);


            })();
        }
        $(this).closest('tr').remove();

    });


    /**** Financial Details ****/

    // Append Financal details table on add-credit-card button click
    $("#add-credit-card").click(function () {
        $(this).attr("disabled", "disabled");

        let row = '<tr>' +
            '<td><input type="text" class="form-control"  name="card-name" id="card-name"></td>' +
            '<td><input type="text" class="form-control" name="card-number" id="card-number"></td>' +
            '<td><input type="date" class="form-control" name="expiry-date" id="expiry-date"></td>' +
            '<td>' + billing_actions + '</td>' +
            '</tr>';


        $("#billing-details-table tr:last").after(row);
        $("#billing-details-table tbody tr:last").find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });


    //Edit Financial Service Details on button click
    $(document).on("click", "#edit-financial-details", function () {

        $(this).parents("tr").find("td:not(:last-child)").each(function () {

            //add input fields to each table data
            $(this).html('<input type="text" class="form-control a"  id="" value="' + $(this).text() + '">')
            $( '.a' ).each(function(index) {
                console.log(index);
                $(this).attr('id',index)
            });
            window.new_ = $('#0').val();
            window.gen_content = $('#1').val();
        });
        $(this).parents("tr").find(".update, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");

    });


    //Delete Financial Details on click
    $(document).on("click", "#delete-financial-details", function () {

        let financial_details_ID = $(this).closest('td').attr('id');
        let url= '/delete-financial-detail/' + financial_details_ID ;
        console.log(financial_details_ID);


        if (confirm("Are you sure you want to delete this Item")) {
            (async () => {
                const response = await fetch(url, {
                    method: 'DELETE',
                });
                const responseData = await response.json();
                if (responseData == 1) {


                    $('#response-message').prepend('<div class="alert alert-heading' +
                        ' alert-success alert-dismissible fade show">' +
                        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        "Record Successfully Deleted" + '</div>'
                    );

                }
                console.log(responseData);


            })();
        }
        $(this).closest('tr').remove();

    });


    /**** Account Details ****/
    //Edit Company Account Details on edit icon click
    $(document).on("click", "#edit-company-account-details", function () {

        $(this).parents("tr").find("td:not(:last-child)").each(function () {

            //add input fields to each table data
            $(this).html('<input type="text" class="form-control a"  id="" value="' + $(this).text() + '">')
            $( '.a' ).each(function(index) {
                console.log(index);
                $(this).attr('id',index)
            });
            window.username = $('#0').val();
            window.email = $('#1').val();
            window.email = $('#1').val();
        });
        $(this).parents("tr").find(".update, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");

    });



    /**** Company's Supplier List ****/
    // Append Supplier details table on add-supplier-details button click
    $("#add-supplier-details").click(function () {
        $(this).attr("disabled", "disabled");
        let index = $("#supplier-details-table tbody tr:last-child").index();
        let row = '<tr>' +
            '<td><input type="text" class="form-control"  name="title" id="title"></td>' +
            '<td><input type="text" class="form-control"  name="first-name" id="first-name"></td>' +
            '<td><input type="text" class="form-control"  name="last-name" id="last-name"></td>' +
            '<td><input type="email" class="form-control"  name="email" id="email"></td>' +
            '<td><input type="text" class="form-control"  name="title" id="title"></td>' +
            '<td><input type="number" class="form-control"  name="contact-number" id="contact-number"></td>' +
            '<td><input type="text" class="form-control"  name="main-service" id="main-service"></td>' +
            '<td><input type="text" class="form-control"  name="sub-service" id="sub-service"></td>' +
            '<td><input type="text" class="form-control"  name="AOP" id="AOP"></td>' +
            '<td><input type="text" class="form-control"  name="others" id="others"></td>' +
            '<td>' + service_actions + '</td>' +
            '</tr>';
        $("#supplier-details-table").append(row);
        $("#supplier-details-table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });


    // Edit Company's Supplier Details on edit button click
    $(document).on("click", "#edit-supplier-details", function () {
        $(this).parents("tr").find("td:not(:last-child)").each(function () {
            $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
        });
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");
    });



});


/*--
REFERENCES:
Code adapted from
Tutorial Republic.2019.Bootstrap Table with Add and Delete Row Feature.
Available at: https://www.tutorialrepublic.com/snippets/preview.php?topic=bootstrap&file=table-with-add-and-delete-row-feature
Accessed[07/08/2019]
and modified to suit project needs
*/

