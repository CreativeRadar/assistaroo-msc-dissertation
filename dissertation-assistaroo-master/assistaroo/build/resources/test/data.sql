INSERT INTO names VALUES(1,'Travis','Robson','Mr');
INSERT INTO names VALUES(2,'Arron','Chen','Mr');
INSERT INTO names VALUES(3,'Tom','Jones','Mr');
INSERT INTO names VALUES(4,'Harry','Baker','Mr');
INSERT INTO names VALUES(5,'Sheila','Thomas','Mrs');

INSERT INTO addresses VALUES(1,'128  Thames Street','SA37 9PW');
INSERT INTO addresses VALUES(2,'39  Argyll Road','LL65 1WH');

INSERT INTO suppliers VALUES(1,'524368215478','524156',3);
INSERT INTO suppliers VALUES(2,'441268215478','524216',4);
INSERT INTO suppliers VALUES(3,'587468215478','658916',5);

INSERT INTO customers VALUES(1,1);
INSERT INTO customers VALUES(2,2);

INSERT INTO users VALUES(1,'travis','12345','travis@email.com',false,'2103654785',1,null ,1,null);
INSERT INTO users VALUES(2,'arron','12345','arron@email.com',false,'6521320147',2,null ,2,null);
INSERT INTO users VALUES(3,'t_jones','02568','tom.jones@gmail.com',false,'02356854745',2,null,null,1);


INSERT INTO services VALUES(1,'Laundry','Laundry','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');
INSERT INTO services VALUES(2,'Cleaning','Cleaning','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQw-j56dzjAhUN1RoKHVGfDT0QjRx6BAgBEAU&url=http%3A%2F%2Fkitacleaning.co.uk%2Fhard-floor-cleaning%2F&psig=AOvVaw22MWiC_m5DERZwn6vwimX9&ust=1564582438139463');
INSERT INTO services VALUES(3,'Personal','Personal','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjcwrH_6tzjAhXV8uAKHVW0As0QjRx6BAgBEAU&url=https%3A%2F%2Fwww.sharecare.com%2Fhealth%2Fbeauty-personal-care&psig=AOvVaw0wZy1Cyl_Kez9ID9tQh2L1&ust=1564582723739806');
INSERT INTO services VALUES(4,'Maintenance','Maintenance','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjmoe6n7tzjAhUIRBoKHSVACkYQjRx6BAgBEAU&url=https%3A%2F%2Fwww.budgetdumpster.com%2Fblog%2Fget-your-home-ready-for-summer-with-these-15-summer-home-maintenance-tips%2F&psig=AOvVaw3JmC48pi5NrPQjMxPKs8p5&ust=1564583611092231');
INSERT INTO services VALUES(5,'Pet','Pet','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiTy_S879zjAhVQxoUKHRKZCaAQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fpet%2F&psig=AOvVaw05EaUM8bJ0i1IAEwlmwSx1&ust=1564583891960249');
INSERT INTO services VALUES(6,'Products','Products','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQvIja79zjAhU3AWMBHT_BBvsQjRx6BAgBEAU&url=https%3A%2F%2Fsumo.com%2Fstories%2Fproduct-photography-ideas&psig=AOvVaw2N2Lx4lmQmHQNCfAadv8nL&ust=1564583977010470');
INSERT INTO services VALUES(7,'Monitoring','Monitoring','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiai8j779zjAhVyAWMBHe16DG8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.apialarm.com%2Fresidential-security%2F24-hour-monitoring%2F&psig=AOvVaw2SMiVX_UHGxQAH2S5TTyWO&ust=1564584034096946');
INSERT INTO services VALUES(8,'Washing','Laundry -> Washing','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');
INSERT INTO services VALUES(9,'Ironing','Laundry -> Ironing','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQw-j56dzjAhUN1RoKHVGfDT0QjRx6BAgBEAU&url=http%3A%2F%2Fkitacleaning.co.uk%2Fhard-floor-cleaning%2F&psig=AOvVaw22MWiC_m5DERZwn6vwimX9&ust=1564582438139463');
INSERT INTO services VALUES(10,'Hair','Personal -> Hair','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjcwrH_6tzjAhXV8uAKHVW0As0QjRx6BAgBEAU&url=https%3A%2F%2Fwww.sharecare.com%2Fhealth%2Fbeauty-personal-care&psig=AOvVaw0wZy1Cyl_Kez9ID9tQh2L1&ust=1564582723739806');
INSERT INTO services VALUES(11,'Beauty','Personal -> Beauty','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjmoe6n7tzjAhUIRBoKHSVACkYQjRx6BAgBEAU&url=https%3A%2F%2Fwww.budgetdumpster.com%2Fblog%2Fget-your-home-ready-for-summer-with-these-15-summer-home-maintenance-tips%2F&psig=AOvVaw3JmC48pi5NrPQjMxPKs8p5&ust=1564583611092231');
INSERT INTO services VALUES(12,'Treatments','Personal -> Treatments','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiTy_S879zjAhVQxoUKHRKZCaAQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fpet%2F&psig=AOvVaw05EaUM8bJ0i1IAEwlmwSx1&ust=1564583891960249');
INSERT INTO services VALUES(13,'Home','Maintenance -> Home','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQvIja79zjAhU3AWMBHT_BBvsQjRx6BAgBEAU&url=https%3A%2F%2Fsumo.com%2Fstories%2Fproduct-photography-ideas&psig=AOvVaw2N2Lx4lmQmHQNCfAadv8nL&ust=1564583977010470');
INSERT INTO services VALUES(14,'Garden','Maintenance -> Garden','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiai8j779zjAhVyAWMBHe16DG8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.apialarm.com%2Fresidential-security%2F24-hour-monitoring%2F&psig=AOvVaw2SMiVX_UHGxQAH2S5TTyWO&ust=1564584034096946');
INSERT INTO services VALUES(15,'Window Cleaning','Maintenance -> Window Cleaning','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');
INSERT INTO services VALUES(16,'Grooming','Pet -> Grooming','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQw-j56dzjAhUN1RoKHVGfDT0QjRx6BAgBEAU&url=http%3A%2F%2Fkitacleaning.co.uk%2Fhard-floor-cleaning%2F&psig=AOvVaw22MWiC_m5DERZwn6vwimX9&ust=1564582438139463');
INSERT INTO services VALUES(17,'Walking','Pet -> Walking','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjcwrH_6tzjAhXV8uAKHVW0As0QjRx6BAgBEAU&url=https%3A%2F%2Fwww.sharecare.com%2Fhealth%2Fbeauty-personal-care&psig=AOvVaw0wZy1Cyl_Kez9ID9tQh2L1&ust=1564582723739806');
INSERT INTO services VALUES(18,'Therapy','Personal -> Treatments -> Therapy','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjmoe6n7tzjAhUIRBoKHSVACkYQjRx6BAgBEAU&url=https%3A%2F%2Fwww.budgetdumpster.com%2Fblog%2Fget-your-home-ready-for-summer-with-these-15-summer-home-maintenance-tips%2F&psig=AOvVaw3JmC48pi5NrPQjMxPKs8p5&ust=1564583611092231');
INSERT INTO services VALUES(19,'Chiropodist','Personal -> Treatments -> Chiropodist','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiTy_S879zjAhVQxoUKHRKZCaAQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fpet%2F&psig=AOvVaw05EaUM8bJ0i1IAEwlmwSx1&ust=1564583891960249');
INSERT INTO services VALUES(20,'Physiotherapy','Personal -> Treatments -> Physiotherapy','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQvIja79zjAhU3AWMBHT_BBvsQjRx6BAgBEAU&url=https%3A%2F%2Fsumo.com%2Fstories%2Fproduct-photography-ideas&psig=AOvVaw2N2Lx4lmQmHQNCfAadv8nL&ust=1564583977010470');


INSERT INTO services2 VALUES(1,'Laundry','Laundry','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');
INSERT INTO services2 VALUES(2,'Cleaning','Cleaning','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQw-j56dzjAhUN1RoKHVGfDT0QjRx6BAgBEAU&url=http%3A%2F%2Fkitacleaning.co.uk%2Fhard-floor-cleaning%2F&psig=AOvVaw22MWiC_m5DERZwn6vwimX9&ust=1564582438139463');
INSERT INTO services2 VALUES(3,'Personal','Personal','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjcwrH_6tzjAhXV8uAKHVW0As0QjRx6BAgBEAU&url=https%3A%2F%2Fwww.sharecare.com%2Fhealth%2Fbeauty-personal-care&psig=AOvVaw0wZy1Cyl_Kez9ID9tQh2L1&ust=1564582723739806');
INSERT INTO services2 VALUES(4,'Maintenance','Maintenance','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjmoe6n7tzjAhUIRBoKHSVACkYQjRx6BAgBEAU&url=https%3A%2F%2Fwww.budgetdumpster.com%2Fblog%2Fget-your-home-ready-for-summer-with-these-15-summer-home-maintenance-tips%2F&psig=AOvVaw3JmC48pi5NrPQjMxPKs8p5&ust=1564583611092231');
INSERT INTO services2 VALUES(5,'Pet','Pet','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiTy_S879zjAhVQxoUKHRKZCaAQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Fpet%2F&psig=AOvVaw05EaUM8bJ0i1IAEwlmwSx1&ust=1564583891960249');
INSERT INTO services2 VALUES(6,'Products','Products','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiQvIja79zjAhU3AWMBHT_BBvsQjRx6BAgBEAU&url=https%3A%2F%2Fsumo.com%2Fstories%2Fproduct-photography-ideas&psig=AOvVaw2N2Lx4lmQmHQNCfAadv8nL&ust=1564583977010470');
INSERT INTO services2 VALUES(7,'Monitoring','Monitoring','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiai8j779zjAhVyAWMBHe16DG8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.apialarm.com%2Fresidential-security%2F24-hour-monitoring%2F&psig=AOvVaw2SMiVX_UHGxQAH2S5TTyWO&ust=1564584034096946');
INSERT INTO services2 VALUES(8,'Care','Laundry -> Washing','https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjSgJ206dzjAhUFmRoKHRzSBs8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.rd.com%2Fhome%2Fcleaning-organizing%2Fhow-to-do-laundry%2F&psig=AOvVaw0a9IVfRWKZrQjdRRmHKRyZ&ust=1564582300123805');



-- POPULATE SERVICE HIERARCHY TABLE --

INSERT INTO serviceHierarchy VALUES(1,1,8);
INSERT INTO serviceHierarchy VALUES(2,1,9);
INSERT INTO serviceHierarchy VALUES(3,3,10);
INSERT INTO serviceHierarchy VALUES(4,3,11);
INSERT INTO serviceHierarchy VALUES(5,3,12);
INSERT INTO serviceHierarchy VALUES(6,4,13);
INSERT INTO serviceHierarchy VALUES(7,4,14);
INSERT INTO serviceHierarchy VALUES(8,4,15);
INSERT INTO serviceHierarchy VALUES(9,5,16);
INSERT INTO serviceHierarchy VALUES(10,5,17);
INSERT INTO serviceHierarchy VALUES(11,12,18);
INSERT INTO serviceHierarchy VALUES(12,12,19);
INSERT INTO serviceHierarchy VALUES(13,12,20);
INSERT INTO serviceHierarchy VALUES(14,null,1);
INSERT INTO serviceHierarchy VALUES(15,null,2);
INSERT INTO serviceHierarchy VALUES(16,null,3);
INSERT INTO serviceHierarchy VALUES(17,null,4);
INSERT INTO serviceHierarchy VALUES(18,null,5);
INSERT INTO serviceHierarchy VALUES(19,null,6);
INSERT INTO serviceHierarchy VALUES(20,null,7);


INSERT INTO servicesOffered VALUES(1,20.00,'washing',1,1);
INSERT INTO servicesOffered VALUES(2,20.00,'ironing',1,1);
INSERT INTO servicesOffered VALUES(3,20.00,'cleaning',2,2);
INSERT INTO servicesOffered VALUES(4,20.00,'hair',3,3);


INSERT INTO servicesOffered2 VALUES(1,20.00,'washing',1,1,'/images/washing.jpg','Laundry');
INSERT INTO servicesOffered2 VALUES(2,20.00,'ironing',1,1,'/images/ironing.jpg','Laundry');
INSERT INTO servicesOffered2 VALUES(3,20.00,'cleaning',2,2,'/images/new-house-clean.jpg','Laundry');
INSERT INTO servicesOffered2 VALUES(4,20.00,'hair',3,3,'/images/hair.jpg','Personal');
INSERT INTO servicesOffered2 VALUES(5,20.00,'beauty',3,3,'/images/beauty.jpg','Personal');
INSERT INTO servicesOffered2 VALUES(6,20.00,'treatments',3,3,'/images/treatments.jpg','Personal');
INSERT INTO servicesOffered2 VALUES(7,20.00,'home',3,3,'/images/home.jpg','Personal');
INSERT INTO servicesOffered2 VALUES(8,20.00,'garden',4,4,'/images/garden-maintenance.jpg','Maintenance');
INSERT INTO servicesOffered2 VALUES(9,20.00,'window cleaning',4,4,'/images/window-cleaning.jpg','Maintenance');
INSERT INTO servicesOffered2 VALUES(10,20.00,'grooming',5,5,'/images/dog-grooming.jpg','Pets');
INSERT INTO servicesOffered2 VALUES(11,20.00,'walking',5,5,'/images/dog-walking.jpg','Pets');
INSERT INTO servicesOffered2 VALUES(12,20.00,'vacum Cleaners',6,6,'/images/product1.jpg','Products');
INSERT INTO servicesOffered2 VALUES(13,20,'mobility Chairs',6,6,'/images/product2.png','Products');
INSERT INTO servicesOffered2 VALUES(14,20.00,'care',7,7,'/images/care1.jpg','Care');
INSERT INTO servicesOffered2 VALUES(15,20.00,'therapy',8,8,'/images/therapist.png','Treatments');
INSERT INTO servicesOffered2 VALUES(16,20.00,'chiropody',8,8,'/images/chiropodist.jpg','Treatments');
INSERT INTO servicesOffered2 VALUES(17,20.00,'physiotherapy',8,8,'/images/physiotherapist.png','Treatments');




INSERT INTO bookings VALUES(1,'2019-08-07','2019-08-07 09:00:00',1.00,1,1);
INSERT INTO bookings VALUES(2,'2019-08-08','2019-08-08 12:40:00',2.00,2,2);
INSERT INTO bookings VALUES(3,'2019-08-08','2019-08-08 09:00:00',1.00,1,1);
INSERT INTO bookings VALUES(4,'2019-08-10','2019-08-10 15:00:00',3.00,2,1);

-- Populate Companies Table
INSERT INTO companies VALUES('01234567','Multi Tech Development');
INSERT INTO companies VALUES('12563248','Pro Garden Management');
INSERT INTO companies VALUES('01234214','Circuit City');
INSERT INTO companies VALUES('CE201356','Central Health');
INSERT INTO companies VALUES('SC853412','Gene Walter');

--Populate all Details Tables

insert into company_general_details(registrationNumber,heading,content,is_deleted)
values('01234214','Name','Laundry Hub',0);
values('01234214','Email','new@assistaroo.com',0);

insert into company_service_details(registration_number,main_services,sub_services,areas_of_operation,is_deleted)
values('01234214','Laundry','ironing','HQ344df',0);
insert into company_service_details(registration_number,main_services,sub_services,areas_of_operation,is_deleted)
values('01234214','Laundry','washing','Hg4567v',0);

insert into financial_details(registration_number,card_name,card_number,card_expiry_date,is_deleted)
values('01234214','Eve Nile','123456783456','2022-12-17',0);

insert into company_account_details(registration_number,username,email,password,is_deleted)
values('01234214','Eve','eve@lll.com','1234',0);
insert into company_account_details(registration_number,username,email,password,is_deleted)
values('01234214','nill','nill@lll.com','1234',0);

insert into supplier_areas_of_operation(areas_of_operation,supplier_id)
values('np5672ds',1);
insert into supplier_areas_of_operation(areas_of_operation,supplier_id)
values('lu34g5h',2);
insert into supplier_areas_of_operation(areas_of_operation,supplier_id)
values('ty7767n',3);