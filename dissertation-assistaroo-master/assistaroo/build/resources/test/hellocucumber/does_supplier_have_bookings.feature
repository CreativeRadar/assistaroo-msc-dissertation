# (cucumber.io, 2019)
Feature: Does a supplier have bookings?
  A supplier wants to know if they have bookings.

  Scenario: No bookings should be returned if a supplier doesn't have any
    Given a supplier exists
    And the supplier doesn't have bookings
    When I ask whether a supplier has bookings
    Then I should be told the supplier has no bookings

  #  References:
  #  cucumber.io. 2019. 10 Minute Tutorial. Available at: https://cucumber.io/docs/guides/10-minute-tutorial/
  #  [Accessed: 09 August 2019].  I have adapted the code to create a feature, scenario and scenario steps that are used in
  #  the cucumber tests.
