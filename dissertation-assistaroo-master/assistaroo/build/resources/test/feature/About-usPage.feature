
Feature: Access about-us page
  A Supplier can navigate to the about us page

  Scenario:  Access about-us page on route /about-us.html
    When a user navigates to the "about us" page
    Then the user must receive the status code of 200
    And the response should contain "html" elements




