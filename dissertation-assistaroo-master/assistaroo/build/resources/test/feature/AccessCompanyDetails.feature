
  Feature: Access Supplier Details page
    A Supplier can navigate to the details page

    Scenario Outline: : Access Company Details by {registration number}
      When a company supplier goes to the details page based on his "<registration_number>"
      Then the client receives status code of 200
      And the details html page should be rendered with all suppliers "<details>"
  Examples:

  |registration_number|details|
  |01234214           |Laundromart|
  |01234567         |Multi Tech Development|


      #REFERENCES
      # Chandrasekaran, A.Set Up and Run Cucumber Tests In Spring Boot Application.
      #2017.Available at:https://medium.com/@bcarunmail/set-up-and-run-cucumber-tests-in-spring-boot-application-d0c149d26220.
      #Accessed[30/08/2019]