package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.CompanyDetailsDTO;

public interface CompanyDetailsRepository {
    public CompanyDetailsDTO findCompanyByRegistrationNumber(String registration_number);
}
