package com.nsa.msc.dissertation.repository;


import com.nsa.msc.dissertation.dto.CustomerDTO;
import com.nsa.msc.dissertation.model.CustomerRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
    public class DisplayCustomerRepositoryJDBC implements DisplayCustomerRepository {

        private JdbcTemplate jdbcTemplate;

        @Autowired
        public DisplayCustomerRepositoryJDBC(JdbcTemplate Template) {
            this.jdbcTemplate = Template;
        }


        public CustomerDTO findCustomerByID(int customerID){
            CustomerRowMapper customerRowMapper = new CustomerRowMapper();
            return jdbcTemplate.queryForObject("SELECT * FROM customers c  \n"+
                            "WHERE c.customerID= (?)",
                    new Object[]{customerID},
                    customerRowMapper);
        }

    }




