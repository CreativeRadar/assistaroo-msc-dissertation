package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.ServiceDTO;
import com.nsa.msc.dissertation.dto.ServicesDTO;

import java.util.List;

public interface ServiceRepositoryInterface {
    public List<ServiceDTO> findAllServiceSubCategories();
    public Object findById(int serviceOfferedID);
    public List<ServiceDTO> findByParentCategory(String service_name);
    public List<ServiceDTO> findAllLaundrySubCategories();
    public List<ServiceDTO> findAllCleaningSubCategories();
    public List<ServiceDTO> findAllPersonalSubCategories();
    public List<ServiceDTO> findAllMaintenanceSubCategories();
    public List<ServiceDTO> findAllPetsSubCategories();
    public List<ServiceDTO> findAllProductsSubCategories();
    public List<ServiceDTO> findAllHomeCareSubCategories();
    public List<ServicesDTO> getAllMainServices(); //returns all Services from DB that have no parent service
    public List<ServicesDTO> getAllLeafServices(); //returns all services that have no child services
    public List<ServiceDTO> findAllTreatmentsSubCategories();
    public List<ServicesDTO> getServicesOfferedBySupplierId(int supplierID);
}
