package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.dto.BookingDTO;
import com.nsa.msc.dissertation.dto.EventDTO;
import com.nsa.msc.dissertation.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class SupplierRESTController {

    SupplierRepository supplierRepository;

    @Autowired
    public SupplierRESTController(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    @GetMapping("/supplierBookings/{supplier_id}") // Returns a JSON feed of a supplier's bookings from the database
    List<BookingDTO> supplierBookings(@PathVariable int supplier_id) {
        return supplierRepository.findASuppliersBookings(supplier_id);
    }

    @GetMapping("/supplierEvents/{supplier_id}") // Used to provide the JSON feed to a supplier's bookings calendar
    ArrayList<EventDTO> supplierEvents(@PathVariable int supplier_id) {
        return supplierRepository.findASuppliersEvents(supplier_id);
    }

}
