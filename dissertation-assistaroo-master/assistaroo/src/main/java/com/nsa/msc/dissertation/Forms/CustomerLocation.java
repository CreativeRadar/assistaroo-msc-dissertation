package com.nsa.msc.dissertation.Forms;

public class CustomerLocation {

    private String postcode;
    private int serviceID;

    public CustomerLocation(String postcode, int serviceID) {
        this.postcode = postcode;
        this.serviceID = serviceID;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }
}