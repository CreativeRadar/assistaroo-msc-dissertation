
package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.*;
import com.nsa.msc.dissertation.Forms.CustomerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Repository
public class CustomerRepositoryJDBC implements CustomerRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CustomerRepositoryJDBC(JdbcTemplate aTemplate) {
        jdbcTemplate = aTemplate;
    }

    @Override
    public List<CustomerDTO> findAll() {
        return jdbcTemplate.query("select addresses.address, addresses.postcode, users.email, users.phone, users.username, users.password, customers.customerID, names.title, names.firstName, names.lastName from customers JOIN names ON customers.nameID=names.nameID join users on users.customerID=customers.customerID join addresses on addresses.addressID=users.addressID",
                new Object[]{},
                (rs, i) -> new CustomerDTO(
                        rs.getString("address"),
                        rs.getString("postcode"),
                        rs.getString("email"),
                        rs.getString("phone"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getInt("customerID"),
                        rs.getString("title"),
                        rs.getString("firstName"),
                        rs.getString("lastName")
                ));
    }

    @Override
    @Transactional
    public int addCustomer (CustomerForm customerForm) {
        //variables for storing the update queries
        int statusNames;
        int statusAddresses;
        int statusCustomers;
        int statusUsers;
        String checkUsernameExistsQuery = "select username from users where username='" + customerForm.getUsername() + "'";
        try {
            String queriedUsername = (String) jdbcTemplate.queryForObject(checkUsernameExistsQuery, String.class);
            if(!queriedUsername.equals("")){
                return 3;
            }
        } catch (Exception e) {
        }
        //insert first name, last name and title of the customer into the names table
        statusNames = jdbcTemplate.update("insert into names(firstName, lastName, title)  values(?,?,?)",
                new Object[]{customerForm.getFname(),customerForm.getLname(),customerForm.getTitle()}
        );

        //insert the nameID foreign key to the customers table. use the last_insert_id() MySQL function to refer to
        //the last primary key inserted, this case the nameID created in the previous insert query
        statusCustomers = jdbcTemplate.update("insert into customers(nameID)  values(LAST_INSERT_ID())");

        //store the customers table primary key using last_insert_id() to use it later in the users table insert query
        String sql = "select customers.customerID from customers where customers.customerID = LAST_INSERT_ID()";
        Integer customer_id = jdbcTemplate.queryForObject(sql, Integer.class);

        //insert the customer's address and postcode in the addresses table
        statusAddresses = jdbcTemplate.update("insert into addresses(address, postcode)   values(?,?)",
                new Object[]{customerForm.getAddress(),customerForm.getPostcode()}
        );

        //store the address table primary key and use it in the users insert query
        sql = "select addresses.addressID from addresses where addresses.addressID = LAST_INSERT_ID()";
        Integer address_id = jdbcTemplate.queryForObject(sql, Integer.class);

        //insert all remaining information of the customer along with the 2 foreign keys addressID and customerID
        //stored previously in the users table
        Object[] passwordAndSalt = get_SHA_1_SecurePassword(customerForm.getPassword(), true);
        String securePassword = (String) passwordAndSalt[0];
        byte[] salt = (byte[])passwordAndSalt[1];
        System.out.println(passwordAndSalt[0].toString());
        System.out.println(passwordAndSalt[1].toString());

        System.out.println(customerForm.getPassword() + " hashes to " + securePassword);
        statusUsers = jdbcTemplate.update("insert into users(username, password, salt, email, isAdmin, phone, addressID, registrationNumber, customerID, supplierID)   values(?,?,?,?,?,?,?,?,?,?)",
                new Object[]{customerForm.getUsername(), securePassword, salt, customerForm.getEmail(), null, customerForm.getPhone(), address_id, null, customer_id, null  }
        );

        // this if/else clause is not related to the JDBC @Transactional behaviour of the addCustomer method.
        // It is only used to relay information to the CustomerController class to redirect either to a
        // registration succesfull page or a registration failure page
        //if ALL insert queries were carried out succesfully, return 1
        if((statusNames == 1) && (statusAddresses == 1) && (statusCustomers == 1) && (statusUsers == 1)){
            return 1;
        } else { //else return 2, "error" code.
            return 2;
        }
    }

    @Override
    public Object[] loginform (String username, String password) {
        String saltQuery = "select salt from users where username='" + username + "'";
        String storedHashQuery = "select password from users where username='" + username + "'";
        String customerIDQuery = "select customerID from users where username='" + username + "'";
        String supplierIDQuery = "select supplierID from users where username='" + username + "'";
        String storedHash;
        byte[] salt;
        Integer customer_id;
        Integer supplier_id;

        try {
            salt = (byte[]) jdbcTemplate.queryForObject(saltQuery, byte[].class);
            storedHash = (String) jdbcTemplate.queryForObject(storedHashQuery, String.class);
            customer_id = (Integer) jdbcTemplate.queryForObject(customerIDQuery, Integer.class);
            supplier_id = (Integer) jdbcTemplate.queryForObject(supplierIDQuery, Integer.class);
        } catch (Exception e) {
            return null;
        }
        boolean verified = verify_SHA_1_SecurePassword(storedHash, password, salt);
        try {
            if(verified) {
                return new Object[]{username,customer_id,supplier_id};
            } else{
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    private static Object[] get_SHA_1_SecurePassword(String passwordToHash, boolean doSalt)
    {
        String generatedPassword = null;
        String saltHexString = null;
        Object[] returnObjects = new Object[2];
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            if(doSalt){
                byte[] salt = getSalt();
                md.update(salt);
                returnObjects[1] = salt;
                System.out.println(returnObjects[1].toString());
            }
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
            returnObjects[0] = generatedPassword;
            System.out.println(returnObjects[0].toString());
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return returnObjects;
    }

    private static boolean verify_SHA_1_SecurePassword(String hashToVerify, String inputPassword, byte[] salt)
    {
        String generatedHash = null;
        String saltHexString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt);
            byte[] bytes = md.digest(inputPassword.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedHash = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return hashToVerify.equals(generatedHash);
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    @Override
    public List<EventDTO> findASuppliersEventsCustomerCalendar(int supplier_id){
        return jdbcTemplate.query("SELECT bookingID,startTime start_date_time,duration " +
                        "FROM bookings b\n" +
                        "JOIN servicesOffered so ON so.serviceOfferedID = b.serviceOfferedID\n" +
                        "JOIN suppliers su ON su.supplierID = so.supplierID\n" +
                        "WHERE su.supplierID = ?", new Object[]{supplier_id},
                (rs, i) ->  new EventDTO(
                        rs.getInt("bookingID"),
                        new Date(rs.getTimestamp("start_date_time").getTime()).toInstant().toString(), // Convert SQL timestamp to Date object
                        new Date(rs.getTimestamp("start_date_time").getTime()).toInstant().plus((long)rs.getFloat("duration"), ChronoUnit.HOURS).toString()
            ));
    }

    @Override
    public List<ServicesDTO> getMainServicesForCustomerCalendar(int supplier_id){
        return jdbcTemplate.query("SELECT DISTINCT name main_service FROM services s\n" +
                        "JOIN servicesOffered so ON so.serviceID = s.serviceID  \n" +
                        "WHERE s.serviceID IN (1,2,3,4,5,6,7) AND supplierID = ?\n" +
                        "ORDER BY name", new Object[]{supplier_id},
                (rs, i) ->  new ServicesDTO(
                        rs.getString("main_service")
                ));

    }

    @Override
    public List<ServicesDTO> getSubServicesForCustomerCalendar(String main_service, String supplier_id){

        int supplier_id_int = Integer.parseInt(supplier_id); // (mkyong.com, 2015)

        return jdbcTemplate.query("SELECT DISTINCT s.name sub_service FROM serviceHierarchy sh\n" +
                        "JOIN services s ON s.serviceID = sh.childID\n" +
                        "JOIN servicesOffered so ON so.serviceID = s.serviceID\n" +
                        "JOIN (SELECT sh.childID FROM services s JOIN serviceHierarchy sh ON sh.parentID = s.serviceID\n" +
                        "WHERE name=? ) main_service ON main_service.childID = s.serviceID\n" +
                        "WHERE so.supplierID=?\n" +
                        "ORDER BY s.name",
                new Object[]{main_service, supplier_id_int},
                (rs, i) ->  new ServicesDTO(
                        rs.getString("sub_service")
                ));

    }

    @Override
    public List<ServicesOfferedDTO> getServicesOfferedAndPricesForCustomerCalendar(String sub_service, String supplier_id){

        int supplier_id_int = Integer.parseInt(supplier_id); // (mkyong.com, 2015)

        return jdbcTemplate.query("SELECT DISTINCT name sub_service, so.description service_offered, so.price \n" +
                        "FROM services s\n" +
                        "JOIN serviceHierarchy sh ON sh.childID = s.serviceID\n" +
                        "JOIN servicesOffered so ON so.serviceID = s.serviceID\n" +
                        "WHERE name=? AND so.supplierID=?\n" +
                        "ORDER BY name",
                new Object[]{sub_service, supplier_id_int},
                (rs, i) ->  new ServicesOfferedDTO(
                        rs.getString("service_offered"),
                        rs.getFloat("price")
                ));
    }

    @Override
    public int createBooking(BookingDTO booking, HttpSession httpSession, int supplier_id){

        return jdbcTemplate.update("INSERT INTO bookings(bookingID,startTime,duration,endTime,serviceOfferedID, " +
                        "customerID) SELECT MAX(bookingID)+1,?,1.0,?, \n" +
                        "(SELECT serviceOfferedID FROM servicesOffered WHERE supplierID=? AND description=?) " +
                        "serviceOfferedID, ? FROM bookings b\n" +
                        "JOIN servicesOffered so ON so.serviceOfferedID=b.serviceOfferedID;",
                new Object[]{new java.sql.Timestamp(booking.getStartTime().getTime()),
                        new java.sql.Timestamp(Date.from(booking.getStartTime().toInstant().plus((long)1.0,
                                ChronoUnit.HOURS)).getTime()), supplier_id,
                        booking.getDescription(),httpSession.getAttribute("customerID")
                });
    }

}



/* References:
   mkyong.com. 2015. Java – Convert String to int – Mkyong.com. Available at:
   https://www.mkyong.com/java/java-convert-string-to-int/ [Accessed: 27 August 2019].  I have used method parseInt()
   to convert String supplier_id to an int.
 */

