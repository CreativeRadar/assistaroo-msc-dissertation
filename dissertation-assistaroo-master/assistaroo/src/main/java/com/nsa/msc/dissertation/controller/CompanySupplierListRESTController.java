package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.dto.CompanySupplierListDTO;
import com.nsa.msc.dissertation.repository.CompanySupplierListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CompanySupplierListRESTController {


    CompanySupplierListRepository companySupplierListRepository;

    @Autowired
    public CompanySupplierListRESTController(CompanySupplierListRepository companySupplierListRepository) {
        this.companySupplierListRepository = companySupplierListRepository;
    }




    @GetMapping(path = "/test/{registration_number}")
    public List<CompanySupplierListDTO> saveNewServiceDetail(@PathVariable String registration_number) {
        return companySupplierListRepository.findSupplierListByRegistrationNumber(registration_number);

    }


}