package com.nsa.msc.dissertation.dto;

import lombok.Data;

    @Data
    public class ChargeRequest {

        public enum Currency {
            GBP, USD;
        }

        private String description;
        private int amount;
        private Currency currency;
        private String stripeEmail;
        private String stripeToken;

        public ChargeRequest(String description, int amount, Currency currency, String stripeEmail, String stripeToken) {
            this.description = description;
            this.amount = amount;
            this.currency = currency;
            this.stripeEmail = stripeEmail;
            this.stripeToken = stripeToken;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public Currency getCurrency() {
            return currency;
        }

        public void setCurrency(Currency currency) {
            this.currency = currency;
        }

        public String getStripeEmail() {
            return stripeEmail;
        }

        public void setStripeEmail(String stripeEmail) {
            this.stripeEmail = stripeEmail;
        }

        public String getStripeToken() {
            return stripeToken;
        }

        public void setStripeToken(String stripeToken) {
            this.stripeToken = stripeToken;
        }

    }


    /*
    REFERENCES:
    Baeldung.2019.Introduction to the Stripe API for Java.Available at: https://www.baeldung.com/java-stripe-api.
    Accessed[24/08/2019]
 */