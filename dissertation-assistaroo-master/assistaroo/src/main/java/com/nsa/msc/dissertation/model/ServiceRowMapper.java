package com.nsa.msc.dissertation.model;

import java.sql.SQLException;

import com.nsa.msc.dissertation.dto.ServiceDTO;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;


    public class ServiceRowMapper implements RowMapper<ServiceDTO> {
        @Override
        public ServiceDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new ServiceDTO(
                    rs.getInt("serviceOfferedID"),
                    rs.getInt("price"),
                    rs.getString("description"),
                    rs.getInt("serviceID"),
                    rs.getInt("supplierID"),
                    rs.getString("image_url"),
                    rs.getString("service_name")
            );

        }
    }

