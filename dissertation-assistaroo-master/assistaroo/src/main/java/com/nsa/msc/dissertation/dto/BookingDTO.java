package com.nsa.msc.dissertation.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

public class BookingDTO {

    private int booking_id;
    private Date start_date_time;
    private float duration;
    private String service;
    private SupplierDTO supplier;
    private CustomerDTO customer;
    private Date startTime;
    private String description; //service offered

    public BookingDTO(int booking_id, Date start_date_time, float duration, String service, SupplierDTO supplier, CustomerDTO customer) {
        this.booking_id = booking_id;
        this.start_date_time = start_date_time;
        this.duration = duration;
        this.service = service;
        this.supplier = supplier;
        this.customer = customer;
    }

    public BookingDTO() {
    }

    public BookingDTO(Date startTime, String description) {
        this.startTime = startTime;
        this.description = description;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public Date getStart_date_time() {
        return start_date_time;
    }

    public void setStart_date_time(Date start_date_time) {
        this.start_date_time = start_date_time;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public SupplierDTO getSupplier() {
        return supplier;
    }

    public void setSupplier(SupplierDTO supplier) {
        this.supplier = supplier;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }
}
