package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.Forms.SaveAccountDetailsForm;
import com.nsa.msc.dissertation.repository.CompanyAccountDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompanyAccountDetailsRESTController {

    CompanyAccountDetailsRepository companyAccountDetailsRepo;


    @Autowired
    public CompanyAccountDetailsRESTController(CompanyAccountDetailsRepository companyAccountDetailsRepo) {
        this.companyAccountDetailsRepo = companyAccountDetailsRepo;
    }


    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, path = "/update-account-detail")
    public int updateAccountDetails(@RequestBody SaveAccountDetailsForm saveAccountDetailsForm) {
        companyAccountDetailsRepo.updateCompanyAccountDetail(saveAccountDetailsForm);
        System.out.println("Record Updated!");
        return 1;

    }



}