package com.nsa.msc.dissertation.dto;

public class EventDTO { // Class attributes match fields that are required for the Full Calendar API
    private int event_id;
    private String title;
    private String client;
    private String address;
    private String postcode;
    private String start;
    private String end;
    private String extra_information;
    private String customer_email;
    private String customer_phone_number;
    private String secret_code;
    private String service;
 //   private String sub_service;

    public EventDTO(int event_id, String title, String client, String address, String postcode, String start, String end,
                    String extra_information, String customer_email, String customer_phone_number, String secret_code,
                    String service) {
          this.event_id = event_id;
          this.title = title;
          this.client = client;
          this.address = address;
          this.postcode = postcode;
          this.start = start;
          this.end = end;
          this.extra_information = extra_information;
          this.customer_email = customer_email;
          this.customer_phone_number = customer_phone_number;
          this.secret_code = secret_code;
          this.service = service;
    }

    public EventDTO(int event_id, String start, String end) {
        this.event_id = event_id;
        this.start = start;
        this.end = end;
    }

    public int getEvent_id() {
        return event_id;
    }

    public String getTitle() {
        return title;
    }

    public String getClient() {
        return client;
    }

    public String getAddress() {
        return address;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getExtra_information() {
        return extra_information;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public String getCustomer_phone_number() {
        return customer_phone_number;
    }

    public String getSecret_code() {
        return secret_code;
    }

    public String getService() {
        return service;
    }

    /*
    public String getSub_service() {
        return sub_service;
    }

    public void setSub_service(String sub_service) {
        this.sub_service = sub_service;
    }
     */
}
