package com.nsa.msc.dissertation.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;


@Controller
public class landingController {

    @GetMapping("/contact")
    public String services() {
        return "../static/contact.html";
    }

    @GetMapping("/assistaroo")
    public String assistaroo(HttpSession session) {

        String isLogged = (String) session.getAttribute("isLoggedIn");
        String usernameLoggedIn = (String) session.getAttribute("username");
        String userType = (String) session.getAttribute("userType");
        int supplierId = (Integer) session.getAttribute("supplierID");
        //ModelAndView mv = new ModelAndView();
        //Map model = mv.getModel();
        System.out.println("Assistaroo is loggedin" + isLogged);
        System.out.println(usernameLoggedIn);
        if(isLogged == "yes"){
            if(userType == "supplier"){
                //model.put("supplierID",supplierId);
                return "redirect:/supplierHome/" + supplierId;
            } else if(userType == "customer") {
                return "redirect:/";
            } else {
                return "../static/landinghomepage.html";
            }
        }

            return "../static/landinghomepage.html";
    }

    @GetMapping("/")
    public String home() {
        return "../static/landinghomepage.html";
    }

    @GetMapping("/termsandconditions")
    public String termsandconditions() {
        return "../static/termsandconditions.html";
    }

}