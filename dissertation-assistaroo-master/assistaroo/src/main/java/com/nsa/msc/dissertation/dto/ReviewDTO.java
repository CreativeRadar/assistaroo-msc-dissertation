package com.nsa.msc.dissertation.dto;

public class ReviewDTO {

    private int reviewID;
    private String reviewDateTime;
    private float starRating;
    private String description;
    private int bookingID;
    private String customerName; // names.firstName in database
    private String reviewerName;
    private String reviewDate;
    private String reviewDescription;

    public ReviewDTO(int reviewID, String reviewDateTime, float starRating, String description, int bookingID, String customerName) {
        this.reviewID = reviewID;
        this.reviewDateTime = reviewDateTime;
        this.starRating = starRating;
        this.description = description;
        this.bookingID = bookingID;
        this.customerName = customerName; // names.firstName in database

    }

    public ReviewDTO(String reviewerName, float starRating, String reviewDate, String reviewDescription) {
        this.reviewerName = reviewerName;
        this.starRating = starRating;
        this.reviewDate = reviewDate;
        this.reviewDescription = reviewDescription;
    }

    public int getReviewID() {
        return reviewID;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }

    public String getReviewDateTime() {
        return reviewDateTime;
    }

    public void setReviewDateTime(String reviewDateTime) {
        this.reviewDateTime = reviewDateTime;
    }

    public float getStarRating() {
        return starRating;
    }

    public void setStarRating(float starRating) {
        this.starRating = starRating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewDescription() {
        return reviewDescription;
    }

    public void setReviewDescription(String reviewDescription) {
        this.reviewDescription = reviewDescription;
    }

}


