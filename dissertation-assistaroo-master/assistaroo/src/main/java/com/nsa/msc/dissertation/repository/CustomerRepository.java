package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.*;
import com.nsa.msc.dissertation.Forms.CustomerForm;

import javax.servlet.http.HttpSession;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface CustomerRepository {
    public List<CustomerDTO> findAll();
    public int addCustomer(CustomerForm customerForm);
    public List<EventDTO> findASuppliersEventsCustomerCalendar(int supplier_id);
    public Object[] loginform(String username, String password);
    public List<ServicesDTO> getMainServicesForCustomerCalendar(int supplier_id);
    public List<ServicesDTO> getSubServicesForCustomerCalendar(String main_service, String supplier_id);
    public List<ServicesOfferedDTO> getServicesOfferedAndPricesForCustomerCalendar(String sub_service, String supplier_id);
    public int createBooking(BookingDTO booking, HttpSession httpSession, int supplier_id);
}


