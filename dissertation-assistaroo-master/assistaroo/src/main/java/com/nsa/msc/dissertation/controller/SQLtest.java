package com.nsa.msc.dissertation.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@RestController
public class SQLtest {

    static final String DB_URL = "jdbc:mysql://csmysql.cs.cf.ac.uk:3306/CMT652_1819_TEAM_2";
    // Database credentials
    static final String USER = "c1864770";
    static final String PASS = "aTs9Q7xgFra3sDd";


    @RequestMapping(path = "/employeesTest")
    public String getEmployee() {
//        This is a bad tequnique but it shows the process !!!!
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected");
            String query = "select * from customers where customerID='1'";
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(query);
            System.out.println("got result set");
            while (rs.next()){
                //get each field from the result set
                int id = rs.getInt("customerID");
               int name = rs.getInt("nameID");
                System.out.println(id + ":" + name);
            }

        }catch(Exception e){
            System.out.println("error in jdbc connection"+e);
        }
        return "See server console for data";
    }

}
