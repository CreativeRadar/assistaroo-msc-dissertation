/* COMMENTED OUT IN DECOUPLED DEPLOYMENT */

/*
package com.nsa.msc.dissertation;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/{spring:\\w+}")
                .setViewName("forward:/");
        registry.addViewController("/**/
   //     {spring:\\w+}")
   //             .setViewName("forward:/");
  //  }
// }




/*

stackoverflow.2017.Spring catch all route for index.html.
Available at:https://stackoverflow.com/questions/39331929/spring-catch-all-route-for-index-html/42998817#42998817
[Accessed: 01 September 2019]

 */