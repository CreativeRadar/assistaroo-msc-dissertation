package com.nsa.msc.dissertation.dto;

public class SupplierInformationDTO {
    int supplierID;
    String firstName;
    String dbsVerified;
    float averageRating;
    int numberOfReviews;

    public SupplierInformationDTO(int supplierID, String firstName, String dbsVerified, float averageRating, int numberOfReviews) {
        this.supplierID = supplierID;
        this.firstName = firstName;
        this.dbsVerified = dbsVerified;
        this.averageRating = averageRating;
        this.numberOfReviews = numberOfReviews;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDbsVerified() {
        return dbsVerified;
    }

    public void setDbsVerified(String dbsVerified) {
        this.dbsVerified = dbsVerified;
    }

    public float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(float averageRating) {
        this.averageRating = averageRating;
    }

    public int getNumberOfReviews() {
        return numberOfReviews;
    }

    public void setNumberOfReviews(int numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }
}
