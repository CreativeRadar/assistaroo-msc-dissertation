package com.nsa.msc.dissertation.dto;

public class ServicesDTO {
    private int serviceID;
    private String name;
    private String servicePathString;
    private String imageURL;
    private int parentID;

    public ServicesDTO(int serviceID, String name, String servicePathString, String imageURL, int parentID) {
        this.serviceID = serviceID;
        this.name = name;
        this.servicePathString = servicePathString;
        this.imageURL = imageURL;
        this.parentID = parentID;
    }

    public ServicesDTO(String name) {
        this.name = name;
    }

    public ServicesDTO() {
    }

    public String getServicePathString() {
        return servicePathString;
    }

    public int getServiceID() {
        return serviceID;
    }

    public String getName() {
        return name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public int getParentID() {
        return parentID;
    }
}
