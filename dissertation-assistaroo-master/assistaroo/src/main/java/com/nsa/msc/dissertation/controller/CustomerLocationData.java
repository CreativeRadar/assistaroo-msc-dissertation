package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.Forms.CustomerLocation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerLocationData {


    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, path = "/customer-location")
    public int updateAccountDetails(@RequestBody CustomerLocation customerLocation) {
        System.out.println(customerLocation.getServiceID());
        System.out.println(customerLocation.getPostcode());
        return 1;

    }
}