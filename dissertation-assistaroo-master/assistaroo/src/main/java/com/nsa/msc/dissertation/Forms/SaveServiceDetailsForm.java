package com.nsa.msc.dissertation.Forms;

public class SaveServiceDetailsForm {

    private String registration_number;
    private String main_services;
    private String sub_services;
    private String areas_of_operation;

    public SaveServiceDetailsForm(String registration_number, String main_services, String sub_services, String areas_of_operation) {
        this.registration_number = registration_number;
        this.main_services = main_services;
        this.sub_services = sub_services;
        this.areas_of_operation = areas_of_operation;
    }

    public SaveServiceDetailsForm() {
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getMain_services() {
        return main_services;
    }

    public void setMain_services(String main_services) {
        this.main_services = main_services;
    }

    public String getSub_services() {
        return sub_services;
    }

    public void setSub_services(String sub_services) {
        this.sub_services = sub_services;
    }

    public String getAreas_of_operation() {
        return areas_of_operation;
    }

    public void setAreas_of_operation(String areas_of_operation) {
        this.areas_of_operation = areas_of_operation;
    }
}