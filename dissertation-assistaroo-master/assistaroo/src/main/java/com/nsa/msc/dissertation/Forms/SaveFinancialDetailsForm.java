package com.nsa.msc.dissertation.Forms;

public class SaveFinancialDetailsForm {

    private String registration_number;
    private String card_name;
    private String card_number;
    private String card_expiry_date;

    public SaveFinancialDetailsForm(String registration_number, String card_name, String card_number, String card_expiry_date) {
        this.registration_number = registration_number;
        this.card_name = card_name;
        this.card_number = card_number;
        this.card_expiry_date = card_expiry_date;
    }
    public SaveFinancialDetailsForm(){}

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_expiry_date() {
        return card_expiry_date;
    }

    public void setCard_expiry_date(String card_expiry_date) {
        this.card_expiry_date = card_expiry_date;
    }
}