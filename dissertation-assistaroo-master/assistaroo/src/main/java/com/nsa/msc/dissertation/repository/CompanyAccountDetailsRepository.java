package com.nsa.msc.dissertation.repository;


import com.nsa.msc.dissertation.Forms.SaveAccountDetailsForm;
import com.nsa.msc.dissertation.dto.CompanyAccountDetailsDTO;

import java.util.List;

public interface CompanyAccountDetailsRepository {

    public List<CompanyAccountDetailsDTO> findAccountDetailsByRegistrationNumber(String registration_number);
    public int updateCompanyAccountDetail(SaveAccountDetailsForm saveAccountDetailsForm);


}