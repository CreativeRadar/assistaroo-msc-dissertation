package com.nsa.msc.dissertation.model;

import com.nsa.msc.dissertation.dto.CustomerDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRowMapper implements RowMapper<CustomerDTO> {

    @Override
    public CustomerDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new CustomerDTO(
                rs.getInt("customerID"),
                rs.getInt("nameID"),
                rs.getString("title"),
                rs.getString("firstname"),
                rs.getString("lastname"),
                rs.getString("email")
        );

    }

}

