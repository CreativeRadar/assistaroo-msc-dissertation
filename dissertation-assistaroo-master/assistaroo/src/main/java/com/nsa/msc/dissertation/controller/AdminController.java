package com.nsa.msc.dissertation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminController {
    @GetMapping("/assistarooAdmin")
    public String adminHome() {
        return "adminhome";
    }
}
