package com.nsa.msc.dissertation.Forms;

public class SaveAccountDetailsForm {

    private String registration_number;
    private String username;
    private String email;
    private String password;

    public SaveAccountDetailsForm(String registration_number, String username, String email, String password) {
        this.registration_number = registration_number;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public SaveAccountDetailsForm(){}

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
