package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.dto.CustomerDTO;
import com.nsa.msc.dissertation.dto.EventDTO;
import com.nsa.msc.dissertation.dto.ServicesDTO;
import com.nsa.msc.dissertation.dto.ServicesOfferedDTO;
import com.nsa.msc.dissertation.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CustomerRestControler {
    private CustomerRepository customerRepo;

    @Autowired
    public CustomerRestControler(CustomerRepository cRepo){
        customerRepo = cRepo;
    }


    @RequestMapping(path = "/customers")
    public List<CustomerDTO> getAllCustomers(){
        List<CustomerDTO> customers;
        customers = customerRepo.findAll();
        System.out.println(customers);
        return customers;
    }

    // Used to provide the JSON feed to a supplier's bookings calendar
    @GetMapping("/supplierEventsCustomerCalendar/{supplier_id}")
    List<EventDTO> supplierEventsCustomerCalendar(@PathVariable int supplier_id) {
        return customerRepo.findASuppliersEventsCustomerCalendar(supplier_id);
    }

    // Used to populate the sub_services dropdown in template customer_bookings_calendar.html
    @RequestMapping(value = "/subServicesForSupplier")
    @ResponseBody
    public List<ServicesDTO> getSubServices(@RequestParam String main_service, @RequestParam String supplier_id) {
        return customerRepo.getSubServicesForCustomerCalendar(main_service, supplier_id);
    }

    // Used to populate the services_offered dropdown and price field in template customer_bookings_calendar.html
    @RequestMapping(value = "/servicesOfferedBySupplierForSubServiceAndPrice")
    @ResponseBody
    public List<ServicesOfferedDTO> getServicesOfferedAndPrice(@RequestParam String sub_service, @RequestParam String supplier_id) {
        return customerRepo.getServicesOfferedAndPricesForCustomerCalendar(sub_service, supplier_id);
    }



}
