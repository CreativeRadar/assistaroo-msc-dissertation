package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.CustomerDTO;

public interface DisplayCustomerRepository {
    public CustomerDTO findCustomerByID(int customerID);
}


