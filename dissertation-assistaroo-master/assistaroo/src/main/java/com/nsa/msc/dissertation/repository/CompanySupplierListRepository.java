package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.Forms.SaveSupplierListForm;
import com.nsa.msc.dissertation.dto.CompanySupplierListDTO;

import java.util.List;

public interface CompanySupplierListRepository {

    public List<CompanySupplierListDTO> findSupplierListByRegistrationNumber(String registration_number);
    public int deleteCompanySupplierByID(int supplier_id);
    //public int saveCompanySupplier(SaveSupplierListForm saveSupplierListForm);
    //public int updateCompanySupplier(SaveSupplierListForm  saveSupplierListForm);

}
