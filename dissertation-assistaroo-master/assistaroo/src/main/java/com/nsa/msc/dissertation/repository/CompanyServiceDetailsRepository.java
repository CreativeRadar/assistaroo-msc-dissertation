package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.Forms.SaveServiceDetailsForm;
import com.nsa.msc.dissertation.dto.CompanyServiceDetailDTO;

import java.util.List;

public interface CompanyServiceDetailsRepository {

    public List<CompanyServiceDetailDTO> findServiceDetailsByRegistrationNumber(String registration_number);
    public int deleteCompanyServiceDetailByID(int company_service_details_ID);
    public int saveCompanyServiceDetail(SaveServiceDetailsForm saveServiceDetailsForm);
    public int updateCompanyServiceDetail(SaveServiceDetailsForm saveServiceDetailsForm);


}