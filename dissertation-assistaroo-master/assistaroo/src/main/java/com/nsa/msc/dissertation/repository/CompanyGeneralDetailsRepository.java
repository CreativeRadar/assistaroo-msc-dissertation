package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.Forms.SaveGeneralDetailForm;
import com.nsa.msc.dissertation.dto.CompanyGeneralDetailsDTO;

import java.util.List;

public interface CompanyGeneralDetailsRepository {

    public List<CompanyGeneralDetailsDTO> findAllGeneralDetails();
    public List<CompanyGeneralDetailsDTO> findGeneralDetailsByRegistrationNumber(String registrationNumber);
    public int deleteDetailByID(int companyGeneralDetailsID);
    public int saveGeneralInformation(SaveGeneralDetailForm saveGeneralDetailForm);
    public int updateGeneralInformation(SaveGeneralDetailForm saveGeneralDetailForm);


    }