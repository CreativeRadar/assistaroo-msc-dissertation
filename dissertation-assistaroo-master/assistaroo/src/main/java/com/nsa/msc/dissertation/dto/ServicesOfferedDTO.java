package com.nsa.msc.dissertation.dto;

public class ServicesOfferedDTO {

    private ServicesDTO service;
    private SupplierDTO supplier;
    private String description;
    private float price;
    private int serviceOfferedID;
    private int serviceID;
    private String name; // from services table
    private int supplierID;

    public ServicesOfferedDTO(String description, float price) {
        this.description = description;
        this.price = price;
    }

    public ServicesOfferedDTO(int serviceOfferedID, float price, String description, int serviceID, String name, int supplierID) {
        this.serviceOfferedID = serviceOfferedID;
        this.price = price;
        this.description = description;
        this.serviceID = serviceID;
        this.name = name;
        this.supplierID = supplierID;
    }

    public ServicesDTO getService() {
        return service;
    }

    public void setService(ServicesDTO service) {
        this.service = service;
    }

    public SupplierDTO getSupplier() {
        return supplier;
    }

    public void setSupplier(SupplierDTO supplier) {
        this.supplier = supplier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getServiceOfferedID() {
        return serviceOfferedID;
    }

    public void setServiceOfferedID(int serviceOfferedID) {
        this.serviceOfferedID = serviceOfferedID;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

}
