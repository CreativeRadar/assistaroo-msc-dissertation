package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.Forms.SaveServiceDetailsForm;
import com.nsa.msc.dissertation.repository.CompanyServiceDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyServiceDetailsRESTController {


        CompanyServiceDetailsRepository companyServiceDetailsRepo;

        @Autowired
        public CompanyServiceDetailsRESTController(CompanyServiceDetailsRepository companyServiceDetailsRepo) {
            this.companyServiceDetailsRepo = companyServiceDetailsRepo;
        }


        @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, path = "/new-service-detail")
        public int saveNewServiceDetail(@RequestBody SaveServiceDetailsForm saveServiceDetailsForm) {
        companyServiceDetailsRepo.saveCompanyServiceDetail(saveServiceDetailsForm);
            System.out.println("new service detail added!");
            return 1;

        }
        @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, path = "/update-service-detail")
        public int updateServiceInformation(@RequestBody SaveServiceDetailsForm saveServiceDetailsForm) {
            companyServiceDetailsRepo.updateCompanyServiceDetail(saveServiceDetailsForm);
            System.out.println("Record Updated!");
            return 1;

        }

        @RequestMapping(path = "/delete-service-detail/{company_service_details_ID}", method = RequestMethod.DELETE)
        public int deleteServiceDetail(@PathVariable int company_service_details_ID) {
            companyServiceDetailsRepo.deleteCompanyServiceDetailByID(company_service_details_ID);
            return 1;

        }



    }