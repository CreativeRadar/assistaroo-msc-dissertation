package com.nsa.msc.dissertation.model;

import com.nsa.msc.dissertation.dto.CompanyDetailsDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class CompanyRowMapper implements RowMapper<CompanyDetailsDTO> {
    @Override
    public CompanyDetailsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new CompanyDetailsDTO(
                rs.getString("registration_number"),
                rs.getString("name")
        );
    }
}