package com.nsa.msc.dissertation.dto;

public class CustomerDTO extends UserDTO {
    private int customerID;
    private  int nameID;
    private String title;
    private String fname;
    private String lname;
    private String firstname;
    private String lastname;
    private String email;

    // Used in BookingDTO
    public CustomerDTO(String email, String phone_number, String address, String postcode, int customerID, String title,
                       String fname, String lname) {
        super(email, phone_number, address, postcode);
        this.customerID = customerID;
        this.title = title;
        this.fname = fname;
        this.lname = lname;
    }

    public CustomerDTO(String address, String postcode, String email, String phone_number, String username,
                       String password, int customerID, String title, String fname, String lname) {
            super(address, postcode, email, phone_number, username, password);
            this.customerID = customerID;
            this.title = title;
            this.fname = fname;
            this.lname = lname;
    }


    // For retrieving customer details by ID
    public CustomerDTO(int customerID, int nameID, String title, String firstname, String lastname,String email){
        this.customerID=customerID;
        this.nameID=nameID;
        this.title=title;
        this.firstname=firstname;
        this.lastname = lastname;
        this.email = email;
    }


    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getNameID() {
        return nameID;
    }

    public void setNameID(int nameID) {
        this.nameID = nameID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

}
