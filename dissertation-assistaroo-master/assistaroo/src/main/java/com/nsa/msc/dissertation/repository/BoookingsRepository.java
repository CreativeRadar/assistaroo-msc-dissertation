package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.BookingDTO;

public interface BoookingsRepository {
    public BookingDTO findLatestBookingByCustomerId(int customerID);

}
