package com.nsa.msc.dissertation.Forms;


public class SaveGeneralDetailForm {

    private String registrationNumber;
    private String heading;
    private String content;
    private int companyGeneralDetailsID;


    public SaveGeneralDetailForm() {
    }

    public SaveGeneralDetailForm(String registrationNumber, String heading, String content, int companyGeneralDetailsID) {
        this.registrationNumber = registrationNumber;
        this.heading = heading;
        this.content = content;
        this.companyGeneralDetailsID = companyGeneralDetailsID;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCompanyGeneralDetailsID() {
        return companyGeneralDetailsID;
    }

    public void setCompanyGeneralDetailsID(int companyGeneralDetailsID) {
        this.companyGeneralDetailsID = companyGeneralDetailsID;
    }
}