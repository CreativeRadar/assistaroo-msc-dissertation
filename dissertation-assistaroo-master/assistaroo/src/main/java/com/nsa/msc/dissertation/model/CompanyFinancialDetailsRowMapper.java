package com.nsa.msc.dissertation.model;

import com.nsa.msc.dissertation.dto.CompanyFinancialDetailsDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyFinancialDetailsRowMapper implements RowMapper<CompanyFinancialDetailsDTO> {

    @Override
    public CompanyFinancialDetailsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new CompanyFinancialDetailsDTO(
                rs.getInt("financial_details_ID"),
                rs.getString("registration_number"),
                rs.getString("card_name"),
                rs.getString("card_number"),
                rs.getString("card_expiry_date")
        );

    }
}

