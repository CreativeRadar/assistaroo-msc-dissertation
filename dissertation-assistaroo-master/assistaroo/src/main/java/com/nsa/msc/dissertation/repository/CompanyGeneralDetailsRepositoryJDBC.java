package com.nsa.msc.dissertation.repository;
import com.nsa.msc.dissertation.Forms.SaveGeneralDetailForm;
import com.nsa.msc.dissertation.dto.CompanyDetailsDTO;
import com.nsa.msc.dissertation.dto.CompanyGeneralDetailsDTO;
import com.nsa.msc.dissertation.model.CompanyGeneralDetailsRowMapper;
import com.nsa.msc.dissertation.model.CompanyRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyGeneralDetailsRepositoryJDBC implements CompanyGeneralDetailsRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CompanyGeneralDetailsRepositoryJDBC(JdbcTemplate Template) {
        this.jdbcTemplate = Template;
    }

    @Override
    public List<CompanyGeneralDetailsDTO> findAllGeneralDetails() {
        CompanyGeneralDetailsRowMapper companyGeneralDetailsRowMapper = new CompanyGeneralDetailsRowMapper();
        return jdbcTemplate.query("select * from company_general_details", companyGeneralDetailsRowMapper);

    }

    @Override
    public int deleteDetailByID(int companyGeneralDetailsID) {
        return jdbcTemplate.update("UPDATE company_general_details SET is_deleted = 1 where companyGeneralDetailsID =(?) and is_deleted = 0;",
                new Object[]{companyGeneralDetailsID});
    }


    public CompanyDetailsDTO findCompanyByRegistrationNumber(String registrationNumber){
    CompanyRowMapper companyRowMapper = new CompanyRowMapper();
        return jdbcTemplate.queryForObject("SELECT * FROM companies WHERE registrationNumber= (?)",
                new Object[]{registrationNumber},
                companyRowMapper);
    }

    public List<CompanyGeneralDetailsDTO> findGeneralDetailsByRegistrationNumber(String registrationNumber){
        CompanyGeneralDetailsRowMapper companyGeneralDetailsRowMapper = new CompanyGeneralDetailsRowMapper();
        return jdbcTemplate.query("SELECT * FROM company_general_details WHERE registrationNumber= (?) AND is_deleted=0",
                new Object[]{registrationNumber},
                companyGeneralDetailsRowMapper );
    }


    @Override
    public int saveGeneralInformation(SaveGeneralDetailForm saveGeneralDetailForm){
        return  jdbcTemplate.update("INSERT INTO company_general_details (registrationNumber,heading,content,is_deleted) values(?,?,?,0)",
                new Object[] {
                        saveGeneralDetailForm.getRegistrationNumber(),
                        saveGeneralDetailForm.getHeading(),
                        saveGeneralDetailForm.getContent(),
                });

    }

    @Override
    public int updateGeneralInformation(SaveGeneralDetailForm saveGeneralDetailForm){
        return  jdbcTemplate.update("Update company_general_details SET heading = ?, content =?, is_deleted=0 WHERE companyGeneralDetailsID = ?  \n",

                new Object[] {
                        saveGeneralDetailForm.getHeading(),
                        saveGeneralDetailForm.getContent(),
                        saveGeneralDetailForm.getCompanyGeneralDetailsID()
                });

    }


}