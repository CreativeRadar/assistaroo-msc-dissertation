package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.dto.*;
import com.nsa.msc.dissertation.repository.CustomerRepository;
import com.nsa.msc.dissertation.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Map;


@Controller
    public class customerServicesController {

    private CustomerRepository customerRepository;
    private SupplierRepository supplierRepository;

    @Autowired
    public customerServicesController(CustomerRepository customerRepo, SupplierRepository supplier_repository){
        customerRepository = customerRepo;
        supplierRepository = supplier_repository;
    }

        @PostMapping("/bookingConfirmation/{supplier_id}")
        public ModelAndView bookingConfirmation(BookingDTO booking, HttpSession httpSession,
                                                @PathVariable int supplier_id) {
            ModelAndView mv = new ModelAndView();
            mv.setViewName("booking_confirmation");
            Map model = mv.getModel();
            SupplierDTO supplier;
            try {
                supplier = supplierRepository.findById(supplier_id);
                model.put("supplier", supplier);
                model.put("supplier_id", supplier_id);

            // Thrown if method findById(supplier_id) does not return data as the supplier_id does not exist in the database.
            } catch (EmptyResultDataAccessException e ){
                mv.setViewName("supplier_does_not_exist");
            }
            customerRepository.createBooking(booking, httpSession, supplier_id);
            return mv;
        }

        @GetMapping("/customerBookingsCalendar")
        public String customerBookingsCalendar() {
            return "customer_bookings_calendar";
        }
    }


