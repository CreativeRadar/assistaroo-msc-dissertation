package com.nsa.msc.dissertation.Forms;

public class SupplierForm {
    private String title;
    private String fname;
    private String lname;
    private String address;
    private String postcode;
    private String phone;
    private String email;
    private String mainservice;
    private float area;
    private float pricePerHour;
    private String hearfrom;
    private String username;
    private String password;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMainservice() {
        return mainservice;
    }

    public void setMainservice(String mainservice) {
        this.mainservice = mainservice;
    }

    public float getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(float pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public String getHearFrom() {
        return hearfrom;
    }

    public void setHearFrom(String hearfrom) {
        this.hearfrom = hearfrom;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SupplierForm(String title, String fname, String lname, String address, String postcode, String phone, String email, String mainservice, float area, float pricePerHour, String hearfrom, String username, String password) {
        this.title = title;
        this.fname = fname;
        this.lname = lname;
        this.address = address;
        this.postcode = postcode;
        this.phone = phone;
        this.email = email;
        this.mainservice = mainservice;
        this.area = area;
        this.pricePerHour = pricePerHour;
        this.hearfrom = hearfrom;
        this.username = username;
        this.password = password;
    }
}
