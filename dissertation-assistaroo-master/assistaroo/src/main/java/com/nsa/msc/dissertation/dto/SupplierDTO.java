package com.nsa.msc.dissertation.dto;

//import lombok.AllArgsConstructor;
//import lombok.Data;

//@Data
//@AllArgsConstructor
public class SupplierDTO extends UserDTO {

    private int id;
    private String dbs_reference_number;
    private String public_liability_insurance;
    private String first_name;
    private String last_name;
    private String title;

    public SupplierDTO(int id, String username, String password, String email, boolean is_admin, String phone_number,
                       String address, String postcode, String registration_number, int id1, String dbs_reference_number,
                       String public_liability_insurance, String first_name, String last_name, String title) {
        super(id, username, password, email, is_admin, phone_number, address, postcode, registration_number);
        this.id = id1;
        this.dbs_reference_number = dbs_reference_number;
        this.public_liability_insurance = public_liability_insurance;
        this.first_name = first_name;
        this.last_name = last_name;
        this.title = title;
    }

    public SupplierDTO(int id, String first_name, String last_name) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public SupplierDTO(int id, String username, String password, int id1) { //id1 is the supplier's id
        super(id, username, password);
        this.id = id1;
    }

    public String getDbs_reference_number() {
        return dbs_reference_number;
    }

    public void setDbs_reference_number(String dbs_reference_number) {
        this.dbs_reference_number = dbs_reference_number;
    }

    public String getPublic_liability_insurance() {
        return public_liability_insurance;
    }

    public void setPublic_liability_insurance(String public_liability_insurance) {
        this.public_liability_insurance = public_liability_insurance;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
