package com.nsa.msc.dissertation.controller;
import com.nsa.msc.dissertation.Forms.SaveGeneralDetailForm;
import com.nsa.msc.dissertation.dto.CompanyGeneralDetailsDTO;
import com.nsa.msc.dissertation.repository.CompanyGeneralDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyGeneralDetailsRESTController {

    CompanyGeneralDetailsRepository generalDetailsRepo;

    @Autowired
    public CompanyGeneralDetailsRESTController(CompanyGeneralDetailsRepository generalDetailsRepo
                                              ) {
        this.generalDetailsRepo = generalDetailsRepo;

    }


    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, path = "/new-general-information")
    public int saveNewGeneralInformation(@RequestBody SaveGeneralDetailForm saveGeneralDetailForm) {
        generalDetailsRepo.saveGeneralInformation(saveGeneralDetailForm);
        System.out.println("new detail added!");
        return 1;

    }
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, path = "/update-general-information")
    public int updateGeneralInformation(@RequestBody SaveGeneralDetailForm saveGeneralDetailForm) {
        generalDetailsRepo.updateGeneralInformation(saveGeneralDetailForm);
        System.out.println("Record Updated!");
        return 1;

    }
    @GetMapping("/general-details")
    public List<CompanyGeneralDetailsDTO> findAllGeneralDetails() {
        return generalDetailsRepo.findAllGeneralDetails();

    }

    @RequestMapping(path = "/delete-general-detail/{companyGeneralDetailsID}", method = RequestMethod.POST)
    public int deleteGeneralInformation(@PathVariable int companyGeneralDetailsID) {
        generalDetailsRepo.deleteDetailByID(companyGeneralDetailsID);
        return 1;

    }



}