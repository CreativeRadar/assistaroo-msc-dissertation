
package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.*;

import com.nsa.msc.dissertation.model.SupplierRowMapper;
import org.apache.commons.lang3.RandomStringUtils;

import com.nsa.msc.dissertation.Forms.SupplierForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.temporal.ChronoUnit;
import java.util.*;

import java.awt.print.Book;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Repository
public class SupplierRepositoryJDBC implements SupplierRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public SupplierRepositoryJDBC(JdbcTemplate aTemplate) {
        jdbcTemplate = aTemplate;
    }

    @Override
    public List<BookingDTO> findASuppliersBookings(int supplier_id) {
        // (docs.spring.io, 2019)
        return jdbcTemplate.query("SELECT bookingID,startTime start_date_time,duration, se.name service, \n" +
            "n1.firstName supplier_first_name, n1.lastName supplier_last_name, su.supplierID,\n" +
            "u.email customer_email, u.phone customer_phone_number,\n" +
            "a.address customer_address, a.postcode customer_postcode, \n" +
            "b.customerID, n2.title customer_title, n2.firstName customer_first_name, n2.lastName customer_last_name\n" +
            "FROM bookings b\n" +
            "JOIN servicesOffered so ON so.serviceOfferedID = b.serviceOfferedID\n" +
            "JOIN services se ON se.serviceID = so.serviceID\n" +
            "JOIN suppliers su ON su.supplierID = so.supplierID\n" +
            "JOIN names n1 ON n1.nameID = su.nameID\n" +
            "JOIN customers c ON c.customerID = b.customerID\n" +
            "JOIN names n2 ON n2.nameID = c.nameID\n" +
            "JOIN users u ON u.customerID = c.customerID \n" +
            "JOIN addresses a ON a.addressID = u.addressID\n" +
            "WHERE su.supplierID = ?", new Object[]{supplier_id},
            (rs, i) -> new BookingDTO(
                rs.getInt("bookingID"),
                new Date(rs.getTimestamp("start_date_time").getTime()), // Convert SQL timestamp to Date object
                rs.getFloat("duration"),
                rs.getString("service"),
                new SupplierDTO(rs.getInt("supplierID"),rs.getString("supplier_first_name"),
                        rs.getString("supplier_last_name")),
                new CustomerDTO(rs.getString("customer_email"), rs.getString("customer_phone_number"),
                        rs.getString("customer_address"), rs.getString("customer_postcode"),
                        rs.getInt("customerID"), rs.getString("customer_title"),
                        rs.getString("customer_first_name"), rs.getString("customer_last_name"))
            ));
    }

    @Override
    public List<ReviewDTO> findASuppliersReviews(int supplier_id) {

        final List<ReviewDTO> query = jdbcTemplate.query("SELECT reviews.reviewID, reviews.reviewDateTime, reviews.starRating, reviews.description, reviews.bookingID, names.firstName AS 'customerName' \n" +
                        "FROM reviews JOIN bookings JOIN servicesOffered JOIN customers JOIN names \n" +
                        "ON reviews.bookingID=bookings.bookingID \n" +
                        "AND bookings.serviceOfferedID=servicesOffered.serviceOfferedID \n" +
                        "AND customers.customerID=bookings.customerID \n" +
                        "AND customers.nameID=names.nameID\n" +
                        "WHERE servicesOffered.supplierID=?;", new Object[]{supplier_id},
                (rs, i) -> new ReviewDTO(
                        rs.getInt("reviewID"),
                        rs.getString("reviewDateTime"),
                        rs.getFloat("starRating"),
                        rs.getString("description"),
                        rs.getInt("bookingID"),
                        rs.getString("customerName"))
        );

        return query;
    }

    @Override
    public List<SupplierDetailsDTO> findASuppliersDetailsBySupplierID(int supplierID){

        List<SupplierDetailsDTO> supplierDetails = jdbcTemplate.query("SELECT suppliers.supplierID, users.userName, names.firstName, names.lastName, users.email, users.phone, suppliers.DBSreferenceNumber, suppliers.PublicLiabilityInsurance, suppliers.areaOfOperation, addresses.address, addresses.postcode\n" +
                        "FROM names JOIN suppliers JOIN users JOIN addresses\n" +
                        "ON names.nameID=suppliers.nameID AND suppliers.supplierID=users.supplierID AND users.addressID=addresses.addressID\n" +
                        "WHERE suppliers.supplierId=?", new Object[]{supplierID},
                (rs, i) -> new SupplierDetailsDTO(
                        rs.getInt("supplierID"),
                        rs.getString("userName"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("email"),
                        rs.getString("phone"),
                        rs.getString("DBSreferenceNumber"),
                        rs.getString("PublicLiabilityInsurance"),
                        rs.getString("areaOfOperation"),
                        rs.getString("address"),
                        rs.getString("postcode")
                ));

        return supplierDetails;
    }

    @Override
    public List<ServicesOfferedDTO> findASuppliersServicesBySupplierID(int supplierID){

        List<ServicesOfferedDTO> supplierServices = jdbcTemplate.query("SELECT servicesOffered.serviceOfferedID, servicesOffered.price, servicesOffered.description, servicesOffered.serviceID, services.name, servicesOffered.supplierID FROM servicesOffered\n" +
                        "JOIN services ON servicesOffered.serviceID=services.serviceID\n" +
                        "WHERE servicesOffered.supplierID=?;", new Object[]{supplierID},
                (rs, i) -> new ServicesOfferedDTO(
                        rs.getInt("serviceOfferedID"),
                        rs.getFloat("price"),
                        rs.getString("description"),
                        rs.getInt("serviceID"),
                        rs.getString("name"),
                        rs.getInt("supplierID")
                ));

        return supplierServices;
    }

    @Override
    public SupplierDTO findById(int supplier_id){
        SupplierRowMapper supplier_row_mapper = new SupplierRowMapper();
        // (docs.spring.io, 2019)
        return jdbcTemplate.queryForObject("SELECT userID,userName,password,supplierID FROM users WHERE supplierID= ?",
                new Object[]{supplier_id},
                supplier_row_mapper);
    }

    // Method uses most of the BookingDTO attributes returned by method findASuppliersBookings(int supplier_id) to
    // create a new EventDTO object (provided the BookingDTO exists).
    @Override
    public ArrayList<EventDTO> findASuppliersEvents(int supplier_id){
        List<BookingDTO> bookings = findASuppliersBookings(supplier_id);
        ArrayList<EventDTO> events = new ArrayList<>();
        for (int i = 0; i < bookings.size(); i++) {
            String client = bookings.get(i).getCustomer().getTitle().toString() + " " +
                    bookings.get(i).getCustomer().getFname().toString() + " " +
                    bookings.get(i).getCustomer().getLname().toString();
            String postcode = bookings.get(i).getCustomer().getPostcode();
            Date startDateTime = bookings.get(i).getStart_date_time();
            EventDTO event = new EventDTO(
                bookings.get(i).getBooking_id(),client + ", "+ postcode, client,
                bookings.get(i).getCustomer().getAddress(), postcode, startDateTime.toInstant().toString(),
                startDateTime.toInstant().plus((long)bookings.get(i).getDuration(), ChronoUnit.HOURS).toString(),
                "I have a hearing difficulty so please knock on the window instead of the door.",
                bookings.get(i).getCustomer().getEmail(), bookings.get(i).getCustomer().getPhone_number(),
                // (mkyong.com. 2019)
                RandomStringUtils.randomAlphanumeric(10), bookings.get(i).getService()
            );
            events.add(event);
        }
        return events;
    }

    @Override
    public int supplierCancelsBooking(int booking_id, String cancellation_message){
        return jdbcTemplate.update("UPDATE bookings SET cancelledById = (SELECT userID FROM bookings b\n" +
                        "JOIN servicesOffered so ON so.serviceOfferedID = b.serviceOfferedID\n" +
                        "JOIN users u ON u.supplierID = so.supplierID \n" +
                        "WHERE bookingID = ?),  cancelledDateTime = NOW(), " +
                        "cancelledBookingMessage = ? " +
                        "WHERE bookingID = ?;",
                new Object[]{ booking_id, cancellation_message, booking_id });
    }

    @Transactional
    public int addSupplier(SupplierForm supplierForm) {
        //variables for storing the update queries
        int statusNames;
        int statusAddresses;
        int statusSuppliers;
        int statusUsers;
        int statusServicesOffered;

        String checkUsernameExistsQuery = "select username from users where username='" + supplierForm.getUsername() + "'";
        try {
            String queriedUsername = (String) jdbcTemplate.queryForObject(checkUsernameExistsQuery, String.class);
            if(!queriedUsername.equals("")){
                return 3;
            }
        } catch (Exception e) {
        }
        //insert first name, last name and title of the supplier into the names table
        statusNames = jdbcTemplate.update("insert into names(firstName, lastName, title)  values(?,?,?)",
                new Object[]{supplierForm.getFname(),supplierForm.getLname(),supplierForm.getTitle()}
        );

        String sql = "select names.nameID from names where names.nameID = LAST_INSERT_ID()";
        Integer name_id = jdbcTemplate.queryForObject(sql, Integer.class);
        //insert the nameID foreign key, the hearFrom and Area of operation infornmation to the suppliers table.
        statusSuppliers = jdbcTemplate.update("insert into suppliers(areaOfOperation, hearFrom, nameID)  values(?,?,?)",
                new Object[]{supplierForm.getArea(),supplierForm.getHearFrom(),name_id  }
        );
        //store the suppliers table primary key using last_insert_id() to use it later in the users table insert query
        sql = "select suppliers.supplierID from suppliers where suppliers.supplierID = LAST_INSERT_ID()";
        Integer supplier_id = jdbcTemplate.queryForObject(sql, Integer.class);

        //insert the suppliers's address and postcode in the addresses table
        statusAddresses = jdbcTemplate.update("insert into addresses(address, postcode)   values(?,?)",
                new Object[]{supplierForm.getAddress(),supplierForm.getPostcode()}
        );

        //store the address table primary key and use it in the users insert query
        sql = "select addresses.addressID from addresses where addresses.addressID = LAST_INSERT_ID()";
        Integer address_id = jdbcTemplate.queryForObject(sql, Integer.class);

        //insert all remaining information of the customer along with the 2 foreign keys addressID and customerID
        //stored previously in the users table
        Object[] passwordAndSalt = get_SHA_1_SecurePassword(supplierForm.getPassword(), true);
        String securePassword = (String) passwordAndSalt[0];
        byte[] salt = (byte[])passwordAndSalt[1];
        System.out.println(passwordAndSalt[0].toString());
        System.out.println(passwordAndSalt[1].toString());

        System.out.println(supplierForm.getPassword() + " hashes to " + securePassword);
        statusUsers = jdbcTemplate.update("insert into users(username, password, salt, email, isAdmin, phone, addressID, registrationNumber, customerID, supplierID)   values(?,?,?,?,?,?,?,?,?,?)",
                new Object[]{supplierForm.getUsername(), securePassword, salt, supplierForm.getEmail(), null, supplierForm.getPhone(), address_id, null, null, supplier_id  }
        );


        sql = "select services.serviceID from services where services.name = " + "'" + supplierForm.getMainservice() + "'";
        Integer service_id = jdbcTemplate.queryForObject(sql, Integer.class);

        statusServicesOffered = jdbcTemplate.update("insert into servicesOffered(price,description,serviceID,supplierID)   values(?,?,?,?)",
                new Object[]{supplierForm.getPricePerHour(),supplierForm.getMainservice(),service_id,supplier_id}
        );
        // this if/else clause is not related to the JDBC @Transactional behaviour of the addCustomer method.
        // It is only used to relay information to the CustomerController class to redirect either to a
        // registration succesfull page or a registration failure page
        //if ALL insert queries were carried out succesfully, return 1
        if((statusNames == 1) && (statusAddresses == 1) && (statusSuppliers == 1) && (statusUsers == 1)){
            return 1;
        } else { //else return 2, "error" code.
            return 2;
        }
    }

    private static Object[] get_SHA_1_SecurePassword(String passwordToHash, boolean doSalt)
    {
        String generatedPassword = null;
        String saltHexString = null;
        Object[] returnObjects = new Object[2];
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            if(doSalt){
                byte[] salt = getSalt();
                md.update(salt);
                returnObjects[1] = salt;
                System.out.println(returnObjects[1].toString());
            }
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
            returnObjects[0] = generatedPassword;
            System.out.println(returnObjects[0].toString());
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return returnObjects;
    }

    private static boolean verify_SHA_1_SecurePassword(String hashToVerify, String inputPassword, byte[] salt)
    {
        String generatedHash = null;
        String saltHexString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt);
            byte[] bytes = md.digest(inputPassword.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedHash = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return hashToVerify.equals(generatedHash);
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    @Override
    @Transactional
    public String[] getChildrenServices(SupplierForm supplierForm) {
        //variables for storing the update queries
        int statusServices;
        String[] serviceArray = new String[10]; //TODO : If JDBC returns string array of subservices, remove the static array size
        try {
            String sql = "select services.serviceID from services where services.name = '" + supplierForm.getMainservice() + "'";
            Integer parentServiceID = jdbcTemplate.queryForObject(sql, Integer.class);
            System.out.println("Parent Service ID = " + parentServiceID);
        } catch (Exception e) {
        }
        return serviceArray;
    }

    @Override
    public List<ReviewDTO> getAllReviewsByServiceOfferedId(int supplierId){
        return jdbcTemplate.query("select names.firstName,reviews.starRating,reviews.reviewDateTime,reviews.description \n" +
                        "from names\n" +
                        "join customers on customers.nameID = names.nameID\n" +
                        "join bookings on bookings.customerID = customers.customerID\n" +
                        "join reviews on reviews.bookingID = bookings.bookingID\n" +
                        "join servicesOffered on servicesOffered.supplierID = ? and bookings.serviceOfferedID = servicesOffered.serviceOfferedID;",
                new Object[]{supplierId},
                (rs, i) -> new ReviewDTO(
                        rs.getString(1),
                        rs.getFloat(2),
                        rs.getString(3),
                        rs.getString(4)
                ));
    }

    public int getSupplierIdByServiceOfferedId(int serviceOfferedId){
        String supplierIDQuery = "select servicesOffered.supplierID \n" +
                "from servicesOffered\n" +
                "where servicesOffered.serviceOfferedID = '" + serviceOfferedId + "'";
        return jdbcTemplate.queryForObject(supplierIDQuery, Integer.class);
    }

    @Override
    public String findSupplierNameById(int supplier_id){
        return jdbcTemplate.queryForObject("SELECT firstName supplier FROM names n\n" +
            "JOIN suppliers s ON s.nameID = n.nameID\n" +
            "WHERE supplierID = ?", new Object[]{supplier_id}, String.class);
    }

    public List<SupplierInformationDTO> getAllSuppliersByService(int serviceId){
        return jdbcTemplate.query("select distinct suppliers.supplierID, names.firstName," +
                        "CASE\n" +
                        "WHEN suppliers.DBSReferenceNumber = null\n" +
                        "               THEN 'DBS Verified'\n" +
                        "       END as dbsVerified" +
                        ",avg(reviews.starRating) as averageRating,count(reviews.starRating) as numberOfReviews from suppliers\n" +
                        "join names on suppliers.nameID = names.nameID\n" +
                        "join servicesOffered s1 on s1.supplierID = suppliers.supplierID\n" +
                        "join services on services.serviceID = s1.serviceID\n" +
                        "left join bookings on bookings.serviceOfferedID = s1.serviceOfferedID \n" +
                        "left join reviews on reviews.bookingID = bookings.bookingID\n" +
                        "where services.serviceID=?\n" +
                        "group by suppliers.supplierID,names.firstName;",
                new Object[]{serviceId},
                (rs, i) -> new SupplierInformationDTO(
                        rs.getInt("supplierID"),
                        rs.getString("firstName"),
                        rs.getString("dbsVerified"),
                        rs.getFloat("averageRating"),
                        rs.getInt("numberOfReviews")
                )
                );
    }

    @Override
    public String getServiceNameFromServiceId(int serviceId){
        String supplierIDQuery = "select services.service_name from services\n" +
                "where services.serviceID= '" + serviceId + "'";
        return jdbcTemplate.queryForObject(supplierIDQuery, String.class);
    }

    @Override
    public String getSupplierNameFromSupplierId(int supplierId){
        String supplierIDQuery = "select names.firstName from names \n" +
                "join suppliers on suppliers.nameID = names.nameID\n" +
                "where suppliers.supplierID = '" + supplierId + "'";
        return jdbcTemplate.queryForObject(supplierIDQuery, String.class);
    }

    @Override
    public List<SupplierInformationDTO>getNumberOfReviewsBySupplier(int supplierId){
        return jdbcTemplate.query("select distinct suppliers.supplierID, names.firstName, if(suppliers.DBSReferenceNumber = null,' ','DBS Verified') as dbsVerified, avg(reviews.starRating) as averageRating,count(reviews.starRating) as numberOfReviews from suppliers\n" +
                        "join names on suppliers.nameID = names.nameID\n" +
                        "join servicesOffered s1 on s1.supplierID = suppliers.supplierID\n" +
                        "join services on services.serviceID = s1.serviceID\n" +
                        "left join bookings on bookings.serviceOfferedID = s1.serviceOfferedID \n" +
                        "left join reviews on reviews.bookingID = bookings.bookingID\n" +
                        "where suppliers.supplierID=?\n" +
                        "group by 1;",
                new Object[]{supplierId},
                (rs, i) -> new SupplierInformationDTO(
                        rs.getInt("supplierID"),
                        rs.getString("firstName"),
                        rs.getString("dbsVerified"),
                        rs.getFloat("averageRating"),
                        rs.getInt("numberOfReviews")
                )
        );
    }
}

//    @Override
//    @Transactional
//    public List<SupplierDTO> findSuppliersByServiceOfferedName(String serviceOfferedName){
//        return jdbcTemplate.query("SELECT users.* FROM users,servicesOffered,services,suppliers WHERE services.name = ? and services.serviceID = servicesOffered.serviceID and servicesOffered.supplierID = suppliers.supplierID and users.supplierID = suppliers.supplierID",
//                new Object[]{serviceOfferedName},
//                (rs, i) -> new ServicesDTO(
//                        rs.getInt("serviceID"),
//                        rs.getString("name"),
//                        rs.getString("servicePathString"),
//                        rs.getString("imageURL"),
//                        0
//                ));
//    }



/* References:
   docs.spring.io. 2019. JdbcTemplate (Spring Framework 5.1.9.RELEASE API). Available at:
   https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/jdbc/core/JdbcTemplate.html
   [Accessed: 08 August 2019].  I have used methods queryForObject() and query() to retrieve data from the database for
   the given parameter, and then map it to the relevant constructors.

   mkyong.com. 2019. Java – How to generate a random String. Available at:
   https://www.mkyong.com/java/java-how-to-generate-a-random-string/ [Accessed: 16 August 2019].  I have used
   RandomStringUtils.randomAlphanumeric(10) to generate a string of length 10 containing randomly selected letters and
   numbers.

   Paul, J. 2016. How to convert java.util.Date to java.sql.Timestamp? Available at:
   https://javarevisited.blogspot.com/2016/12/how-to-convert-javautildate-to-java.sql.Timestamp.html. [Accessed: 08 August 2019].
   I have used Date() to convert the timestamp to a Date object.
*  */
