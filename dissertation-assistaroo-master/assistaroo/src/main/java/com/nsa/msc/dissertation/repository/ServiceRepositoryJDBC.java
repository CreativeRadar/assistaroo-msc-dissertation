package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.ServiceDTO;
import com.nsa.msc.dissertation.dto.ServicesDTO;
import com.nsa.msc.dissertation.model.ServiceRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceRepositoryJDBC implements ServiceRepositoryInterface {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ServiceRepositoryJDBC(JdbcTemplate Template) {
        this.jdbcTemplate = Template;
    }

    @Override
    public List<ServiceDTO> findAllServiceSubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("select * from servicesOffered2", serviceRowMapper);

    }

    @Override
    public ServiceDTO findById(int serviceOfferedID) {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.queryForObject("select * from servicesOffered2 where serviceOfferedID = ?",
                new Object[]{serviceOfferedID},
                serviceRowMapper);
    }

    @Override
    public List<ServiceDTO> findByParentCategory(String service_name) {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("select * from servicesOffered2 where service_name = ?",
                new Object[]{service_name},
                serviceRowMapper);
    }
    @Override
    public List<ServiceDTO> findAllLaundrySubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("SELECT * FROM servicesOffered2 o\n" +
                                        " JOIN services2 s \n" +
                                        "ON o.serviceID = s.serviceID \n" +
                                        "WHERE s.serviceID = 1" , serviceRowMapper);

    }
    @Override
    public List<ServiceDTO> findAllCleaningSubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("SELECT * FROM servicesOffered2 o\n" +
                                        " JOIN services2 s \n" +
                                        "ON o.serviceID = s.serviceID \n" +
                                        "WHERE s.serviceID = 2" , serviceRowMapper);

    }
    @Override
    public List<ServiceDTO> findAllPersonalSubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("SELECT * FROM servicesOffered2 o\n" +
                                        " JOIN services2 s \n" +
                                        "ON o.serviceID = s.serviceID \n" +
                                        "WHERE s.serviceID = 3" , serviceRowMapper);

    }
    @Override
    public List<ServiceDTO> findAllMaintenanceSubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("SELECT * FROM servicesOffered2 o\n" +
                                        " JOIN services2 s \n" +
                                        "ON o.serviceID = s.serviceID \n" +
                                        "WHERE s.serviceID = 4" , serviceRowMapper);

    }
    @Override
    public List<ServiceDTO> findAllPetsSubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("SELECT * FROM servicesOffered2 o\n" +
                                        " JOIN services2 s \n" +
                                        "ON o.serviceID = s.serviceID \n" +
                                        "WHERE s.serviceID = 5" , serviceRowMapper);

    }
    @Override
    public List<ServiceDTO> findAllProductsSubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("SELECT * FROM servicesOffered2 o\n" +
                                        " JOIN services2 s \n" +
                                        "ON o.serviceID = s.serviceID \n" +
                                        "WHERE s.serviceID = 6" , serviceRowMapper);

    }

    @Override
    public List<ServiceDTO> findAllHomeCareSubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("SELECT * FROM servicesOffered2 o\n" +
                                        " JOIN services2 s \n" +
                                        "ON o.serviceID = s.serviceID \n" +
                                        "WHERE s.serviceID = 7" , serviceRowMapper);

    }
    @Override
    public List<ServiceDTO> findAllTreatmentsSubCategories() {
        ServiceRowMapper serviceRowMapper = new ServiceRowMapper();
        return jdbcTemplate.query("SELECT * FROM servicesOffered2 o\n" +
                                      " JOIN services2 s \n" +
                                      "ON o.serviceID = s.serviceID \n" +
                                      "WHERE s.serviceID = 8" , serviceRowMapper);

    }

    @Override
    public List<ServicesDTO> getAllMainServices() {
        return jdbcTemplate.query("select services.serviceID,services.name,services.imageURL,serviceHierarchy.parentID from services,serviceHierarchy where serviceHierarchy.parentID IS NULL and serviceHierarchy.childID = services.serviceID",
                new Object[]{},
                (rs, i) -> new ServicesDTO(
                        rs.getInt("serviceID"),
                        rs.getString("name"),
                        rs.getString("servicePathString"),
                        rs.getString("imageURL"),
                        0
                ));
    }

    @Override
    public List<ServicesDTO> getAllLeafServices() {
        return jdbcTemplate.query("SELECT * FROM services WHERE services.serviceID NOT IN ( SELECT serviceHierarchy.parentID FROM serviceHierarchy WHERE serviceHierarchy.parentID IS NOT NULL)",
                new Object[]{},
                (rs, i) -> new ServicesDTO(
                        rs.getInt("serviceID"),
                        rs.getString("name"),
                        rs.getString("servicePathString"),
                        rs.getString("imageURL"),
                        0
                ));
    }

//    public List<ServicesDTO> getServicesOfferedBySupplierId(int supplierID){
//        return jdbcTemplate.query("select distinct services.name \n" +
//                        "from services\n" +
//                        "join servicesOffered on servicesOffered.supplierID = ? and servicesOffered.serviceID = services.serviceID",
//                new Object[]{supplierID},
//                (rs, i) -> new ServicesDTO(
//                        0,
//                        rs.getString("name"),
//                        "",
//                        "",
//                        0
//                ));
//    }
public List<ServicesDTO> getServicesOfferedBySupplierId(int supplierID){
    return jdbcTemplate.query("SELECT distinct services.name from services\n" +
                    "join servicesOffered on services.serviceID=servicesOffered.serviceID\n" +
                    "where servicesOffered.supplierID=?;",
            new Object[]{supplierID},
            (rs, i) -> new ServicesDTO(
                    0,
                    rs.getString("name"),
                    "",
                    "",
                    0
            ));
}

}