package com.nsa.msc.dissertation.model;

import com.nsa.msc.dissertation.dto.CompanyAccountDetailsDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyAccountDetailsRowMapper implements RowMapper<CompanyAccountDetailsDTO> {

    @Override
    public CompanyAccountDetailsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new CompanyAccountDetailsDTO(
                rs.getString("registration_number"),
                rs.getString("username"),
                rs.getString("email"),
                rs.getString("password")
        );

    }
}

