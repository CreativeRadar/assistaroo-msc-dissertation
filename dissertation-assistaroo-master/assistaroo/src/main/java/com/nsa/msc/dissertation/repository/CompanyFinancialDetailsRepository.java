package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.Forms.SaveFinancialDetailsForm;
import com.nsa.msc.dissertation.dto.CompanyFinancialDetailsDTO;

import java.util.List;

public interface CompanyFinancialDetailsRepository {

    public List<CompanyFinancialDetailsDTO> findFinancialDetailsByRegistrationNumber(String registration_number);
    public int deleteCompanyFinancialDetailByID(int company_service_details_ID);
    public int saveCompanyFinancialDetail(SaveFinancialDetailsForm saveFinancialDetailsForm);
    public int updateCompanyFinancialDetail(SaveFinancialDetailsForm saveFinancialDetailsForm);


}