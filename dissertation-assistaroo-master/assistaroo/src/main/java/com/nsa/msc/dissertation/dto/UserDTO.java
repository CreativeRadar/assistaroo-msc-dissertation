package com.nsa.msc.dissertation.dto;

public class UserDTO {
    private int id;
    private String username;
    private String password;
    private String email;
    private boolean is_admin;
    private String phone_number;
    private String address;
    private String postcode;
    private String registration_number;

    public UserDTO(int id, String username, String password, String email, boolean is_admin, String phone_number,
                   String address, String postcode, String registration_number) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.is_admin = is_admin;
        this.phone_number = phone_number;
        this.address = address;
        this.postcode = postcode;
        this.registration_number = registration_number;
    }

    public UserDTO(String address, String postcode, String email, String phone_number, String username, String password) { //Required for a constructor in CustomerDTO
        this.address = address;
        this.postcode = postcode;
        this.email = email;
        this.phone_number = phone_number;
        this.username = username;
        this.password = password;
    }

    //Required for a constructor in CustomerDTO for BookingDTO
    public UserDTO(String email, String phone_number, String address, String postcode) {
        this.email = email;
        this.phone_number = phone_number;
        this.address = address;
        this.postcode = postcode;
    }

    public UserDTO() {
    }

    public UserDTO(int id, String username, String password) { // Used in SupplierDTO
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isIs_admin() {
        return is_admin;
    }

    public void setIs_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }
}
