package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.Forms.CustomerLocation;
import com.nsa.msc.dissertation.Forms.SaveFinancialDetailsForm;
import com.nsa.msc.dissertation.repository.CompanyFinancialDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyFinancialDetailsRESTController {

    CompanyFinancialDetailsRepository financialDetailsRepo;


    @Autowired
    public CompanyFinancialDetailsRESTController(CompanyFinancialDetailsRepository financialDetailsRepo) {
        this.financialDetailsRepo = financialDetailsRepo;
    }


    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, path = "/new-financial-detail")
    public int saveNewFinancialDetail(@RequestBody SaveFinancialDetailsForm saveFinancialDetailsForm) {
        financialDetailsRepo.saveCompanyFinancialDetail(saveFinancialDetailsForm);
        System.out.println("new financial detail added!");
        return 1;

    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, path = "/update-financial-detail")
    public int updateFinancialInformation(@RequestBody SaveFinancialDetailsForm saveFinancialDetailsForm) {
        financialDetailsRepo.updateCompanyFinancialDetail(saveFinancialDetailsForm);
        System.out.println("Record Updated!");
        return 1;

    }

    @RequestMapping(path = "/delete-financial-detail/{financial_details_ID}", method = RequestMethod.DELETE)
    public int deleteFinancialDetail(@PathVariable int financial_details_ID) {
        financialDetailsRepo.deleteCompanyFinancialDetailByID(financial_details_ID);
        return 1;

    }

}