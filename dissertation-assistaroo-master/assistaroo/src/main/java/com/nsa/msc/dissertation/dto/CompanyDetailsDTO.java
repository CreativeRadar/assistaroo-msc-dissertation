package com.nsa.msc.dissertation.dto;

public class CompanyDetailsDTO {

    private String registration_number;
    private String name;

    public CompanyDetailsDTO(String registration_number, String name) {
        this.registration_number = registration_number;
        this.name = name;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

