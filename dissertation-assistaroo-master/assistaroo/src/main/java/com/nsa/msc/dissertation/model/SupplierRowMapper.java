package com.nsa.msc.dissertation.model;

import com.nsa.msc.dissertation.dto.SupplierDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SupplierRowMapper implements RowMapper<SupplierDTO> {
    @Override
    public SupplierDTO mapRow(ResultSet rs, int rowNum) throws SQLException { // (docs.spring.io, 2019)
        return new SupplierDTO(
                rs.getInt("userID"),
                rs.getString("userName"),
                rs.getString("password"),
                rs.getInt("supplierID")
        );
    }
}

/*  References:
    docs.spring.io. 2019. RowMapper (Spring Framework 5.1.9.RELEASE API). Available at:
    https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/jdbc/core/RowMapper.html
    [Accessed: 14 August 2019].  I have used mapRow() to map the row result for each 'users' table column to the
    SupplierDTO object.
 */

