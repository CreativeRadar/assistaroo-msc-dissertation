package com.nsa.msc.dissertation.Forms;

public class CustomerForm {
    private String title;
    private String fname;
    private String lname;
    private String address;
    private String postcode;
    private String email;
    private String phone;
    private String username;
    private String password;

    public CustomerForm(String title, String fname, String lname, String address, String postcode, String email, String phone, String username, String password) {
        this.title = title;
        this.fname = fname;
        this.lname = lname;
        this.address = address;
        this.postcode = postcode;
        this.email = email;
        this.phone = phone;
        this.username = username;
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
