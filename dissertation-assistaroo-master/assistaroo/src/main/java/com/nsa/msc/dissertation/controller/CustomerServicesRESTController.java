package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.dto.ServiceDTO;
import com.nsa.msc.dissertation.repository.ServiceRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://www.assistaroo.co.uk", maxAge = 3600)
@RestController
public class    CustomerServicesRESTController {

    private ServiceRepositoryInterface serviceRepoInterface;

    @Autowired
    public CustomerServicesRESTController(ServiceRepositoryInterface serviceRepoInterface) {
        this.serviceRepoInterface = serviceRepoInterface;
    }

    @GetMapping("/all-services")
    public List<ServiceDTO> findAllServices(){
      return serviceRepoInterface.findAllServiceSubCategories();

    }
    @GetMapping("/laundry-sub-categories")
    public List<ServiceDTO> findAllLaundrySubCategories(){
        return serviceRepoInterface.findAllLaundrySubCategories();

    }
    @GetMapping("/cleaning-sub-categories")
    public List<ServiceDTO> findAllCleaningSubCategories(){
        return serviceRepoInterface.findAllCleaningSubCategories();

    }
    @GetMapping("/personal-sub-categories")
    public List<ServiceDTO> findAllPersonalSubCategories(){
        return serviceRepoInterface.findAllPersonalSubCategories();

    }

    @GetMapping("/maintenance-sub-categories")
    public List<ServiceDTO> findAllMaintenanceSubCategories(){
        return serviceRepoInterface.findAllMaintenanceSubCategories();

    }
    @GetMapping("/pets-sub-categories")
    public List<ServiceDTO> findAllPetsSubCategories(){
        return serviceRepoInterface.findAllPetsSubCategories();

    }
    @GetMapping("/products-sub-categories")
    public List<ServiceDTO> findAllProductsSubCategories(){
        return serviceRepoInterface.findAllProductsSubCategories();

    }


    @GetMapping("/home-care-sub-categories")
    public List<ServiceDTO> findAllHomeCareSubCategories(){
        return serviceRepoInterface.findAllHomeCareSubCategories();

    }
    @GetMapping("/treatments-sub-categories")
    public List<ServiceDTO> findAllTreatmentsSubCategories(){
        return serviceRepoInterface.findAllTreatmentsSubCategories();

    }

}



