package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.Forms.SaveFinancialDetailsForm;
import com.nsa.msc.dissertation.dto.CompanyFinancialDetailsDTO;
import com.nsa.msc.dissertation.model.CompanyFinancialDetailsRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CompanyFinancialDetailsRepositoryJDBC implements CompanyFinancialDetailsRepository{

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CompanyFinancialDetailsRepositoryJDBC(JdbcTemplate Template) {
        this.jdbcTemplate = Template;
    }


    @Override
    public int deleteCompanyFinancialDetailByID(int financial_details_ID) {
        return jdbcTemplate.update("UPDATE financial_details SET is_deleted = 1 where financial_details_ID =(?) and is_deleted = 0;",
                new Object[]{financial_details_ID});
    }

    @Override
    public List<CompanyFinancialDetailsDTO> findFinancialDetailsByRegistrationNumber(String registration_number){
        CompanyFinancialDetailsRowMapper companyFinancialDetailsRowMapper = new CompanyFinancialDetailsRowMapper();
        return jdbcTemplate.query("SELECT * FROM financial_details WHERE registration_number= (?) AND is_deleted=0",
                new Object[]{registration_number},
               companyFinancialDetailsRowMapper );
    }


    @Override
    public int saveCompanyFinancialDetail(SaveFinancialDetailsForm saveFinancialDetailsForm){
        return  jdbcTemplate.update("INSERT INTO financial_details (registration_number,card_name,card_number,card_expiry_date,is_deleted) values(?,?,?,?,0)",
                new Object[] {
                        saveFinancialDetailsForm.getRegistration_number(),
                        saveFinancialDetailsForm.getCard_name(),
                        saveFinancialDetailsForm.getCard_number(),
                        saveFinancialDetailsForm.getCard_expiry_date()
                });

    }


    @Override
    public int updateCompanyFinancialDetail(SaveFinancialDetailsForm saveFinancialDetailsForm){
        return  jdbcTemplate.update("UPDATE financial_details\n" +
                        "SET card_name = ?, \n" +
                        "card_number = ?, \n" +
                        "card_expiry_date = ? \n" +
                        "is_deleted = 0" +
                        "WHERE financial_details_ID = ?;\n;",
                new Object[] {

                        saveFinancialDetailsForm.getRegistration_number(),
                        saveFinancialDetailsForm.getCard_name(),
                        saveFinancialDetailsForm.getCard_number(),
                        saveFinancialDetailsForm.getCard_expiry_date(),

                });

    }




}
