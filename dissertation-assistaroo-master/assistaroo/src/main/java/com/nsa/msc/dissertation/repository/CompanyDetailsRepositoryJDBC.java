package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.CompanyDetailsDTO;
import com.nsa.msc.dissertation.model.CompanyRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CompanyDetailsRepositoryJDBC implements CompanyDetailsRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CompanyDetailsRepositoryJDBC(JdbcTemplate Template) {
        this.jdbcTemplate = Template;
    }


    public CompanyDetailsDTO findCompanyByRegistrationNumber(String registration_number){
        CompanyRowMapper companyRowMapper = new CompanyRowMapper();
        return jdbcTemplate.queryForObject("select * from companies where registration_number= (?)",
                new Object[]{registration_number},
                companyRowMapper);
    }

    }




