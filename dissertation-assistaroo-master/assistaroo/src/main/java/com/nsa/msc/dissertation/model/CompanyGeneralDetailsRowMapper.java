package com.nsa.msc.dissertation.model;

import com.nsa.msc.dissertation.dto.CompanyGeneralDetailsDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyGeneralDetailsRowMapper implements RowMapper<CompanyGeneralDetailsDTO> {

    @Override
    public CompanyGeneralDetailsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new CompanyGeneralDetailsDTO(
                rs.getInt("companyGeneralDetailsID"),
                rs.getString("registrationNumber"),
                rs.getString("heading"),
                rs.getString("content"),
                rs.getInt("is_deleted")
        );

    }
}

