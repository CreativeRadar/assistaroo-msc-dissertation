package com.nsa.msc.dissertation.dto;

public class ServiceDTO {

    private int servicesOfferedID;
    private int price;
    private String description;
    private int serviceID;
    private int supplierID;
    private String image_url;
    private String service_name;

    public ServiceDTO(int servicesOfferedID, int price, String description, int serviceID, int supplierID, String image_url, String service_name) {
        this.servicesOfferedID = servicesOfferedID;
        this.price = price;
        this.description = description;
        this.serviceID = serviceID;
        this.supplierID = supplierID;
        this.image_url = image_url;
        this.service_name = service_name;
    }

    public int getServicesOfferedID() {
        return servicesOfferedID;
    }

    public void setServicesOfferedID(int servicesOfferedID) {
        this.servicesOfferedID = servicesOfferedID;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }
}


