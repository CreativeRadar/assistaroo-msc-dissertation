package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.Forms.SaveSupplierListForm;
import com.nsa.msc.dissertation.dto.CompanySupplierListDTO;
import com.nsa.msc.dissertation.model.CompanySupplierListRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CompanySupplierListRepositoryJDBC implements CompanySupplierListRepository {


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CompanySupplierListRepositoryJDBC(JdbcTemplate Template) {
        this.jdbcTemplate = Template;
    }

    @Override
    public List<CompanySupplierListDTO> findSupplierListByRegistrationNumber(String registration_number) {
        CompanySupplierListRowMapper companySupplierListRowMapper = new CompanySupplierListRowMapper();
        return jdbcTemplate.query("SELECT * from users u \n" +
                        "LEFT Join suppliers s \n" +
                        "ON s.supplierID = u.supplierID \n" +
                        "LEFT JOIN names n \n" +
                        "ON n.nameID = s.nameID \n" +
                        "LEFT JOIN addresses a " +
                        "ON a.addressID = u.addressID \n" +
                        "LEFT JOIN servicesOffered so \n" +
                        "ON so.supplierID = u.supplierID \n" +
                        "LEFT JOIN services se \n" +
                        "ON so.serviceID = se.serviceID \n" +
                        "LEFT Join supplier_areas_of_operation sa \n " +
                        "ON sa.supplier_id = s.supplierID \n" +
                        "WHERE u.registration_number= (?)",
                new Object[]{registration_number},
                companySupplierListRowMapper);
    }



    @Override
    public int deleteCompanySupplierByID(int supplier_id) {
        return jdbcTemplate.update("UPDATE names SET is_deleted = 1 where nameID =(?) and is_deleted = 0;",
                new Object[]{supplier_id});
    }

}