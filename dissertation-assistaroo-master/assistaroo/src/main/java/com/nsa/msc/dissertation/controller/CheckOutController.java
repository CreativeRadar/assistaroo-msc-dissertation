package com.nsa.msc.dissertation.controller;


import com.nsa.msc.dissertation.dto.BookingDTO;
import com.nsa.msc.dissertation.dto.ChargeRequest;
import com.nsa.msc.dissertation.repository.BookingsRepositoryJDBC;
import com.nsa.msc.dissertation.repository.BoookingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.awt.print.Book;

@Controller
public class CheckOutController {

    private BoookingsRepository boookingsRepository;

    @Value("${stripe.keys.public}")
    private String stripePublicKey;

    @Autowired
    public CheckOutController(BoookingsRepository boookingsRepository) {
        this.boookingsRepository = boookingsRepository;
    }


    @RequestMapping("/checkout")
    public String checkout(Model model) {


        // Fixed amount of 20pounds for each service in cents
        model.addAttribute("amount", 2000);
        model.addAttribute("stripePublicKey", stripePublicKey);
        model.addAttribute("currency", ChargeRequest.Currency.GBP);
        return "booking_confirmation";


        /*
        BookingDTO newBooking;
        newBooking = boookingsRepository.findLatestBookingByCustomerId(customerID);
        model.addAttribute("bookingID",newBooking.getBooking_id());
        model.addAttribute("customerID",newBooking.getCustomer().getCustomerID());
        model.addAttribute("first_name",newBooking.getSupplier().getFirst_name());
        model.addAttribute("date",newBooking.getStart_date_time());
        model.addAttribute("service",newBooking.getService());

         */


    }
}



/*
    REFERENCES:
    Baeldung.2019.Introduction to the Stripe API for Java.Available at: https://www.baeldung.com/java-stripe-api.
    [Accessed: 24 August 2019]

 */