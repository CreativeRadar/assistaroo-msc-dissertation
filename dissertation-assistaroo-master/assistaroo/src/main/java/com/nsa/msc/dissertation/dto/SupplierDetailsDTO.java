package com.nsa.msc.dissertation.dto;

public class SupplierDetailsDTO {
    private int supplierID;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String DBSreferenceNumber;
    private String PublicLiabilityInsurance;
    private String areaOfOperation;
    private String address;
    private String postcode;

    public SupplierDetailsDTO(int supplierID, String userName, String firstName, String lastName, String email, String phone, String DBSreferenceNumber, String publicLiabilityInsurance, String areaOfOperation, String address, String postcode) {
        this.supplierID = supplierID;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.DBSreferenceNumber = DBSreferenceNumber;
        PublicLiabilityInsurance = publicLiabilityInsurance;
        this.areaOfOperation = areaOfOperation;
        this.address = address;
        this.postcode = postcode;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDBSreferenceNumber() {
        return DBSreferenceNumber;
    }

    public void setDBSreferenceNumber(String DBSreferenceNumber) {
        this.DBSreferenceNumber = DBSreferenceNumber;
    }

    public String getPublicLiabilityInsurance() {
        return PublicLiabilityInsurance;
    }

    public void setPublicLiabilityInsurance(String publicLiabilityInsurance) {
        PublicLiabilityInsurance = publicLiabilityInsurance;
    }

    public String getAreaOfOperation() {
        return areaOfOperation;
    }

    public void setAreaOfOperation(String areaOfOperation) {
        this.areaOfOperation = areaOfOperation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
}
