
package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.dto.ServicesDTO;
import com.nsa.msc.dissertation.dto.SupplierDTO;
import com.nsa.msc.dissertation.Forms.CustomerForm;
import com.nsa.msc.dissertation.repository.CustomerRepository;
import com.nsa.msc.dissertation.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
public class CustomerController {
    private CustomerRepository CR;
    private SupplierRepository supplierRepository;

    @Autowired
    public CustomerController(CustomerRepository cr, SupplierRepository supplier_repository) {

        CR = cr;
        supplierRepository = supplier_repository;
    }
    @PostMapping(path = "/newCustomer")
    public ModelAndView addCustomer(CustomerForm customerform, BindingResult br) {
        ModelAndView mv = new ModelAndView();
        if (br.hasErrors()) {
            mv.setViewName("errorPage");
            System.out.println("1");
        }
        else{
            String test = String.valueOf(CR.addCustomer(customerform));
            if(!test.equals("1")){
                if(test.equals("2")) {
                    mv.setViewName("errorPage");
                    System.out.println("2");
                } else if (test.equals("3")){
                    mv.setViewName("username-exists-page");
                    System.out.println("3");
                }
            }
            else
            {
                mv.setViewName("registrationSuccessfulPage");
            }
        }
        return mv;
    }

    @RequestMapping(path="/login", method= RequestMethod.POST)
    public ModelAndView verifyLogin(@RequestParam String username, @RequestParam String password, HttpSession session){
        ModelAndView mv = new ModelAndView();
        Map model = mv.getModel();
        Object[] userData  = CR.loginform(username,password );
        if(userData != null){
            String uname = (String) userData[0];
            Integer customerID = (Integer) userData[1];
            Integer supplierID = (Integer) userData[2];
            if(customerID != null){
                session.setAttribute("customerID", customerID);
                session.setAttribute("userType", "customer");
                session.setAttribute("customerID", customerID);
                mv.setViewName("redirect:/");
            } else if(supplierID != null){
                session.setAttribute("supplierID", supplierID);
                model.put("supplierID",supplierID);
                session.setAttribute("userType", "supplier");
                session.setAttribute("supplierID", supplierID);
                mv.setViewName("redirect:/supplierHome/{supplierID}");
            } else {
                session.setAttribute("userType", "guest");
                mv.setViewName("login-error-page");
            }
            session.setAttribute("isLoggedIn", "yes");
            session.setAttribute("username", uname);
            String isLogged = (String) session.getAttribute("isLoggedIn");
            String usernameLoggedIn = (String) session.getAttribute("username");
            System.out.println(isLogged);
            System.out.println(usernameLoggedIn);
        }
        else {
            mv.setViewName("login-error-page");
        }
        return mv;
    }

    @GetMapping("/customerBookingsCalendar/{supplier_id}")
    public ModelAndView customerBookingsCalendar(@PathVariable int supplier_id, HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("customer_bookings_calendar");
        Map model = mv.getModel();
        session.getAttribute("customerID");
        SupplierDTO supplier;
        String supplier_name;
        supplier_name = supplierRepository.findSupplierNameById(supplier_id);
        List<ServicesDTO> main_services = CR.getMainServicesForCustomerCalendar(supplier_id);

        model.put("supplier_name", supplier_name);
        model.put("main_services", main_services);
        try {
            supplier = supplierRepository.findById(supplier_id);
            model.put("supplier", supplier);
            model.put("supplier_id", supplier_id);

            // Thrown if method findById(supplier_id) does not return data as the supplier_id does not exist in the database.
        } catch (EmptyResultDataAccessException e ){
            mv.setViewName("supplier_does_not_exist");
        }
        return mv;
    }

    @RequestMapping(path="/logout", method= RequestMethod.POST)
    public ModelAndView logOut(HttpSession session){
        ModelAndView mv = new ModelAndView();
        session.setAttribute("userType", "guest");
        session.setAttribute("isLoggedIn", "no");
        session.setAttribute("username", "");
        mv.setViewName("logOutPage");
        return mv;
    }
}
