package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.Forms.SaveAccountDetailsForm;
import com.nsa.msc.dissertation.dto.CompanyAccountDetailsDTO;
import com.nsa.msc.dissertation.model.CompanyAccountDetailsRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CompanyAccountDetailsRepositoryJDBC implements  CompanyAccountDetailsRepository {


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CompanyAccountDetailsRepositoryJDBC(JdbcTemplate Template) {
        this.jdbcTemplate = Template;
    }


    @Override
    public List<CompanyAccountDetailsDTO> findAccountDetailsByRegistrationNumber(String registration_number){
        CompanyAccountDetailsRowMapper companyAccountDetailsRowMapper = new CompanyAccountDetailsRowMapper();
        return jdbcTemplate.query("SELECT * FROM company_account_details WHERE registration_number= (?) AND is_deleted=0",
                new Object[]{registration_number},
                companyAccountDetailsRowMapper );
    }




    @Override
    public int updateCompanyAccountDetail(SaveAccountDetailsForm saveAccountDetailsForm){
        return  jdbcTemplate.update("UPDATE company_account_details\n" +
                        "SET username = ?, \n" +
                        "email = ?, \n" +
                        "password = ? \n" +
                        "is_deleted = 0" +
                        "WHERE registration_number = ?;\n;",
                new Object[] {

                       saveAccountDetailsForm.getUsername(),
                        saveAccountDetailsForm.getEmail(),
                        saveAccountDetailsForm.getPassword(),
                        saveAccountDetailsForm.getRegistration_number()
                });

    }




}
