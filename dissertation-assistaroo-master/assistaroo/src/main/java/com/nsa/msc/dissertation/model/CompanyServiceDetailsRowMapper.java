package com.nsa.msc.dissertation.model;

import com.nsa.msc.dissertation.dto.CompanyServiceDetailDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyServiceDetailsRowMapper implements RowMapper<CompanyServiceDetailDTO> {

    @Override
    public CompanyServiceDetailDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new CompanyServiceDetailDTO(
                rs.getInt("company_service_details_ID"),
                rs.getString("registration_number"),
                rs.getString("main_services"),
                rs.getString("sub_services"),
                rs.getString("areas_of_operation")
        );

    }
}

