package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.BookingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BookingsRepositoryJDBC implements BoookingsRepository {


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public BookingsRepositoryJDBC(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public BookingDTO findLatestBookingByCustomerId(int customerID) {
        return jdbcTemplate.queryForObject("SELECT * FROM Bookings WHERE customerID=?)",
                new Object[]{customerID},
                BookingDTO.class);
    }
}
