package com.nsa.msc.dissertation.dto;

public class CompanyGeneralDetailsDTO {

    private int companyGeneralDetailsID;
    private String registrationNumber;
    private String heading;
    private String content;
    private int is_deleted;


    public CompanyGeneralDetailsDTO(int companyGeneralDetailsID, String registrationNumber, String heading, String content, int is_deleted) {
        this.companyGeneralDetailsID = companyGeneralDetailsID;
        this.registrationNumber = registrationNumber;
        this.heading = heading;
        this.content = content;
        this.is_deleted = is_deleted;
    }

    public int getCompanyGeneralDetailsID() {
        return companyGeneralDetailsID;
    }

    public void setCompanyGeneralDetailsID(int companyGeneralDetailsID) {
        this.companyGeneralDetailsID = companyGeneralDetailsID;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }
}


