package com.nsa.msc.dissertation.service;

import com.nsa.msc.dissertation.dto.ChargeRequest;
import com.stripe.Stripe;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;


    @Service
    public class StripeService {

        @Value("${stripe.keys.secret}")
        private String secretKey;

        @PostConstruct
        public void init() {
            Stripe.apiKey = secretKey;
        }
        public Charge charge(ChargeRequest chargeRequest)
                throws AuthenticationException, InvalidRequestException,
                StripeException, CardException{
            Map<String, Object> chargeParams = new HashMap<>();
            chargeParams.put("amount", chargeRequest.getAmount());
            chargeParams.put("currency", chargeRequest.getCurrency());
            chargeParams.put("description", chargeRequest.getDescription());
            chargeParams.put("source", chargeRequest.getStripeToken());
            return Charge.create(chargeParams);
        }
    }


    /*

    REFERENCES:
    Baeldung.2019.Introduction to the Stripe API for Java.Available at: https://www.baeldung.com/java-stripe-api.
    [Accessed: 24 August 2019]

   */