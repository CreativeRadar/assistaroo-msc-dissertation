package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.Forms.CustomerLocation;
import com.nsa.msc.dissertation.dto.*;

import com.nsa.msc.dissertation.Forms.SupplierForm;
import com.nsa.msc.dissertation.repository.ServiceRepositoryInterface;
import com.nsa.msc.dissertation.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;

import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

import java.util.List;
import java.util.Map;

@Controller
public class supplierController {
    private SupplierRepository supplierRepository;
    private ServiceRepositoryInterface SER;

    @Autowired
    public supplierController(SupplierRepository supplierRepository, ServiceRepositoryInterface ser) {
        this.supplierRepository = supplierRepository;
        SER = ser;
    }

    @GetMapping("/supplierHome/{supplier_id}")
    public ModelAndView supplierHome(HttpSession session, @PathVariable int supplier_id) { // (docs.spring.io, 2019)
        ModelAndView mv = new ModelAndView();
        if ((String) session.getAttribute("userType") != "supplier") {
            mv.setViewName("supplier_does_not_exist");
        }
        mv.setViewName("supplier_home");
        Map model = mv.getModel();
        SupplierDTO supplier;
        try {
            supplier = supplierRepository.findById(supplier_id);
            model.put("supplier", supplier);
            model.put("supplier_id", supplier_id);
            // Thrown if method findById(supplier_id) does not return data as the supplier_id does not exist in the database.
        } catch (EmptyResultDataAccessException e) {
            mv.setViewName("supplier_does_not_exist");
        }
        return mv;
    }

    @GetMapping("/supplierMyActivity/{supplier_id}")
    public ModelAndView supplierActivity(@PathVariable int supplier_id) { // (docs.spring.io, 2019)
        ModelAndView mv = new ModelAndView();
        mv.setViewName("supplier_my_activity");
        Map model = mv.getModel();
        SupplierDTO supplier;
        try {
            supplier = supplierRepository.findById(supplier_id);
            model.put("supplier", supplier);
            model.put("supplier_id", supplier_id);
            // Thrown if method findById(supplier_id) does not return data as the supplier_id does not exist in the database.
        } catch (EmptyResultDataAccessException e) {
            mv.setViewName("supplier_does_not_exist");
        }
        return mv;
    }

    // No longer used, HttpSession has been added to method public ModelAndView
    // supplierHome(HttpSession session, @PathVariable int supplier_id)
    // @GetMapping("/supplierHome")
    // public String supplierHome(HttpSession session){
    //    if ((String) session.getAttribute("userType") != "supplier"){
    //        return "";
    //    }
    //    return "supplier_home";
    //   }

    @RequestMapping(path = "/supplierMyFeedback/{supplierID}")
    public ModelAndView supplierMyFeedback(@PathVariable int supplierID) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("supplier_my_feedback");
        Map model = mv.getModel();
        List<ReviewDTO> suppliersReviews;
        suppliersReviews = supplierRepository.findASuppliersReviews(supplierID);
        model.put("suppliersReviews", suppliersReviews);
        model.put("supplierID", supplierID);
        return mv;
    }

        @RequestMapping(path = "suppliersList/{serviceId}")
        public ModelAndView suppliersList (@PathVariable int serviceId){
            String Title;
            ModelAndView mv = new ModelAndView();
            mv.setViewName("suppliers_list");
            Map model = mv.getModel();
            List<SupplierInformationDTO> SupplierInformation;
            SupplierInformation = supplierRepository.getAllSuppliersByService(serviceId);
            Title = supplierRepository.getServiceNameFromServiceId(serviceId);
            model.put("SupplierInformation", SupplierInformation);
            model.put("serviceId", serviceId);
            model.put("Title", Title);
            return mv;
        }
//    @RequestMapping(path = "/supplierProfile" ,
//            params = {"serviceOfferedId"})
//    public ModelAndView supplierProfile(@RequestParam(value = "serviceOfferedId") int serviceOfferedId) {
//        ModelAndView mv = new ModelAndView();
//        mv.setViewName("supplier_profile");
//        Map model = mv.getModel();
//        List<ReviewDTO> allReviews;
//        List<ServicesDTO> allAvailableServices;
//        allReviews = SR.getAllReviewsByServiceOfferedId(serviceOfferedId);
//        int supplier_id = SR.getSupplierIdByServiceOfferedId(serviceOfferedId);
//        allAvailableServices = SER.getServicesOfferedBySupplierId(supplier_id);
//        model.put("allReviews", allReviews);
//        model.put("allAvailableServices", allAvailableServices);
//        return mv;
//    }
        @RequestMapping(path = "/supplierProfile/{supplierId}")
        public ModelAndView supplierProfile ( @PathVariable int supplierId, HttpSession session){
            int customerID= (Integer) session.getAttribute("customerID");
            System.out.println(customerID);
            String Name;
            ModelAndView mv = new ModelAndView();
            mv.setViewName("supplier_profile");
            Map model = mv.getModel();
            List<ReviewDTO> allReviews;
            List<ServicesDTO> allAvailableServices;
            allReviews = supplierRepository.getAllReviewsByServiceOfferedId(supplierId);
            int supplier_id = supplierRepository.getSupplierIdByServiceOfferedId(supplierId);
            allAvailableServices = SER.getServicesOfferedBySupplierId(supplier_id);
            List<SupplierInformationDTO> NumberOfReviewsBySupplier;
            Name = supplierRepository.getSupplierNameFromSupplierId(supplierId);
            allReviews = supplierRepository.getAllReviewsByServiceOfferedId(supplierId);
            allAvailableServices = SER.getServicesOfferedBySupplierId(supplierId);
            NumberOfReviewsBySupplier = supplierRepository.getNumberOfReviewsBySupplier(supplierId);
            model.put("name", Name);
            model.put("allReviews", allReviews);
            model.put("allAvailableServices", allAvailableServices);
            model.put("NumberOfReviewsBySupplier", NumberOfReviewsBySupplier);
            return mv;
        }

        @RequestMapping(path = "/supplierMyDetails/{supplierID}")
        public ModelAndView supplierMyDetails ( @PathVariable int supplierID){
            ModelAndView mv = new ModelAndView();
            List<SupplierDetailsDTO> supplierDetails;
            supplierDetails = supplierRepository.findASuppliersDetailsBySupplierID(supplierID);
            List<ServicesOfferedDTO> supplierServices;
            supplierServices = supplierRepository.findASuppliersServicesBySupplierID(supplierID);
            mv.setViewName("sole-trader-details");
            Map model = mv.getModel();
            model.put("supplierID", supplierID);
            model.put("supplierDetails", supplierDetails);
            model.put("supplierServices", supplierServices);
            return mv;
        }

        @GetMapping("/supplierMyDetails")
        public String supplierMyDetails () {
            return "sole-trader-details";
        }

        @RequestMapping(path = "/supplierSignUp")
        public ModelAndView supplierSignUpSetup () {
            ModelAndView mv = new ModelAndView();
            mv.setViewName("supplier-sole-trader-form");
            Map model = mv.getModel();
            List<ServicesDTO> allLeafServices;
            allLeafServices = SER.getAllLeafServices();
            model.put("allLeafServices", allLeafServices);
            return mv;
        }

        @PostMapping(path = "/newSupplier")
        public ModelAndView addSupplier (SupplierForm supplierForm, BindingResult br){
            ModelAndView mv = new ModelAndView();
            if (br.hasErrors()) {
                mv.setViewName("errorPage");
                System.out.println("[USER ERROR] : Binding result has errors...");
            } else {
                String test = String.valueOf(supplierRepository.addSupplier(supplierForm));
                if (!test.equals("1")) {
                    if (test.equals("2")) {
                        mv.setViewName("errorPage");
                        System.out.println("2");
                    } else if (test.equals("3")) {
                        mv.setViewName("username-exists-page");
                        System.out.println("3");
                    }
                } else {
                    mv.setViewName("registrationSuccessfulPage");
                }
            }
            return mv;
        }

     /*  References:
    docs.spring.io. 2019. Class ModelAndView. Available at:
    https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/servlet/ModelAndView.html [Accessed:
    14 August 2019].  I have used the ModelAndView class so that the supplier's home page (or template "supplier_home")
    changes based on the supplier_id that is given.
   */

    }

