package com.nsa.msc.dissertation.model;
import com.nsa.msc.dissertation.dto.CompanySupplierListDTO;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanySupplierListRowMapper implements RowMapper<CompanySupplierListDTO> {


    @Override
    public CompanySupplierListDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new CompanySupplierListDTO(
                rs.getInt("nameID"),
                rs.getString("firstname"),
                rs.getString("lastname"),
                rs.getString("title"),
                rs.getInt("userID"),
                rs.getString("email"),
                rs.getInt("addressID"),
                rs.getString("address"),
                rs.getString("postcode"),
                rs.getString("phone"),
                rs.getInt("serviceID"),
                rs.getString("service_name"),
                rs.getInt("serviceOfferedID"),
                rs.getString("description"),
                rs.getString("Others"),
                rs.getInt("supplierID"),
                rs.getString("areas_of_operation")
        );


    }
}

