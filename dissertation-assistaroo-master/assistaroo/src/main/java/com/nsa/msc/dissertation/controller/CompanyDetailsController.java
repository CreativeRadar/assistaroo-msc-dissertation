package com.nsa.msc.dissertation.controller;
import com.nsa.msc.dissertation.dto.*;
import com.nsa.msc.dissertation.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;
import java.util.Map;


@Controller
public class CompanyDetailsController {

    CompanyDetailsRepository companyRepo;
    CompanyGeneralDetailsRepository generalDetailsRepo;
    CompanyServiceDetailsRepository companyServiceDetailsRepo;
    CompanyAccountDetailsRepository companyAccountDetailsRepo;
    CompanyFinancialDetailsRepository companyFinancialDetailsRepo;
    CompanySupplierListRepository companySupplierListRepo;


    @Autowired
    public CompanyDetailsController(CompanyDetailsRepository companyRepo,
                                    CompanyGeneralDetailsRepository generalDetailsRepo,
                                    CompanyServiceDetailsRepository companyServiceDetailsRepo,
                                    CompanyAccountDetailsRepository companyAccountDetailsRepo,
                                    CompanyFinancialDetailsRepository companyFinancialDetailsRepo,
                                    CompanySupplierListRepository companySupplierListRepo
                                 ) {
        this.companyRepo = companyRepo;
        this.generalDetailsRepo = generalDetailsRepo;
        this.companyServiceDetailsRepo = companyServiceDetailsRepo;
        this.companyAccountDetailsRepo = companyAccountDetailsRepo;
        this.companyFinancialDetailsRepo = companyFinancialDetailsRepo;
        this.companySupplierListRepo = companySupplierListRepo;
    }


    @GetMapping("/company-details/{registration_number}")
    public ModelAndView getAllCompanyDetails(@PathVariable String registration_number) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("company-details");
        Map model = mv.getModel();


        //Display Company Details Page for each company
        CompanyDetailsDTO company;
        company = (CompanyDetailsDTO)companyRepo.findCompanyByRegistrationNumber(registration_number);
        model.put("company", company);


        //Display General Details for each company
        List<CompanyGeneralDetailsDTO> generalDetails;
        generalDetails = (List<CompanyGeneralDetailsDTO>) generalDetailsRepo.findGeneralDetailsByRegistrationNumber(registration_number);
        model.put("generalDetails", generalDetails);

        //Display Service Details for each company
        List<CompanyServiceDetailDTO> serviceDetails;
        serviceDetails = (List<CompanyServiceDetailDTO>) companyServiceDetailsRepo.findServiceDetailsByRegistrationNumber(registration_number);
        model.put("serviceDetails", serviceDetails);


        //Display Account Details for each company
        List<CompanyAccountDetailsDTO> accountDetails;
        accountDetails = (List<CompanyAccountDetailsDTO>)companyAccountDetailsRepo.findAccountDetailsByRegistrationNumber(registration_number);
        model.put("accountDetails", accountDetails);

        //Display Financial Details for each company
        List<CompanyFinancialDetailsDTO> financialDetails;
        financialDetails = (List<CompanyFinancialDetailsDTO>) companyFinancialDetailsRepo.findFinancialDetailsByRegistrationNumber(registration_number);
        model.put("financialDetails", financialDetails);

        //Display List of Suppliers for each company
        List<CompanySupplierListDTO> supplierDetails;
        supplierDetails = (List<CompanySupplierListDTO>) companySupplierListRepo.findSupplierListByRegistrationNumber(registration_number);
        model.put("supplierDetails", supplierDetails);

        return mv;


    }

}