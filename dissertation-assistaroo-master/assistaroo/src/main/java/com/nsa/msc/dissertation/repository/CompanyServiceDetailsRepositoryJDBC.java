package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.Forms.SaveServiceDetailsForm;
import com.nsa.msc.dissertation.dto.CompanyServiceDetailDTO;
import com.nsa.msc.dissertation.model.CompanyServiceDetailsRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CompanyServiceDetailsRepositoryJDBC implements CompanyServiceDetailsRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CompanyServiceDetailsRepositoryJDBC(JdbcTemplate Template) {
        this.jdbcTemplate = Template;
    }


    @Override
    public int deleteCompanyServiceDetailByID(int company_service_details_ID) {
        return jdbcTemplate.update("UPDATE company_service_details SET is_deleted = 1 where company_service_details_ID =(?) and is_deleted = 0;",
                new Object[]{company_service_details_ID});
    }

    @Override
    public List<CompanyServiceDetailDTO> findServiceDetailsByRegistrationNumber(String registration_number){
        CompanyServiceDetailsRowMapper companyServiceDetailsRowMapper = new CompanyServiceDetailsRowMapper();
        return jdbcTemplate.query("SELECT DISTINCT * FROM company_service_details WHERE registration_number= (?) AND is_deleted=0 \n",
                new Object[]{registration_number},
                companyServiceDetailsRowMapper );
    }


    @Override
    public int saveCompanyServiceDetail(SaveServiceDetailsForm saveServiceDetailsForm){
        return  jdbcTemplate.update("INSERT INTO company_service_details (registration_number,main_services,sub_services,areas_of_operation,is_deleted) values(?,?,?,?,0)",
                new Object[] {
                        saveServiceDetailsForm.getRegistration_number(),
                        saveServiceDetailsForm.getMain_services(),
                        saveServiceDetailsForm.getSub_services(),
                        saveServiceDetailsForm.getAreas_of_operation()
                });

    }


    @Override
    public int updateCompanyServiceDetail(SaveServiceDetailsForm saveServiceDetailsForm){
        return  jdbcTemplate.update("UPDATE company_service_details\n" +
                        "SET registrationNumber = ?," +
                        "main_services = ?, \n" +
                        "sub_services = ?, \n" +
                        "areas_of_operation = ? \n" +
                        "is_deleted = 0" +
                        "WHERE company_service_details_ID = ?;\n;",
                new Object[] {

                        saveServiceDetailsForm.getRegistration_number(),
                        saveServiceDetailsForm.getMain_services(),
                        saveServiceDetailsForm.getSub_services(),
                        saveServiceDetailsForm.getAreas_of_operation()
                });

    }




}
