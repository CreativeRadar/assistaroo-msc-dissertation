package com.nsa.msc.dissertation.Forms;

public class SaveSupplierListForm {

    private int nameID;
    private String firstname;
    private String lastname;
    private String title;
    private int userID;
    private String email;
    private int addressID;
    private String address;
    private String postcode;
    private String phone;
    private int serviceID;
    private String service_name;
    private int serviceOfferedID;
    private String description;
    private String Others;
    private int supplierID;
    private String areas_of_operation;

    public SaveSupplierListForm(int nameID, String firstname, String lastname, String title, int userID, String email, int addressID, String address, String postcode, String phone, int serviceID, String service_name, int serviceOfferedID, String description, String others, int supplierID, String areas_of_operation) {
        this.nameID = nameID;
        this.firstname = firstname;
        this.lastname = lastname;
        this.title = title;
        this.userID = userID;
        this.email = email;
        this.addressID = addressID;
        this.address = address;
        this.postcode = postcode;
        this.phone = phone;
        this.serviceID = serviceID;
        this.service_name = service_name;
        this.serviceOfferedID = serviceOfferedID;
        this.description = description;
        Others = others;
        this.supplierID = supplierID;
        this.areas_of_operation = areas_of_operation;
    }

    public int getNameID() {
        return nameID;
    }

    public void setNameID(int nameID) {
        this.nameID = nameID;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public int getServiceOfferedID() {
        return serviceOfferedID;
    }

    public void setServiceOfferedID(int serviceOfferedID) {
        this.serviceOfferedID = serviceOfferedID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOthers() {
        return Others;
    }

    public void setOthers(String others) {
        Others = others;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    public String getAreas_of_operation() {
        return areas_of_operation;
    }

    public void setAreas_of_operation(String areas_of_operation) {
        this.areas_of_operation = areas_of_operation;
    }
}