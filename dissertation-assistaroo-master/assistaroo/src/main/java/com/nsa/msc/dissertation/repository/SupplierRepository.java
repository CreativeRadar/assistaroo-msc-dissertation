package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.*;

import com.nsa.msc.dissertation.Forms.SupplierForm;

import java.util.ArrayList;
import java.util.List;

public interface SupplierRepository {
    public List<BookingDTO> findASuppliersBookings(int supplier_id);
    public SupplierDTO findById(int id);
    public ArrayList<EventDTO> findASuppliersEvents(int supplier_id);
    public int supplierCancelsBooking(int booking_id, String cancellation_message);
    public List<ReviewDTO> findASuppliersReviews(int supplier_id);
//    public List<SupplierDTO> findSuppliersByServiceOfferedName(String serviceOfferedName);
    public int addSupplier(SupplierForm supplierForm);
    public String[] getChildrenServices(SupplierForm supplierForm);
    public List<ReviewDTO> getAllReviewsByServiceOfferedId(int supplierId);
    public int getSupplierIdByServiceOfferedId(int serviceOfferedId);
    public String findSupplierNameById(int supplier_id);
    public List<SupplierDetailsDTO> findASuppliersDetailsBySupplierID(int supplierID);
    public List<ServicesOfferedDTO> findASuppliersServicesBySupplierID(int supplierID);
    public List<SupplierInformationDTO> getAllSuppliersByService(int serviceId);
    public String getServiceNameFromServiceId(int serviceId);
    public String getSupplierNameFromSupplierId(int supplierId);
    public List<SupplierInformationDTO> getNumberOfReviewsBySupplier(int supplierId);


}
