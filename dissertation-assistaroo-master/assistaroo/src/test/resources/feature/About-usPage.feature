
Feature: Access about-us page
  A user can navigate to the about us page

  Scenario:  Access about-us page on route /about-us.html
    When a user navigates to "/about-us.html"
    Then the about us page must be opened
    And the response should contain "Assistaroo | About-us"




