DROP TABLE IF EXISTS bookings;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS addresses;
DROP TABLE IF EXISTS servicesOffered;
DROP TABLE IF EXISTS servicesOffered2;
DROP TABLE IF EXISTS services;
DROP TABLE IF EXISTS services2;
DROP TABLE IF EXISTS suppliers;
DROP TABLE IF EXISTS names;
DROP TABLE IF EXISTS companies;
DROP TABLE IF EXISTS company_account_details;
DROP TABLE IF EXISTS financial_details;
DROP TABLE IF EXISTS company_general_details;
DROP TABLE IF EXISTS company_service_details;
DROP TABLE IF EXISTS supplier_areas_of_operation;



CREATE TABLE names
(
nameID int not null,
firstName varchar (30),
lastName varchar(30),
title varchar(5),
CONSTRAINT names_PK PRIMARY KEY (nameID)
);

CREATE TABLE addresses
(
addressID int not null,
address varchar(50),
postcode varchar(20),
CONSTRAINT addresses_PK PRIMARY KEY (addressID)
);

CREATE TABLE suppliers
(
supplierID int not null,
DBSReferenceNumber varchar(50),
PublicLiabilityInsurance varchar(50),
nameID int,
CONSTRAINT suppliers_pk PRIMARY KEY (supplierID),
CONSTRAINT fk_names
      FOREIGN KEY (nameID)
      REFERENCES names(nameID)
  );

CREATE TABLE customers
(
customerID int not null,
nameID int,
CONSTRAINT customers_pk PRIMARY KEY (customerID),
CONSTRAINT fk_names2
      FOREIGN KEY (nameID)
      REFERENCES names(nameID)
);


CREATE TABLE services
(
serviceID int(5) not null auto_increment,
name varchar(50),
servicePathString varchar(300),
imageUrl varchar(400),
CONSTRAINT services_pk PRIMARY KEY (serviceID)
);

CREATE TABLE services2
(
serviceID int(5) not null auto_increment,
name varchar(50),
servicePathString varchar(300),
imageUrl varchar(400),
CONSTRAINT services_pk2 PRIMARY KEY (serviceID)
);

DROP TABLE IF EXISTS serviceHierarchy;
CREATE table serviceHierarchy
(
serviceHierarchyID int not null auto_increment,
parentID int(5),
childID int(5),
CONSTRAINT serviceHierarchy_pk PRIMARY KEY (serviceHierarchyID),
CONSTRAINT fk_parentID
      FOREIGN KEY (parentID)
	  REFERENCES services(serviceID),
CONSTRAINT fk_childID
      FOREIGN KEY (childID)
	  REFERENCES services(serviceID)
);

CREATE TABLE servicesOffered
(
serviceOfferedID int(5) not null auto_increment,
price float(10),
description varchar(400),
serviceID int,
supplierID int,
CONSTRAINT servicesO_pk PRIMARY KEY (serviceOfferedID),
CONSTRAINT fk_services
      FOREIGN KEY (serviceID)
      REFERENCES services(serviceID),
 CONSTRAINT fk_suppliers2
      FOREIGN KEY (supplierID)
      REFERENCES suppliers(supplierID)
);

CREATE TABLE servicesOffered2
(
serviceOfferedID int(5) not null auto_increment,
price float(10),
description varchar(400),
serviceID int,
supplierID int,
image_url varchar(200),
service_name varchar (200),
CONSTRAINT servicesO2_pk PRIMARY KEY (serviceOfferedID),
CONSTRAINT fk_services2
      FOREIGN KEY (serviceID)
      REFERENCES services2(serviceID),

);

CREATE TABLE bookings
(
bookingID int not null,
startDate date,
startTime timestamp,
duration float,
serviceOfferedID int,
customerID int,
CONSTRAINT bookings_pk PRIMARY KEY (bookingID),
CONSTRAINT fk_servicesOffered
      FOREIGN KEY (serviceOfferedID)
      REFERENCES servicesOffered(serviceOfferedID),
CONSTRAINT fk_customers2
      FOREIGN KEY (customerID)
      REFERENCES customers(customerID)
);

DROP TABLE IF EXISTS companies;
CREATE TABLE companies
(
registration_number varchar(20) not null,
name varchar(40),
CONSTRAINT companies_pk PRIMARY KEY (registration_number)
);

CREATE TABLE users
(
userID int not null,
userName varchar(30),
password varchar(30),
email varchar(30),
isAdmin boolean,
phone varchar (20),
addressID int,
registration_number varchar(20),
customerID int,
supplierID int,
CONSTRAINT users_pk PRIMARY KEY (userID),
CONSTRAINT fk_addresses
       FOREIGN KEY (addressID)
       REFERENCES addresses(addressID),
CONSTRAINT fk_customers
       FOREIGN KEY (customerID)
       REFERENCES customers(customerID),
CONSTRAINT fk_suppliers
      FOREIGN KEY (supplierID)
	  REFERENCES suppliers(supplierID)
);


DROP TABLE IF EXISTS company_account_details;
create table company_account_details(
    company_account_details_ID int not null  auto_increment PRIMARY KEY,
    registration_number varchar(20),
    username varchar(100) not null,
    email varchar(100)  not null,
    password varchar(100)  not null,
    is_deleted tinyint not null,
    FOREIGN KEY (registration_number) REFERENCES companies(registration_number)
);


DROP TABLE IF EXISTS financial_details;
create table financial_details(
    financial_details_ID int not null auto_increment PRIMARY KEY,
    registration_number varchar(20),
    card_name varchar(100) not null,
    card_number varchar(100)  not null,
    card_expiry_date date  not null,
    is_deleted tinyint not null,
    FOREIGN KEY (registration_number) REFERENCES companies(registration_number)
);

DROP TABLE IF EXISTS company_general_details;
create table company_general_details(
    companyGeneralDetailsID int not null  auto_increment PRIMARY KEY,
    registrationNumber varchar(20),
    heading varchar(500) not null,
    content varchar(500)  not null,
    is_deleted tinyint not null,
    FOREIGN KEY (registrationNumber) REFERENCES companies(registration_number)
);

DROP TABLE IF EXISTS company_service_details;
create table company_service_details(
    company_service_details_ID int not null  auto_increment PRIMARY KEY,
    registration_number varchar(20),
    main_services varchar(100) not null,
    sub_services varchar(100)  not null,
    areas_of_operation varchar(100)  not null,
    is_deleted tinyint not null,
    FOREIGN KEY (registration_number) REFERENCES companies(registration_number)
);

DROP TABLE IF EXISTS supplier_areas_of_operation;
Create table supplier_areas_of_operation(
areas_of_operation_id int not null IDENTITY PRIMARY KEY,
areas_of_operation varchar(200),
supplier_id int not null,
FOREIGN KEY (supplier_id) REFERENCES suppliers(supplierID)
);

