package com.nsa.msc.dissertation.acceptance.cucumber.stepdefs;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class AboutUsPageStepDef {

    WebDriver driver;
    String url = "http://localhost:8080";
    @Before()
    public void setUp() {
        //Not a good practice. Replace with driver path below
        System.setProperty("webdriver.chrome.driver", "ENTER DRIVER PATH");
        driver =new ChromeDriver();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @When("a user navigates to {string}")
    public void a_user_navigates_to(String string) {
       driver.get(url + string);
    }


    @Then("the about us page must be opened")
    public void the_about_us_page_must_be_opened() {
       assertEquals(driver.getCurrentUrl(), "http://localhost:8080/about-us.html");
       System.out.println(driver.getCurrentUrl());

    }

    @Then("the response should contain {string}")
    public void the_response_should_contain_elements(String string) {
       String title =driver.getTitle();
       assertEquals(title , string);
        System.out.println(title);
        driver.quit();
    }


}
