package com.nsa.msc.dissertation.dto;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServiceDTOTest {

    private ServiceDTO serviceDTO;


    @Before
    public final void createServiceDTO() {
        serviceDTO = new ServiceDTO(1,20,"washing",1,6,
                                    "/images/washing.jpg","Laundry");
    }

    @Test
    public void getServicesOfferedID() {
        assertNotNull(serviceDTO);
        assertEquals(serviceDTO.getServicesOfferedID(),1);
    }

    @Test
    public void getPrice() {
        assertNotNull(serviceDTO);
        assertEquals(serviceDTO.getPrice(),20);
    }


    @Test
    public void getDescription() {
        assertNotNull(serviceDTO);
        assertEquals(serviceDTO.getDescription(),"washing");
    }


    @Test
    public void getServiceID() {
        assertNotNull(serviceDTO);
        assertEquals(serviceDTO.getServiceID(),1);
    }

    @Test
    public void getSupplierID() {
        assertNotNull(serviceDTO);
        assertEquals(serviceDTO.getSupplierID(),6);
    }

    @Test
    public void getImage_url() {
        assertNotNull(serviceDTO);
        assertEquals(serviceDTO.getImage_url(),"/images/washing.jpg");
    }

    @Test
    public void getService_name() {
        assertNotNull(serviceDTO);
        assertEquals(serviceDTO.getService_name(),"Laundry");
    }

}