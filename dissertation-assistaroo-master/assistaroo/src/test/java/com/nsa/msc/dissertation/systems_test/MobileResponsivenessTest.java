package com.nsa.msc.dissertation.systems_test;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import java.util.*;
public class MobileResponsivenessTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {

    //Not a good practice. Replace with driver path below
    System.setProperty("webdriver.chrome.driver", "ENTER DRIVER PATH");
    driver =new ChromeDriver();

    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void mobileresponsiveness() {
    driver.get("http://www.assistaroo.co.uk/");
    driver.manage().window().setSize(new Dimension(1100, 680));
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
    driver.manage().window().setSize(new Dimension(700, 580));
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(4)")).click();
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
    driver.manage().window().setSize(new Dimension(300, 580));
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
    driver.manage().window().setSize(new Dimension(400, 580));
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
    driver.findElement(By.cssSelector(".MuiGrid-item:nth-child(3)")).click();
  }
}

//Generated from selenium IDE, fixed bugs and modified to run