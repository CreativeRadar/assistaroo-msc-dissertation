package com.nsa.msc.dissertation;

import com.nsa.msc.dissertation.dto.ReviewDTO;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ReviewDTOTest {

    private ReviewDTO review;

    @Before
    public final void createReview(){
        review = new ReviewDTO(1,"2019-03-01 00:00:01", 5,"Very good service", 1, "Susan");
    }

    @Test
    public void reviewExistsTest(){
        assertNotNull(review);
    }

    @Test
    public void reviewDTOGettersTest(){
        assertEquals(1, review.getReviewID());
        assertEquals("2019-03-01 00:00:01", review.getReviewDateTime());
//        assertEquals(5, review.getStarRating());
        assertEquals("Very good service", review.getDescription());
        assertEquals(1, review.getBookingID());
        assertEquals("Susan", review.getCustomerName());
    }

    @Test
    public void reviewDTOSettersTest(){
        review.setReviewID(2);
        review.setReviewDateTime("2000-12-12 00:00:50");
        review.setStarRating(1);
        review.setDescription("bad");
        review.setBookingID(2);
        review.setCustomerName("Fred");

        assertEquals(2, review.getReviewID());
        assertEquals("2000-12-12 00:00:50", review.getReviewDateTime());
//        assertEquals(1, review.getStarRating());
        assertEquals("bad", review.getDescription());
        assertEquals(2, review.getBookingID());
        assertEquals("Fred", review.getCustomerName());
    }


}
