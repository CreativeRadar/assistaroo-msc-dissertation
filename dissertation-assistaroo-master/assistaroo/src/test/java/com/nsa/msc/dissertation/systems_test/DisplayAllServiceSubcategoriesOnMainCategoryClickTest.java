package com.nsa.msc.dissertation.systems_test;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import java.util.*;
public class DisplayAllServiceSubcategoriesOnMainCategoryClickTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    //Not a good practice. Replace with driver path below
    System.setProperty("webdriver.chrome.driver", "ENTER DRIVER PATH");
    driver =new ChromeDriver();

    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void displayallservicesubcategoriesonmaincategoryclick() {
    driver.get("http://www.assistaroo.co.uk/");
    driver.manage().window().setSize(new Dimension(1100, 680));
    driver.findElement(By.cssSelector(".fa-dryer-alt")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".fa-dryer-alt"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(2) > .jss70 .caption")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(2) > .jss70 .caption"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    {
      WebElement element = driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(3) .caption"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(3) .caption")).click();
    driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(4) > .jss70 > .MuiPaper-root")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(4) .MuiPaper-root"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    {
      WebElement element = driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(5) .MuiPaper-root"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(5) .MuiPaper-root")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(6) .MuiPaper-root"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(6) .MuiPaper-root")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(7) .MuiPaper-root"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    driver.findElement(By.cssSelector(".MuiGrid-root:nth-child(7) .MuiPaper-root")).click();
  }
}


//Generated from selenium IDE, fixed bugs and modified to run