package com.nsa.msc.dissertation.acceptance.cucumber.stepdefs;


import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;

public class AccessCompanyDetailStepDefs {


    WebDriver driver;
    String url = "http://localhost:8080/company-details/";


    @Before()
    public void setUp() {
        //Not a good practice. Replace with driver path below
        System.setProperty("webdriver.chrome.driver", "ENTER DRIVER PATH");
        driver =new ChromeDriver();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @When("a company supplier goes to the details page based on his {string}")
    public void a_company_supplier_goes_to_the_details_page_based_on_his(String string)  {
        driver.get(url + string);
    }
    @Then("the supplier should be directed to the details page with his {string}")
    public void the_supplier_should_be_directed_to_the_details_page_with_his(String string) {
        String curentUrl = driver.getCurrentUrl();
        assertEquals(curentUrl, url + string);
        System.out.println(curentUrl);


    }
    @Then("the details html page should be rendered with all suppliers {string}")
    public void the_details_html_page_should_be_rendered_with_all_suppliers(String string){
      String element =driver.findElement(By.xpath("//*[@id='general-details-table']/tbody[1]/tr/td[2]")).getText();
      assert(element.contains(string));
      System.out.println(element);
        driver.quit();
    }




}










/*
REFERENCES
Baeldung.2018.Cucumber Spring Integration.
Available at:https://www.baeldung.com/cucumber-spring-integration.
[Accessed: 30 August 2019]
 */