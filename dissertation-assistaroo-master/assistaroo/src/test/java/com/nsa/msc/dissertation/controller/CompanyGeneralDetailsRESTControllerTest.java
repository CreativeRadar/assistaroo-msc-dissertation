package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.Forms.SaveGeneralDetailForm;
import com.nsa.msc.dissertation.repository.CompanyGeneralDetailsRepository;
import io.restassured.mapper.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CompanyGeneralDetailsRESTControllerTest {

   // private SaveGeneralDetailForm saveGeneralDetailForm;

    @Before
    public void setUp() throws Exception {
     // saveGeneralDetailForm = new SaveGeneralDetailForm();
       // saveGeneralDetailForm.setCompanyGeneralDetailsID(1);
        //saveGeneralDetailForm.setHeading("Address");
        //saveGeneralDetailForm.setContent("2 Havens place, newport");
        //saveGeneralDetailForm.setRegistrationNumber("01234214");
    }

    @Autowired
    private MockMvc mockMvc;
   // private ObjectMapper objectMapper;



    @Test
    public void saveNewGeneralInformation() throws Exception {
        //String json = objectMapper.deserialize(saveGeneralDetailForm);
      //  this.mockMvc.perform(post("/new-general-information" ).param(String.valueOf(saveGeneralDetailForm.getCompanyGeneralDetailsID()),
        //        String.valueOf(saveGeneralDetailForm.getHeading()),
          //      String.valueOf(saveGeneralDetailForm.getContent()),
            //    String.valueOf(saveGeneralDetailForm.getRegistrationNumber())))
              //  .andExpect(status().isOk())
                //.andExpect(content().contentType("application/json"))
                //.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

    }


    @Test
    public void updateGeneralInformation() {
    }

    @Test
    public void findAllGeneralDetails() {
    }

    @Test
    public void deleteGeneralInformation() {
    }
}