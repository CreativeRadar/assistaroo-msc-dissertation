package com.nsa.msc.dissertation.Forms;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SaveServiceDetailsFormTest {

    private SaveServiceDetailsForm saveServiceDetailsForm;

    @Before
    public final void createSaveServiceDetailsForm(){
        saveServiceDetailsForm = new SaveServiceDetailsForm("012345",
                "Laundry","ironing","NP2345");
    }

    @Test
    public void getRegistration_number() {
        assertNotNull(saveServiceDetailsForm);
        assertEquals("012345",saveServiceDetailsForm.getRegistration_number());
    }

    @Test
    public void getMain_services() {
        assertNotNull(saveServiceDetailsForm);
        assertEquals("Laundry",saveServiceDetailsForm.getMain_services());
    }

    @Test
    public void getSub_services() {
        assertNotNull(saveServiceDetailsForm);
        assertEquals("ironing",saveServiceDetailsForm.getSub_services());
    }
    @Test
    public void getAreas_of_operations() {
        assertNotNull(saveServiceDetailsForm);
        assertEquals("NP2345",saveServiceDetailsForm.getAreas_of_operation());
    }
}