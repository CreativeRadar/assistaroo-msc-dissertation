package com.nsa.msc.dissertation.dto;

import org.junit.Before;
import org.junit.Test;

import static com.nsa.msc.dissertation.dto.ChargeRequest.Currency.GBP;
import static com.nsa.msc.dissertation.dto.ChargeRequest.Currency.USD;
import static org.junit.Assert.*;

public class ChargeRequestTest {

    private ChargeRequest chargeRequest;


    @Before
    public final void createChargeRequestDTO(){
        chargeRequest = new ChargeRequest("Service Charge",2000, ChargeRequest.Currency.GBP,
                                        "assitaroo@assistaroo.com", "pk_live_BbwLhNQ1vPTcPzEK8hf2USfH00Qul7JNn0");
    }

    @Test
    public void getDescription() {
        assertNotNull(chargeRequest);
        assertEquals(chargeRequest.getDescription(),"Service Charge");
    }

    @Test
    public void setDescription() {
        assertNotNull(chargeRequest);
        chargeRequest.setDescription("Regular charge");
        assertEquals("Regular charge",chargeRequest.getDescription());
    }

    @Test
    public void getAmount() {
        assertNotNull(chargeRequest);
        assertEquals(chargeRequest.getAmount(),2000);
    }

    @Test
    public void setAmount() {
        assertNotNull(chargeRequest);
        chargeRequest.setAmount(500);
        assertEquals(500,chargeRequest.getAmount());
    }

    @Test
    public void getCurrency() {
        assertNotNull(chargeRequest);
        assertEquals(chargeRequest.getCurrency(),GBP);
    }

    @Test
    public void setCurrency() {
        assertNotNull(chargeRequest);
        chargeRequest.setCurrency(USD);
        assertEquals(USD,chargeRequest.getCurrency());
    }

    @Test
    public void getStripeEmail() {
        assertNotNull(chargeRequest);
        assertEquals(chargeRequest.getStripeEmail(),"assitaroo@assistaroo.com");
    }

    @Test
    public void setStripeEmail() {
        assertNotNull(chargeRequest);
        chargeRequest.setStripeEmail("new@assistaroo.com");
        assertEquals("new@assistaroo.com",chargeRequest.getStripeEmail());
    }

    @Test
    public void getStripeToken() {
        assertNotNull(chargeRequest);
        assertEquals("pk_live_BbwLhNQ1vPTcPzEK8hf2USfH00Qul7JNn0",chargeRequest.getStripeToken());
    }
    @Test
    public void setStripeToken() {
        assertNotNull(chargeRequest);
        chargeRequest.setStripeToken("pk_345678fghjk5467fghjkl");
        assertEquals("pk_345678fghjk5467fghjkl",chargeRequest.getStripeToken());
    }
}

/*
REFERENCES

coderanch.com.2019.JUnit tests for Getters and Setters.Available at:https://coderanch.com/t/515213/engineering/JUnit-tests-Getters-Setters
[Accessed: 29 August 2019]

 */