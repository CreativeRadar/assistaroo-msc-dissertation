package com.nsa.msc.dissertation.dto;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServicesDTOTest {
    private ServicesDTO services;

    @Before
    public final void setServices(){
        services = new ServicesDTO(1,"Walking","Pet->Walking","image",4);
    }

    @Test
    public void serviceExists(){
        assertNotNull(services);
    }

    @Test
    public void servicesTests(){
        assertEquals(1,services.getServiceID());
        assertEquals("Walking",services.getName());
        assertEquals("Pet->Walking",services.getServicePathString());
        assertEquals("image",services.getImageURL());
        assertEquals(4,services.getParentID());
    }
}
