package com.nsa.msc.dissertation.systems_test;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import java.util.*;
public class ScrollFeatureTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {

    //Not a good practice. Replace with driver path below
    System.setProperty("webdriver.chrome.driver", "ENTER DRIVER PATH");
    driver =new ChromeDriver();

    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void scrollFeature() {
    driver.get("http://www.assistaroo.co.uk/");
    driver.manage().window().setSize(new Dimension(516, 580));
    driver.findElement(By.cssSelector(".fa-chevron-right > path")).click();
    driver.findElement(By.cssSelector(".fa-chevron-left")).click();
    driver.findElement(By.id("right-button")).click();
    driver.findElement(By.cssSelector(".fa-chevron-left")).click();
    driver.findElement(By.cssSelector(".fa-chevron-right")).click();
    driver.findElement(By.id("left-button")).click();
  }
}

//Generated from selenium IDE, fixed bugs and modified to run