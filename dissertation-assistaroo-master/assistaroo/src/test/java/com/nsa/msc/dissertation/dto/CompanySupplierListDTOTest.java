package com.nsa.msc.dissertation.dto;

import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.NoFieldShadowingRule;
import com.openpojo.validation.rule.impl.NoPublicFieldsExceptStaticFinalRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.Test;


public class CompanySupplierListDTOTest {

    private static final Validator ACCESSOR_VALIDATOR = ValidatorBuilder.create()
            .with(new GetterMustExistRule())
            .with(new SetterMustExistRule())
            .with(new GetterTester())
            .with(new SetterTester())
            .with(new NoPublicFieldsExceptStaticFinalRule())
            .with(new NoFieldShadowingRule())
            .build();

    public static void validateAccessors(final Class<?> CompanySupplierListDTO) {
        ACCESSOR_VALIDATOR.validate(PojoClassFactory.getPojoClass(CompanySupplierListDTO));
    }

    @Test
    public void accesorsShouldAccessAppropriateFields() {
        validateAccessors(CompanySupplierListDTO.class);
    }


}




  /*
    REFERENCES
    reliablesoftwareblog.wordpress.com.2016.Achieving 100% code coverage. Testing getters and setters.
    Available at:https://reliablesoftwareblog.wordpress.com/2016/04/28/achieving-100-code-coverage-testing-getters-and-setters/
    [Accessed: 13 September 2019]
     */