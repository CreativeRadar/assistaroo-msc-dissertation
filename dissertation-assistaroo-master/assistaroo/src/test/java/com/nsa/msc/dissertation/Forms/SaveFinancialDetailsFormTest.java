package com.nsa.msc.dissertation.Forms;

import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.NoFieldShadowingRule;
import com.openpojo.validation.rule.impl.NoPublicFieldsExceptStaticFinalRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.Test;


public class SaveFinancialDetailsFormTest {
    private static final Validator ACCESSOR_VALIDATOR = ValidatorBuilder.create()
            .with(new GetterMustExistRule())
            .with(new SetterMustExistRule())
            .with(new GetterTester())
            .with(new SetterTester())
            .with(new NoPublicFieldsExceptStaticFinalRule())
            .with(new NoFieldShadowingRule())
            .build();

    public static void validateAccessors(final Class<?> SaveFinancialDetailsForm) {
        ACCESSOR_VALIDATOR.validate(PojoClassFactory.getPojoClass(SaveFinancialDetailsForm));
    }

    @Test
    public void accesorsShouldAccessAppropriateFields() {
        validateAccessors(SaveFinancialDetailsForm.class);
    }

}