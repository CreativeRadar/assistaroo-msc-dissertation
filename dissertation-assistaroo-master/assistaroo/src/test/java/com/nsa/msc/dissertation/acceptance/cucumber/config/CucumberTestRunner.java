package com.nsa.msc.dissertation.acceptance.cucumber.config;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature"
        ,glue={"com.nsa.msc.dissertation.acceptance.cucumber.stepdefs"}
        ,monochrome = true,
        plugin = {"pretty", "html:target"})
public class CucumberTestRunner {
}
