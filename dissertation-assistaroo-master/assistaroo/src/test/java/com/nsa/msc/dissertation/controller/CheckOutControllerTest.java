package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.dto.ChargeRequest;
import com.stripe.exception.InvalidRequestException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations="classpath:application-test.properties")
public class CheckOutControllerTest {

    @Value("${stripe.keys.public}")
    private String testPublicKey;

    @Autowired
    private MockMvc mockMvc;



    @Test
    public void checkout() throws Exception {
        this.mockMvc.perform(get("/checkout"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=utf-8"))
                .andExpect(model().attribute("amount",2000))
                 .andExpect(model().attribute("stripePublicKey",testPublicKey))
                 .andExpect(model().attribute("currency",ChargeRequest.Currency.GBP));

    }

}







    /* REFERENCES
    stack overflow.2019.Spring Boot accessing application.properties for JUnit Test.
    Available at:https://stackoverflow.com/questions/55464408/spring-boot-accessing-application-properties-for-junit-test
    [Accessed: 30 August 2019]

     */