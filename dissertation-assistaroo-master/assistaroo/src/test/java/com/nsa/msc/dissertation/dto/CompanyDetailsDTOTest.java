package com.nsa.msc.dissertation.dto;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CompanyDetailsDTOTest {
    private CompanyDetailsDTO companyDetailsDTO;


    @Before
    public final void createServiceDTO() {
        companyDetailsDTO = new CompanyDetailsDTO("12342","LaundryHub");
    }

    @Test
    public void getRegistration_number() {
        assertNotNull(companyDetailsDTO);
        assertEquals(companyDetailsDTO.getRegistration_number(),  "12342");
    }

    @Test
    public void getName() {
        assertNotNull(companyDetailsDTO);
        assertEquals(companyDetailsDTO.getName() , "LaundryHub");
    }

}

/*
REFERENCES

coderanch.com.2019.JUnit tests for Getters and Setters.Available at:https://coderanch.com/t/515213/engineering/JUnit-tests-Getters-Setters
[Accessed: 29 August 2019]


 */