
package com.nsa.msc.dissertation;
import com.nsa.msc.dissertation.dto.BookingDTO;
import com.nsa.msc.dissertation.dto.CustomerDTO;
import com.nsa.msc.dissertation.dto.SupplierDTO;

import com.nsa.msc.dissertation.repository.SupplierRepositoryJDBC;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan
public class SupplierRepositoryJDBCTest { //(https://htr3n.github.io., 2018)

    private final int BOOKING_ID_ONE = 1;
    private final int BOOKING_ID_TWO = 2;
    private DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private final Date BOOKING_ONE_START_DATE_TIME = format.parse("2019-08-07 09:00:00.000");
    private final Date BOOKING_TWO_START_DATE_TIME = format.parse("2019-08-08 12:40:00.000");
    private final float BOOKING_ONE_DURATION = 1;
    private final float BOOKING_TWO_DURATION = 2;
    private final String BOOKING_ONE_SERVICE = "Laundry";
    private final String BOOKING_TWO_SERVICE = "Cleaning";
    private final int SUPPLIER_ONE_ID = 1;
    private final int SUPPLIER_TWO_ID = 2;
    private final String SUPPLIER_ONE_FIRST_NAME = "Tom";
    private final String SUPPLIER_ONE_LAST_NAME = "Jones";
    private final String SUPPLIER_TWO_FIRST_NAME = "Harry";
    private final String SUPPLIER_TWO_LAST_NAME = "Baker";
    private final String CUSTOMER_ONE_EMAIL = "travis@email.com";
    private final String CUSTOMER_TWO_EMAIL = "arron@email.com";
    private final String CUSTOMER_ONE_PHONE_NUMBER = "2103654785";
    private final String CUSTOMER_TWO_PHONE_NUMBER = "6521320147";
    private final String CUSTOMER_ONE_ADDRESS = "128  Thames Street";
    private final String CUSTOMER_TWO_ADDRESS = "39  Argyll Road";
    private final String CUSTOMER_ONE_POSTCODE = "SA37 9PW";
    private final String CUSTOMER_TWO_POSTCODE = "LL65 1WH";
    private final int CUSTOMER_ONE_ID = 1;
    private final int CUSTOMER_TWO_ID = 2;
    private final String CUSTOMER_ONE_TITLE = "Mr";
    private final String CUSTOMER_ONE_FIRST_NAME = "Travis";
    private final String CUSTOMER_ONE_LAST_NAME = "Robson";
    private final String CUSTOMER_TWO_TITLE = "Mr";
    private final String CUSTOMER_TWO_FIRST_NAME = "Arron";
    private final String CUSTOMER_TWO_LAST_NAME = "Chen";

    @Autowired
    private SupplierRepositoryJDBC supplierRepositoryJDBC;

    private SupplierDTO supplier_one;
    private SupplierDTO supplier_two;
    private BookingDTO booking_one;
    private BookingDTO booking_two;

    public SupplierRepositoryJDBCTest() throws ParseException { // Needed for parse error
    }

    @Before
    public void createSuppliers(){
        supplier_one = new SupplierDTO(SUPPLIER_ONE_ID,SUPPLIER_ONE_FIRST_NAME,SUPPLIER_ONE_LAST_NAME);
        supplier_two = new SupplierDTO(SUPPLIER_TWO_ID,SUPPLIER_TWO_FIRST_NAME,SUPPLIER_TWO_LAST_NAME);
    }

    @Test
    public void bookingsReturnedForCorrectSupplier() {
        booking_one = new BookingDTO(BOOKING_ID_ONE,BOOKING_ONE_START_DATE_TIME,BOOKING_ONE_DURATION,BOOKING_ONE_SERVICE,
                supplier_one,
                new CustomerDTO(CUSTOMER_ONE_EMAIL,CUSTOMER_ONE_PHONE_NUMBER,CUSTOMER_ONE_ADDRESS,CUSTOMER_ONE_POSTCODE,
                        CUSTOMER_ONE_ID,CUSTOMER_ONE_TITLE,CUSTOMER_ONE_FIRST_NAME,CUSTOMER_ONE_LAST_NAME));
        booking_two = new BookingDTO(BOOKING_ID_TWO,BOOKING_TWO_START_DATE_TIME,BOOKING_TWO_DURATION,BOOKING_TWO_SERVICE,
                supplier_two,
                new CustomerDTO(CUSTOMER_TWO_EMAIL,CUSTOMER_TWO_PHONE_NUMBER,CUSTOMER_TWO_ADDRESS,CUSTOMER_TWO_POSTCODE,
                        CUSTOMER_TWO_ID,CUSTOMER_TWO_TITLE,CUSTOMER_TWO_FIRST_NAME,CUSTOMER_TWO_LAST_NAME));

        List<BookingDTO> supplier_one_result = supplierRepositoryJDBC.findASuppliersBookings(SUPPLIER_ONE_ID);
        List<BookingDTO> supplier_two_result = supplierRepositoryJDBC.findASuppliersBookings(SUPPLIER_TWO_ID);

        assertThat(supplier_one_result.get(0)).isEqualToComparingFieldByFieldRecursively(booking_one); //(Wang, Y. et al., 2018)

        try {
            assertThat(supplier_two_result.get(0)).isEqualToComparingFieldByFieldRecursively(booking_one); //(Wang, Y. et al., 2018)
        } catch (AssertionError e) {
            System.out.println("The booking doesn't belong to supplier " + SUPPLIER_TWO_FIRST_NAME + " " + SUPPLIER_TWO_LAST_NAME + ".");
            System.out.println(e);
        }

        try {
            assertThat(supplier_one_result.get(0)).isEqualToComparingFieldByFieldRecursively(booking_two); //(Wang, Y. et al., 2018)
        } catch (AssertionError e) {
            System.out.println("The booking doesn't belong to supplier " + SUPPLIER_ONE_FIRST_NAME + " " + SUPPLIER_ONE_LAST_NAME + ".");
            System.out.println(e);
        }
    }

    @Test
    public void findByIdReturnsCorrectSupplier(){
        SupplierDTO expectedSupplier = new SupplierDTO(3,"t_jones","02568",1);
        SupplierDTO supplier = supplierRepositoryJDBC.findById(SUPPLIER_ONE_ID);
        assertNotNull(supplier);
        assertThat(supplier).isEqualToComparingFieldByField(expectedSupplier); //(Wang, Y. et al., 2018)
    }

}

/* References:

   https://htr3n.github.io. 2018. Back to basics: Test-driven Spring JDBC. Available at:
   https://htr3n.github.io/2018/11/test-driven-spring-jdbc/ [Accessed: 07 August 2019].  I have adapted the code to test
   that the method findASuppliersBookings(int supplier_id) only returns the bookings for the correct supplier from the
   database.

   Wang, Y. et al. 2018. Class AbstractObjectAssert<SELF extends AbstractObjectAssert<SELF,ACTUAL>,ACTUAL>.
   Available at: http://joel-costigliola.github.io/assertj/core/api/org/assertj/core/api/AbstractObjectAssert.html
   [Accessed: 08 August 2019].  I have used the method isEqualToComparingFieldByFieldRecursively() to check that
   the values for each field are being returned as expected.  In this case, the recursive version is needed as
   BookingDTO() contains nested DTOs SupplierDTO() and CustomerDTO().  Similarly, I have used method
   isEqualToComparingFieldByField() to check that the findById() method in supplierRepositoryJDBC returns the correct
   supplier fields.
 */
