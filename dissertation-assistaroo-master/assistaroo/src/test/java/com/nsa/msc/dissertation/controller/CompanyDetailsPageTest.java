package com.nsa.msc.dissertation.controller;

import com.nsa.msc.dissertation.dto.CompanyDetailsDTO;
import com.nsa.msc.dissertation.dto.CompanyGeneralDetailsDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc


public class CompanyDetailsPageTest {




    @Autowired
    private MockMvc mvc;


    @Test
    public void CompanyDetailsPageRendersProperly() throws Exception {
        this.mvc.perform(get("/company-details/{registration_number}","01234214" ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("company-details"));



    }

}
