package com.nsa.msc.dissertation.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerHomePageTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void customerHomePageTest() throws Exception {
        this.mvc.perform(get("/"))
             .andExpect(status().isOk());
   }

}


/*
REFERENCES
stack Overflow.2017.Spring Boot - How to test that Model attribute was added from configuration file?.
Available at:https://stackoverflow.com/questions/46434553/spring-boot-how-to-test-that-model-attribute-was-added-from-configuration-file
[Accessed: 30 August 2019]
 */