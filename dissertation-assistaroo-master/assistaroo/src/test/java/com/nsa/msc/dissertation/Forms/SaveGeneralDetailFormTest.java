package com.nsa.msc.dissertation.Forms;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SaveGeneralDetailFormTest {

    private SaveGeneralDetailForm saveGeneralDetailForm;

    @Before
    public final void createSaveGeneralDetailsForm(){
        saveGeneralDetailForm = new SaveGeneralDetailForm("1346543",
                "Email","e@assistaroo.com",3);
    }

    @Test
    public void getHeading() {
        assertNotNull(saveGeneralDetailForm);
        assertEquals("Email",saveGeneralDetailForm.getHeading());
    }

    @Test
    public void getContent() {
        assertNotNull(saveGeneralDetailForm);
        assertEquals("e@assistaroo.com",saveGeneralDetailForm.getContent());
    }

    @Test
    public void getRegistrationNumber() {
        assertNotNull(saveGeneralDetailForm);
        assertEquals("1346543",saveGeneralDetailForm.getRegistrationNumber());
    }

    @Test
    public void getCompanyGeneralDetailsID() {
        assertNotNull(saveGeneralDetailForm);
        assertEquals(3,saveGeneralDetailForm.getCompanyGeneralDetailsID());
    }


}
