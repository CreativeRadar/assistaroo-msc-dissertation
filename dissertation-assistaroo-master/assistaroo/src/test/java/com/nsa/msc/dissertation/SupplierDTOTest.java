package com.nsa.msc.dissertation;

import com.nsa.msc.dissertation.dto.SupplierDTO;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SupplierDTOTest {

    private SupplierDTO supplier;

    @Before
    public final void createSupplier(){
        supplier = new SupplierDTO(1,"bob_jones","password", "bob.jones@outlook.com",
                false, "01252124589","28 Station Road, Porthcawl",
                "CF56 7TR","021abc",1,"012584ty",
                "125846954ab", "Bob", "Jones", "Mr");
    }

    @Test
    public void supplierExists(){
        assertNotNull(supplier);
    }

    @Test
    public void userAttributesAreSetWithSupplierInstantiation(){
        assertEquals(1,supplier.getId());
        assertEquals("bob_jones",supplier.getUsername());
        assertEquals("password",supplier.getPassword());
        assertEquals("bob.jones@outlook.com",supplier.getEmail());
        assertFalse(supplier.isIs_admin());
        assertEquals("01252124589",supplier.getPhone_number());
        assertEquals("28 Station Road, Porthcawl",supplier.getAddress());
        assertEquals("CF56 7TR",supplier.getPostcode());
    }

}

/* References:
*  Stefanski, D. et. al. 2016. Assertions · junit-team/junit4 Wiki · GitHub. Available at:
*  https://github.com/junit-team/junit4/wiki/Assertions [Accessed: 6 August 2019].  I have used assertions to check
*  that user attributes are set when a supplier is created.
*/
