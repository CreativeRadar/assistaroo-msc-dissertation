package com.nsa.msc.dissertation.repository;

import com.nsa.msc.dissertation.dto.ServiceDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan
public class ServiceRepositoryJDBCTest {

    private static final int servicesOfferedID=1;
    private static final  int price = 20;
    private static final  String description ="washing";
    private static final int serviceID =1;
    private static final int supplierID=1;
    private static final String image_url= "/images/washing.jpg";
    private static final String service_name ="Laundry";


    @Autowired
    private ServiceRepositoryJDBC serviceRepositoryJDBC;

    private ServiceDTO washing;

    @Before
    public void setUp() throws Exception {
        washing = new ServiceDTO(servicesOfferedID,price,description,serviceID,
                                supplierID,image_url,service_name);

    }


    @Test
    public void findAllServiceSubCategories() {
        List<ServiceDTO> service_sub_categories = serviceRepositoryJDBC.findAllServiceSubCategories();
        assertNotNull(service_sub_categories );

    }

    @Test
    public void findById() {
        ServiceDTO validService = serviceRepositoryJDBC.findById(washing.getServicesOfferedID());
        assertNotNull(validService);
        assertEquals(washing.getPrice(),validService.getPrice());
        assertEquals(washing.getDescription(),validService.getDescription());
        assertEquals(washing.getServiceID(),validService.getServiceID());
        assertEquals(washing.getSupplierID(),validService.getSupplierID());
        assertEquals(washing.getImage_url(),validService.getImage_url());
        assertEquals(washing.getService_name(),validService.getService_name());

    }

    @Test
    public void findByParentCategory() {
        List<ServiceDTO> validService = serviceRepositoryJDBC.findByParentCategory(washing.getService_name());
        assertNotNull(validService);
    }

    @Test
    public void findAllLaundrySubCategories() {
        List<ServiceDTO> laundrySubCategories = serviceRepositoryJDBC.findAllLaundrySubCategories();
        assertNotNull(laundrySubCategories);
    }

    @Test
    public void findAllCleaningSubCategories() {
        List<ServiceDTO> cleaningSubCategories = serviceRepositoryJDBC.findAllCleaningSubCategories();
        assertNotNull(cleaningSubCategories);
    }

    @Test
    public void findAllPersonalSubCategories() {
        List<ServiceDTO> personalSubCategories = serviceRepositoryJDBC.findAllPersonalSubCategories();
        assertNotNull(personalSubCategories);
    }

    @Test
    public void findAllMaintenanceSubCategories() {
        List<ServiceDTO> maintenanceSubCategories = serviceRepositoryJDBC.findAllMaintenanceSubCategories();
        assertNotNull(maintenanceSubCategories);
    }

    @Test
    public void findAllPetsSubCategories() {
        List<ServiceDTO> petsSubCategories = serviceRepositoryJDBC.findAllPetsSubCategories();
        assertNotNull(petsSubCategories);
    }

    @Test
    public void findAllProductsSubCategories() {
        List<ServiceDTO> prodcutsSubCategories = serviceRepositoryJDBC.findAllProductsSubCategories();
        assertNotNull(prodcutsSubCategories);
    }

    @Test
    public void findAllHomeCareSubCategories() {
        List<ServiceDTO> homeCareSubCategories = serviceRepositoryJDBC.findAllHomeCareSubCategories();
        assertNotNull(homeCareSubCategories);
    }

    @Test
    public void findAllTreatmentsSubCategories() {
        List<ServiceDTO> treatmentsSubCategories = serviceRepositoryJDBC.findAllTreatmentsSubCategories();
        assertNotNull(treatmentsSubCategories);
    }
}


/*
REFERENCES

htr3n's.2018.Back to basics: Test-driven Spring JDBC.
Available at:https://htr3n.github.io/2018/11/test-driven-spring-jdbc/
[Accessed: 30 August 2019]

 */