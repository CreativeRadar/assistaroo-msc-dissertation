package com.nsa.msc.dissertation.dto;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CompanyAccountDetailsDTOTest {

    private CompanyAccountDetailsDTO accountDetailsDTO;

    @Before
    public final void createAccountDetailsDTO() {
        accountDetailsDTO = new CompanyAccountDetailsDTO("1234124","Laundromart",
                                                    "admin@laundromart.com","laundPasskey@123");
    }


    @Test
    public void getRegistration_number() {
        assertNotNull(accountDetailsDTO);
        assertEquals(accountDetailsDTO.getRegistration_number(),"1234124");
    }

    @Test
    public void getUsername() {
        assertNotNull(accountDetailsDTO);
        assertEquals(accountDetailsDTO.getUsername(),"Laundromart");
    }


    @Test
    public void getEmail() {
        assertNotNull(accountDetailsDTO);
        assertEquals(accountDetailsDTO.getEmail(),"admin@laundromart.com");
    }

    @Test
    public void getPassword() {
        assertNotNull(accountDetailsDTO);
        assertEquals(accountDetailsDTO.getPassword(),  "laundPasskey@123");
    }


}

/*
REFERENCES

coderanch.com.2019.JUnit tests for Getters and Setters.Available at:https://coderanch.com/t/515213/engineering/JUnit-tests-Getters-Setters
[Accessed: 29 August 2019]


 */